function [vE,vN,vU,verr,vX,vY,vZ] = AD2CP_coordTransform(V1,V2,V3,V4,hdg,pitch,roll,varargin)
% Converts beam velocities into both instrument coordinates and
% east-north-up coordinates.
%
% NOT SURE IF TRANSFORMATION FROM XYZ TO ENU IS CORRECT
%
% Inputs:
%  - V1,V2,V3,V4: Beam velocities along each transducer (matrices)
%  - hdg: A vector of the heading measurements
%  - pitch: A vector of the heading measurements
%  - roll: A vector of the heading measurements
%
% Outputs:
%  - vE: Eastward velocity (relative to magnetic north)
%  - vN: Northward velocity (relative to magnetic north)
%  - vU: Vertical velocity
%  - verr: Error velocity (Difference of two vert vel estimates)
%  - vE: Eastward velocity
%  - vX,vY,vZ: Velocity in X,Y,Z directions (instrument coordinates)
%
% Justine McMillan
% May 6, 2014 - instrument velocities seem to make sense for both GP and DG data
% June 29, 2015 -- made some modifications trying to get it to agree with
%                  ADCP located on same frame (turb2013 experiment)
% July 6, 2015 -- added options stucture to be able to control optional
%                 parameters
theta = 25;

if nargin>7
    options = varargin{1}; %Struct with options
else
    options = struct;
end
% Defaults
if ~isfield(options,'figure'); options.figure=1; end


%% horizontal and vertical components (see documentation)
V13 = (V3-V1)/2/sind(theta);
V24 = (V4-V2)/2/sind(theta);
Vz24 = (V4+V2)/2/cosd(theta);
Vz13 = (V3+V1)/2/cosd(theta);

%% instrument coordinates
vX = (V24-V13)*cosd(45);
vY = (V24+V13)*sind(45);
vZ = 1/2*(Vz24+Vz13);
verr = Vz24-Vz13;

%%
if options.figure
    figure(1),clf
    subplot(311)
    plot([V1 V2 V3 V4])
    legend('V1','V2','V3','V4')
    
    subplot(312)
    plot([V13 V24 Vz13 Vz24])
    legend('V13','V24','Vz13','Vz24')
    
    subplot(313)
    plot([vX vY vZ])
    legend('X','Y','Z')
end

%% Transformation Matrix (HAVE NOT VERIFIED THIS!)
for ii = 1:length(roll)
    
    cH = cosd(hdg(ii));   sH = sind(hdg(ii));
    cP = cosd(pitch(ii)); sP = sind(pitch(ii));
    cR = cosd(roll(ii));  sR = sind(roll(ii));
    
    RH = [sH -cH 0; cH  sH   0;   0  0  1];
    RP = [cP 0 -sP;  0   1   0;  sP  0 cP];
    RR = [ 1  0  0;  0  cR -sR;   0 sR cR];
    
    T = RH*RP*RR;
    
    U_instr = [vX(ii); vY(ii); vZ(ii)];
    
    U_earth = T*U_instr;
    
    vE(ii) = U_earth(1);
    vN(ii) = U_earth(2);
    vU(ii) = U_earth(3);
    
    
end

warning('Have not verified transformation from XYZ to ENU - doesnt seem right yet')
