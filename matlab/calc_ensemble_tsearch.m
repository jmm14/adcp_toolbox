function [AVGDATA] = calc_ensemble_tsearch(t_ens,DATA)
% Calculates the ensembles for each field in the structure. Does so by
% searching through the time vector.
%
% Assumes that one of the fields is "mtime" (could be modified)
%
% Justine McMillan
% Nov 19, 2013

flg.debug = 1;

tlen = length(DATA.mtime);

%% Set up time vector
[yy,mm,dd,hr,min,~] = datevec(DATA.mtime(1));

t1min = round2(min,t_ens);
if t1min>60
    t1min = t1min - 60;
    hr = hr+1;
end
AVGDATA.mtime = [datenum(yy,mm,dd,hr,t1min,0):t_ens/60/24:DATA.mtime(end)]';

%% initialize
fields = fieldnames(DATA);
for ii = 1:length(fields)
    if strcmp(fields{ii},'mtime')==0
        if size(DATA.(fields{ii}),1) == tlen
            AVGDATA.(fields{ii}) = NaN*ones(length(AVGDATA.mtime)-1,size(DATA.(fields{ii}),2));
        else
            AVGDATA.(fields{ii}) = NaN*ones(size(DATA.(fields{ii}),1),length(AVGDATA.mtime)-1);

        end
    end
end
%% Compute means and standard deviations
for ii = 1:length(AVGDATA.mtime)-1
    tstart = AVGDATA.mtime(ii);
    tend = AVGDATA.mtime(ii+1);

    ind_t = find(DATA.mtime>tstart & DATA.mtime<tend);
    for jj = 1:length(fields)
        if strcmp(fields{jj},'mtime')==1
            continue
        else
            %Assign field
            fieldvar = fields{jj};
            fieldstd = [fields{jj},'_std'];
            eval(['field = DATA.',fields{jj},';'])
            

            %Compute mean and std
            if size(DATA.(fields{jj}),1) == tlen
            AVGDATA.(fieldvar)(ii,:) = nanmean(field(ind_t,:));
            AVGDATA.(fieldstd)(ii,:) = nanstd(field(ind_t,:));
            else
            AVGDATA.(fieldvar)(:,ii) = nanmean(field(:,ind_t)')';
            AVGDATA.(fieldstd)(:,ii) = nanstd(field(:,ind_t)')';
            end
        end
    end
    
    clear ind_t
    
end
if size(DATA.(fields{end}),1) == tlen
    AVGDATA.mtime = AVGDATA.mtime(1:end-1);
else
    AVGDATA.mtime = AVGDATA.mtime(1:end-1)';
end



if flg.debug == 1
    tdrop = (DATA.mtime(end) - (AVGDATA.mtime(end)+t_ens/24/60))*24*60;
    disp([num2str(tdrop),' minutes of data dropped from the end of the file'])
end