function calc_attstats(fileinfo, options)
% Calculates some statistics of the attitude parameters.
%   - Determines the mean and variance (standard deviation) of ensemble
%     quantities
%   - Creates a cumulative distribution function of the standard deviations of
%     of the attitude parameters
%   - Interpolates the expected standard deviations at certain percentiles.
%
% Inputs
% - fileinfo: a structure containing the names of the files to use
%       (Attfile)
% - options: a structure containing some options to use for the
% computations (percentiles, histogram bin size)
%
% Outputs (saved into an output file)
% - Structures called "pitch","roll","heading with the fields"
%       meas (measured quantity -- an ensemble average)
%       mean (mean ensemble value)
%       varmean (standard deviation of the mean value)
%       std (standard deviation within an ensemble)
%       prct (percentiles to estimate std at)
%       prct_stdvals (standard deviations at corresponding percentiles)
%       degbins (the bins used in the computation of the histogram)
%       dist (the distribution of the standard deviations)
%       cdf (the cumulative distribution function) 
% - yd (A time vector corresponding to the year day of 2012)
%
% Justine McMillan
% Nov 14, 2013 (modified from an earlier version used for the swnsra)
%
% Jan 14, 2014 - Modified to find and remove outliers (motivated by
% outliers at the beginning of each burst for GP130730TA RDI data)
% Sep 24, 2014 - Modified to allow for computation of mean over a different
% range than the plot shows (modivated by movement of GP130730TA RDI data)

%% defaults
if ~isfield(options,'outliers')
    options.outliers = 0;
end
if ~isfield(options,'tlimplot')
    options.tlimplot = options.tlim;
end
%%
cdfoptions.percentiles = options.percentiles;
cdfoptions.histbinsize = options.histbinsize;

load([fileinfo.datadir fileinfo.Attfile])
day1 = datevec(adcp.mtime(1));
ydall = adcp.mtime-datenum(day1(1),0,0);

ind_plot = find(ydall>options.tlimplot(1) & ydall<options.tlimplot(2));
tind = find(ydall>options.tlim(1) & ydall<options.tlim(2));
mtime = adcp.mtime(tind);
yd = ydall(tind);

%% calculate mean and standard deviations of ensemble attitude parameters
pitch.meas = adcp.pitch(tind);
pitch.std = adcp.pitch_std(tind);

roll.meas = adcp.roll(tind);
roll.std = adcp.roll_std(tind);


heading.meas = adcp.heading(tind);
heading.std = adcp.heading_std(tind);
if isfield(options,'adjustheading') %adjust the heading for wrap around
   if options.adjustheading == 1
    hind = find(heading.meas>300);
    heading.meas(hind) = heading.meas(hind)-360;
   end
end

%% Remove outliers and recompute
if options.outliers == 1
    pitch.meas = rm_outliers(pitch.meas,options);
    roll.meas = rm_outliers(roll.meas,options);
    heading.meas = rm_outliers(heading.meas,options);
end

pitch.mean = nanmean(pitch.meas);
pitch.varmean = nanstd(pitch.meas);

roll.mean = nanmean(roll.meas);
roll.varmean = nanstd(roll.meas);

heading.mean = nanmean(heading.meas);
heading.varmean = nanstd(heading.meas);


%% Plot means
myfigure('MeanAttitude')
set(gcf,'Position',[680  50 1071 928])
ax_h(1)=subplot(311);
%plot(yd,pitch.meas,'-k.')
plot(get_yd(adcp.mtime(ind_plot)),rm_outliers(adcp.pitch(ind_plot),options),'-k.')
hold on
plot([min(yd) max(yd)],[pitch.mean pitch.mean],'r')
plot([min(yd) max(yd)],(pitch.mean+pitch.varmean)*[1 1],'--r')
plot([min(yd) max(yd)],(pitch.mean-pitch.varmean)*[1 1],'--r')
axis('tight')
ylabel('pitch')

ax_h(2)=subplot(312);
%plot(yd,roll.meas,'-k.')
plot(get_yd(adcp.mtime(ind_plot)),rm_outliers(adcp.roll(ind_plot),options),'-k.')
hold on
plot([min(yd) max(yd)],[roll.mean roll.mean],'r')
plot([min(yd) max(yd)],(roll.mean+roll.varmean)*[1 1],'--r')
plot([min(yd) max(yd)],(roll.mean-roll.varmean)*[1 1],'--r')
axis('tight')
ylabel('roll')


ax_h(3)=subplot(313);
%plot(yd,heading.meas,'-k.')
plot(get_yd(adcp.mtime(ind_plot)),rm_outliers(adcp.heading(ind_plot),options),'-k.')
hold on
plot([min(yd) max(yd)],[heading.mean heading.mean],'r')
plot([min(yd) max(yd)],(heading.mean+heading.varmean)*[1 1],'--r')
plot([min(yd) max(yd)],(heading.mean-heading.varmean)*[1 1],'--r')
axis('tight')
ylabel('heading')

ll = legend('measured','mean','std dev','Location','NorthEast');
pos = get(ll,'Position');
set(ll,'Position',pos+[0.02 0.02  0 0])

xlabel('year day (UTC)')

linkaxes(ax_h,'x')

%% Separate heading into flood and ebb

if options.fldebb == 1
    load([fileinfo.outdir fileinfo.BSfile])
    
    [tideflg, spdinterp] = get_ebbfld(time.mtime,data.mag_signed_vel,mtime);
    
    % Flood
    tfldind = find(tideflg == 1);
    headingfld.yd = yd(tfldind);
    headingfld.meas = heading.meas(tfldind);
    headingfld.std = heading.std(tfldind);
    headingfld.mean = nanmean(headingfld.meas);
    headingfld.varmean = nanstd(headingfld.meas);
    
    % myfigure('Heading_flood')
    % subplot(211)
    % plot(headingfld.yd,headingfld.meas,'.')
    % subplot(212)
    % x = cos(headingfld.meas*pi/180);
    % y = sin(headingfld.meas*pi/180);
    % plot(x,y,'.')
    % %meanx = nanmean(x);
    % %meany = nanmean(y);
    %
    % dir = atan2(y,x)*180/pi;
    %
    % %headingebb.mean = nanmean(dir);
    % %headingebb.varmean = nanstd(dir);
    
    
    %% Get the mean angle and std using a histogram approach
    myfigure('HistogramFld')
    
    [N,degbins]=hist(headingfld.meas,360);
    
    subplot(311)
    plot(degbins,N)
    
    %find the maximum
    [~,maxind] = max(N);
    maxdeg = degbins(maxind);
    %shift the histogram to be centered around zero
    shiftdir = headingfld.meas - maxdeg;
    %force angles to be between -180 and 180
    ind = find(shiftdir>180);
    shiftdir(ind) = shiftdir(ind) - 360;
    ind = find(shiftdir<-180);
    shiftdir(ind) = shiftdir(ind) + 360;
    
    subplot(312)
    plot(headingfld.yd,shiftdir,'.')
    
    
    %Get new histogram
    [N2,degbins2]=hist(shiftdir,360);
    
    subplot(313)
    plot(degbins2,N2)
    
    % Get the mean and standard deviation
    headingfld.mean = mean(shiftdir) + maxdeg;
    headingfld.varmean = std(shiftdir);
    
    
    %% Ebb
    tebbind = find(tideflg == 0);
    headingebb.yd = yd(tebbind);
    headingebb.meas = heading.meas(tebbind);
    headingebb.std = heading.std(tebbind);
    headingebb.mean = nanmean(headingebb.meas);
    headingebb.varmean = nanstd(headingebb.meas);
    
    % myfigure('Heading_ebb')
    % subplot(211)
    % plot(headingebb.yd,headingebb.meas,'.')
    % subplot(212)
    % x = cos(headingebb.meas*pi/180);
    % y = sin(headingebb.meas*pi/180);
    % plot(x,y,'.')
    % %meanx = nanmean(x);
    % %meany = nanmean(y);
    %
    % dir = atan2(y,x)*180/pi;
    %
    % %headingebb.mean = nanmean(dir);
    % %headingebb.varmean = nanstd(dir);
    
    
    %% Get the mean angle and std using a histogram approach
    [N,degbins]=hist(headingebb.meas,360);
    
    myfigure('HistogramEbb')
    subplot(311)
    plot(degbins,N)
    
    %find the maximum
    [~,maxind] = max(N);
    maxdeg = degbins(maxind);
    %shift the histogram to be centered around zero
    shiftdir = headingebb.meas - maxdeg;
    %force angles to be between -180 and 180
    ind = find(shiftdir>180);
    shiftdir(ind) = shiftdir(ind) - 360;
    ind = find(shiftdir<-180);
    shiftdir(ind) = shiftdir(ind) + 360;
    
    subplot(312)
    plot(headingebb.yd,shiftdir,'.')
    
    
    %Get new histogram
    [N2,degbins2]=hist(shiftdir,360);
    subplot(313)
    plot(degbins2,N2)
    
    % Get the mean and standard deviation
    headingebb.mean = mean(shiftdir) + maxdeg;
    headingebb.varmean = std(shiftdir);
    
    %% Plot results
    myfigure('MeanHeading_EbbFld')
    set(gcf,'Position',[680  50 1071 928])
    subplot(211)
    hold on
    plot(yd,heading.meas,'k')
    plot(headingfld.yd,headingfld.meas,'.')
    plot([min(yd) max(yd)],[headingfld.mean headingfld.mean],'r')
    plot([min(yd) max(yd)],(headingfld.mean+headingfld.varmean)*[1 1],'--r')
    plot([min(yd) max(yd)],(headingfld.mean-headingfld.varmean)*[1 1],'--r')
    title('Flood')
    axis('tight')
    
    subplot(212)
    hold on
    plot(yd,heading.meas,'k')
    plot(headingebb.yd,headingebb.meas,'.')
    
    plot([min(yd) max(yd)],[headingebb.mean headingebb.mean],'r')
    plot([min(yd) max(yd)],(headingebb.mean+headingebb.varmean)*[1 1],'--r')
    plot([min(yd) max(yd)],(headingebb.mean-headingebb.varmean)*[1 1],'--r')
    title('Ebb')
    axis('tight')
end    
    
%% Probability distributions
if options.cdf == 1
    
    if options.fldebb == 0
        error('cdf part of code is only set up now if fldebb flag is equal to 1')
    end
    if isfield(options,'u_cutin')
        tspdind = find(abs(spdinterp) >options.u_cutin);
    else
        tspdind=1:length(mtime);
    end
    tfldspdind = intersect(tspdind,tfldind);
    headingfld.tspdind = tfldspdind;
    tebbspdind = intersect(tspdind,tebbind);
    headingebb.tspdind = tebbspdind;
    
    % plot(yd(tfldspdind),heading.meas(tfldspdind),'c.')
    % plot(yd(tebbspdind),heading.meas(tebbspdind),'r.')
    % legend('meas','fld','ebb','fld>cutoff','ebb>cutoff')
    
    
    myfigure('Speed_Pitch')
    set(gcf,'Position',[680 148 1135 825])
    subplot(211)
    plot(yd,spdinterp)
    hold on
    plot(yd(tspdind),spdinterp(tspdind),'r.')
    xlim([options.tlim(1) options.tlim(2)])
    subplot(212)
    plot(yd,pitch.std)
    hold on
    plot(yd(tspdind),pitch.std(tspdind),'r.')
    xlim([options.tlim(1) options.tlim(2)])
    
    myfigure('Speed_Heading')
    set(gcf,'Position',[680 148 1135 825])
    subplot(211)
     plot(yd,spdinterp)
    hold on
    plot(yd(tspdind),spdinterp(tspdind),'r.')
    xlim([options.tlim(1) options.tlim(2)])
    subplot(212)
    plot(yd,heading.std)
    hold on
    plot(yd(tfldspdind),heading.std(tfldspdind),'r.')
    plot(yd(tebbspdind),heading.std(tebbspdind),'g.')
    xlim([options.tlim(1) options.tlim(2)])
    
    %%
    cdfstats = calc_cdfstats('pitch',yd(tspdind),pitch.std(tspdind),cdfoptions);
    pitch = catstruct(pitch,cdfstats);
    cdfstats = calc_cdfstats('roll',yd(tspdind),roll.std(tspdind),cdfoptions);
    roll = catstruct(roll,cdfstats);
    cdfstats = calc_cdfstats('heading',mtime(tspdind),heading.std(tspdind),cdfoptions);
    heading = catstruct(heading,cdfstats);
    if ~isempty(tfldspdind)
        cdfstats = calc_cdfstats('headingfld',yd(tfldspdind),heading.std(tfldspdind),cdfoptions);
        headingfld = catstruct(headingfld,cdfstats);
    end
    
    cdfstats = calc_cdfstats('headingebb',yd(tebbspdind),heading.std(tebbspdind),cdfoptions);
    headingebb = catstruct(headingebb,cdfstats);
    
    
end

%% save
outfile = [fileinfo.outdir fileinfo.attstats];
disp(['Saving data to ',outfile])

Comments{1} = 'Statistics of attitude parameters';
Comments{end+1} = '"yd" is a time vector corresponding to the year day of 2012';
Comments{end+1} = 'Structures called "pitch","roll","heading" contain the output';
Comments{end+1} = 'meas (measured quantity -- an ensemble average)';
Comments{end+1} = 'mean (mean ensemble value)';
Comments{end+1} = 'varmean (standard deviation of the mean value)';
Comments{end+1} = 'std (standard deviation within an ensemble)';
Comments{end+1} = 'prct (percentiles to estimate std at)';
Comments{end+1} = 'prct_stdvals (standard deviations at corresponding percentiles)';
Comments{end+1} = 'degbins (the bins used in the computation of the histogram)';
Comments{end+1} = 'dist (the distribution of the standard deviations)';
Comments{end+1} =  'cdf (the cumulative distribution function)'; 
Comments{end+1} = 'nobs (number of observations cdf values are based on';

if options.fldebb == 0
    save(outfile,'pitch','roll','heading','yd','Comments')
else
    save(outfile,'pitch','roll','heading','headingfld','headingebb','yd','Comments')
end
%% Save metadata
metadata.progname=[mfilename('fullpath')];
metadata.date = datestr(now);
metadata.paramfile = fileinfo.paramfile;
save(outfile,'metadata','-append')


end

function stats = calc_cdfstats(varname,time,stddata,cdfoptions)
stats.nobs = length(time); %Number of observations statistics are based on

stats.prct = cdfoptions.percentiles;
histbinsize = cdfoptions.histbinsize;

left = 0.06;
right = 0.94;

myfigure([varname,'_std'])
set(gcf,'Position',[ 87 56 1063 917])
subplot('Position',[0.06 0.6 right-left 0.35])
plot(time,stddata,'.')
xlim([min(time) max(time)])
xlabel('year day')
ylabel(['\sigma ',varname,' (deg)'])

% Histogram
stats.degbins = [0:histbinsize:max(stddata)];
n = hist(stddata,stats.degbins);
stats.dist = n/length(time)*100;

width = 0.4;
subplot('Position',[left 0.1 width 0.40])
plot(stats.degbins,stats.dist)
xlabel(['\sigma ',varname,' (deg)'])
ylabel('occurance (%)')

%Cumulative distribution
for ii = 1:length(n)
    stats.cdf(ii) = sum(stats.dist(1:ii));
end

subplot('Position',[right-width 0.1 width 0.40])
plot(stats.degbins,stats.cdf)
xlabel(['\sigma ', varname,' (deg)'])
ylabel('cumulative occurance (%)') 


% Pick off percentages (do a linear fit because interp1 gives errors)
for ii = 1:length(stats.prct)
    ind = find(stats.cdf<stats.prct(ii));
    if isempty(ind)
        linpts = [1 2];
    else
        linpts = [ind(end) ind(end)+1];
    end
    pfit = polyfit(stats.degbins(linpts),stats.cdf(linpts),1);
    stats.prct_stdvals(ii) = (stats.prct(ii) - pfit(2))/pfit(1);
   % pitch_prctvals(ii) = interp1(pitch_cdf',degbins',prct(ii),'linear')
end


hold on
grid on
plot(stats.prct_stdvals,stats.prct,'ro')
for ii = 1:length(stats.prct)
    text(stats.prct_stdvals(ii)+0.25,stats.prct(ii),...
        [num2str(stats.prct(ii)),'th percentile, '...
        '\sigma = ',num2str(stats.prct_stdvals(ii))])
end

end
