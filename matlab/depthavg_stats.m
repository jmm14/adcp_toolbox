function dataavg = depthavg_stats(data,zlim)
% Calculates the depth average of Polagye Tools output
% data: A struct containing all the data
% zlim: The limits over which to compute the average
% dataavg: A structure containing the average quantities
%
% Justine McMillan
% October 2nd, 2012

zmin = zlim(1);
zmax = zlim(2);
[~,minbin] = min(abs(data.z-zmin))
[~,maxbin] = min(abs(data.z-zmax))

% Initialize
dataavg = data;

dataavg.slack_t = nanmean(data.slack_t(:,minbin:maxbin),2);

vars = {'ens_s','ens_d','ens_u','ens_v','ens_w','ens_d_PA',...
    'pdf_v'};
for ii = 1:length(vars)
    var = vars{ii};
    field = getfield(data,var);
    avgfield = calc_depthavg(field,data.z,[zmin zmax],2);
    dataavg = setfield(dataavg,var,avgfield);
end
dataavg.jpdf_vd = calc_depthavg_3D(data.jpdf_vd,data.z,[zmin zmax],3);

%z
dataavg.z = calc_depthavg(data.z,data.z,[zmin zmax]);

%structures
dataavg.all = calc_depthavg_struct(data.all,data.z,[zmin zmax]);
dataavg.ebb = calc_depthavg_struct(data.ebb,data.z,[zmin zmax]);
dataavg.fld = calc_depthavg_struct(data.fld,data.z,[zmin zmax]);

names = fieldnames(data.cycle);
for ii = 1:length(names)
    name = names{ii};
    field = getfield(data.cycle,name);
    avgfield = calc_depthavg(field,data.z,[zmin zmax],2);
    dataavg.cycle = setfield(dataavg.cycle,name,avgfield);
end

names = fieldnames(data.res_s)
for ii = 1:length(names)
    name = names{ii};
    if strcmp(name,'t')==0
        field = getfield(data.res_s,name);
        avgfield = calc_depthavg(field,data.z,[zmin zmax],2);
        dataavg.res_s = setfield(dataavg.res_s,name,avgfield);
    end
end
