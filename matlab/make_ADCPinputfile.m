function make_ADCPinputfile(varargin)
% This function asks the user what the parameters are for an ADCP and makes
% a .m file that will be used as an input for analysis.
% Input:
% filename = filename to save inputs to (if empty will save a default
%                       filename)
%
% Justine McMillan
% Sept 28th, 2012


stn = input('Station number: ');
datadir = input('Directory of processed data: ','s');
ADCPfile = input('ADCP file name: ','s');
rbrfile = input('RBR file name (set to 0 if no rbr sensor): ','s');
outputdir = input('Path to output directory:','s');
flowfile = input('Flow file base name: ','s');
ancfile = input('Acillary file base name: ','s');
yr = input('Year of measurements: ');		
tmin = input('Minimum time to include (year day): ');			
tmax = input('Maximum time to include (year day): ');			
flooddir = input('Approximate direction of the flood tide (relative to true north, CCW is positive): ');		
lat = input('Latitude of ADCP: ');				
lon = input('Longitude of ADCP: ');			
dabADCP = input('Depth of ADCP above bottom (m): ');			
dabPS = input('Depth of pressure sensor above bottom (m): ');	
notes = input('Any notes to add to the file: ','s');

%save to a .m file
if nargin == 1
    filename = varargin{1};
else
    today = clock;
    filename = ['Stn',num2str(stn),'_ADCPinputs_',num2str(today(1)),...
        num2str(today(2),'%02d'),num2str(today(3),'%02d'),'.m']
end

fid = fopen(filename,'w');
fprintf(fid,'%% Created by:  \n',datestr(now));
fprintf(fid,'%%  Justine McMillan \n',datestr(now));
fprintf(fid,'%%  %s \n',datestr(now));
fprintf(fid,'%%  Notes: %s \n\n',notes);

fprintf(fid,'stn = %d; \t\t %% station number\n',stn);

fprintf(fid,'datadir = 	%c%s%c; \t %%path to raw data files \n',char(39),datadir,char(39));
fprintf(fid,'ADCPfile = %c%s%c; \t %%name of ADCP file \n',char(39),ADCPfile,char(39));
fprintf(fid,'rbrfile = 	%c%s%c; \t %%name of rbr data file (set to zero if there is no rbr data) \n',char(39),rbrfile,char(39));
fprintf(fid,'outputdir = %c%s%c; \t %%path to output directory\n',char(39),outputdir,char(39));
fprintf(fid,'flowfile =  %c%s%c; \t %%name of output file with Flow data\n',char(39),flowfile,char(39));
fprintf(fid,'ancfile = 	%c%s%c; \t %%name of output file with Ancillary data\n',char(39),ancfile,char(39));
fprintf(fid,'yr = %d; \t %%year of measurements\n',yr);
fprintf(fid,'tmin = %f; \t %%tmin (year day)\n',tmin);
fprintf(fid,'tmax = %f; \t %%tmax (year day)\n',tmax);
fprintf(fid,'flooddir= %f; \t %%Flood direction (relative to true north, CCW is positive)\n',flooddir);
fprintf(fid,'lat = %f; \t %%latitude\n',lat);
fprintf(fid,'lon = %f; \t %%longitude\n',lon);
fprintf(fid,'dabADCP = %f; \t %%depth above bottom of ADCP\n',dabADCP);
fprintf(fid,'dabPS = %f; \t %%depth above bottom of pressure sensor	\n',dabPS);


fclose(fid);