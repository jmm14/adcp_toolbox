function make_table_row(variable,data,formats,fid)
%% function make_table_row(variable,data,formats,fid)
% Creates a row in a table from the input "data". Each entry in the row
% corresponds to a different site. 
%
% Inputs:
%   variable: variable name (ie. 's_mean')
%   formats: cell array of formatting strings
%   data: row of data to put in tex file
%   fid: file ID from "fopen" called before function
%
% Justine McMillan
% June 5th, 2012


[description format] = get_desc_form(variable,formats);

fprintf(fid,'%s',description);
fprintf(fid,([format,'\\\\ \n']),data);

end

function [description format]= get_desc_form(variable,formats)
%% function [description format]= get_desc_form(variable,formats)
% Gets the row label for a table and the format of the data. 
% 
% Variable: A single variable
% Formats: An array of strings containing the formats to print for the
% table

if isequal(char(variable),'s_mean')
    description = 'Mean speed (m/s) & ';
    format = formats{1};
elseif isequal(char(variable),'s_max')
    description = 'Maximum speed (m/s) & ';
    format = formats{1};
elseif isequal(char(variable),'s_asym')
    description = 'Ebb/flood asymmetry & ';
    format = formats{1};
elseif isequal(char(variable),'P_mean')
    description = 'Mean power density (kW/m$^2$) & ';
    format = formats{1};
elseif isequal(char(variable),'P_asym')
    description = 'Power asymmetry & ';
    format = formats{1};
elseif isequal(char(variable),'d_mean')
    description = 'Principal direction ($^\circ \mbox{ CW } from \mbox{ N}$) & ';
    format = formats{3};
elseif isequal(char(variable),'d_sigma')
    description = 'Directional deviation ($^\circ$) & ';
    format = formats{3};
elseif isequal(char(variable),'d_asym')
    description = 'Directional asymmetry ($^\circ$) & ';
    format = formats{3}; 
elseif isequal(char(variable),'d_fld_mean')
    description = 'Direction -- Flood ($^\circ$) & ';
    format = formats{3}; 
elseif isequal(char(variable),'d_ebb_mean')
    description = 'Direction -- Ebb ($^\circ$) & ';
    format = formats{3}; 

elseif isequal(char(variable),'d_fld_sigma')
    description = 'Std. dev. -- Flood ($^\circ$) & ';
    format = formats{3}; 
    
elseif isequal(char(variable),'d_ebb_sigma')
    description = 'Std. dev. -- Ebb ($^\circ$) & ';
    format = formats{3}; 
elseif isequal(char(variable),'h')
    description = 'Mean depth (m) & ';
    format = formats{1}; 
elseif isequal(char(variable),'lon')
    description = 'Longitude ($^\circ$) & ';
    format = formats{2};
elseif isequal(char(variable),'lat')
    description ='Latitude ($^\circ$) & ';
    format = formats{2}; 
elseif isequal(char(variable),'dur')
    description = 'Duration (days)& ';
    %format = formats{1};
    format = formats{3};
elseif isequal(char(variable),'ens_t')
    description = 'Ensemble interval (min) & ';
    %format = formats{1};
    format = formats{3};
elseif isequal(char(variable),'zlevel')
    description = 'Vertical level of measurement (m) & ';
    format = formats{1};
elseif isequal(char(variable),'zmin')
    description = 'Minimum height for depth average (m) & ';
    format = formats{1};
elseif isequal(char(variable),'zmax')
    description = 'Maximum height for depth average (m) & ';
    format = formats{1};
  
else
    disp(['no description for ',var])
end
end
