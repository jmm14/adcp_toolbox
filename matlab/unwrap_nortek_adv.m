function [newVel,maskInd,dy,indDF]=unwrap_nortek_adv(BEAM_VEL,nFilt,vAmb)
% function unwrap_nortek_adv(BEAM_VEL,thres)
% Ambuity velicity is best applied to BEAM_VEL
%thres=0.8; %  proportion of vAmb to use as threshold
if nargin<3
   vAmb=(-min(BEAM_VEL)+max(BEAM_VEL))/2; % estimate ambuiguity vel
end %nFilt=44; number of points to smooth.. I like to use no more than 1-2
%seconds... filter is 2*nFilt+1
% Output
%   newVel: replaced (unwrapped) velocities
%   maskInd: mx3 matric of 1/0 for whether the data was replaced or not.

%%
disp(vAmb)
if length(vAmb)==1
   vAmb=vAmb*[1 1 1];
end
    %%

newVel=BEAM_VEL;
df=diff(BEAM_VEL);
% df=[df; 0 0 0];
y = movmedian(BEAM_VEL,[nFilt nFilt]);
dy=y-BEAM_VEL;


maskInd=zeros(size(BEAM_VEL));
for jj=1:3
    indR=[];
    indP=find(BEAM_VEL(:,jj)>0);
    indN=find(BEAM_VEL(:,jj)<0);
    ind=find(dy(:,jj)>vAmb(jj));
    %ind=find(df(:,jj)>vAmb(jj)); %ind=ind+1;
    ind=intersect(indN,ind);
    newVel(ind,jj)=BEAM_VEL(ind,jj)+2*vAmb(jj);
    
    indR=union(indR, ind);
    ind=find(dy(:,jj)<-vAmb(jj));
    %ind=find(df(:,jj)<-vAmb(jj)); %ind=ind-1;
    ind=intersect(indP,ind);
    newVel(ind,jj)=BEAM_VEL(ind,jj)-2*vAmb(jj);
    indR=union(indR, ind);
    
    maskInd(indR,jj)=1;
    %     ind=find(df(:,jj)>thres*vAmb); % positive jump
   
end
%% plot
figure
t=1:1:length(BEAM_VEL);
for jj=1:3
    ax(jj)=subplot(3,1,jj);
    plot(t,BEAM_VEL(:,jj),'color',.7*[1 1 1]); hold on
    plot(t,y(:,jj),'r-')
    % plot(t,newVel(:,jj),'k-')
end

figure
plot(BEAM_VEL,df./vAmb,'.')
indDF=find(df./vAmb>1);

ylabel('dV v_{ambiguity}^{-1} []')
xlabel('Beam vel [m/s]')
title('Mov median filter was applied to find dV')

