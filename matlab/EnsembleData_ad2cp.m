function [adcp_out] = EnsembleData_ad2cp(t_ens, adcp,options)
%Description: ensemble average (time and space) measurements that are in
% the structure "adcp"
%
%Inputs:
%   - t_ens: ensemble time in seconds
%   - adcp: input structure
%   - options: structure with some options (i.e. which method to use -- TimeSearch or NumPoints)
%
% Outputs:
%    - adcp_out: same format as input file (with std dev included)
%
% Justine McMillan
% Oct 28, 2013
%
% - April 22, 2014 (Modified EnsembleData_FlowFiles.m)
% - Jun 13, 2014: Detrended the data before computing the standard
%                   deviations
% - July 9, 2014: Modified to deal with bursts (NEEDS A MAJOR CLEANUP!!)
% - July 10, 2014: Cleaned up and modified it to work with matrices
%- Apr 16, 2015: Modified to add 3D fields
%
% Note: Probably only works for scalars (i.e. Attitude parameters)

%%
disp_linebreak('_')
disp('COMPUTING AD2CP ENSEMBLES')
if ~isfield(options,'method'), options.method = 'TimeSearch'; end
if ~isfield(options,'showoutput'), options.showoutput = 1; end

ens_tol = 0.1;  % allow for 10% of the points to be missing, and still compute the ensemble
SEC2DAY = 1/(24*3600);

%% Set up time vectors (depends on method)

if strcmp(options.method,'TimeSearch') == 1
    error('Not Set up yet')
%     if options.showoutput
%         disp('  Method: Time Search')
%     end
%     
%     
%     if round(t_ens/60)>60
%         warning(['Check to make sure ensemble averaging is done correctly.',...
%             'Originally created for much shorter ensembles'])
%     end
%     
%     % Set up time vector -- want timestamps to be on even minutes
%     dt = mean(diff(adcp.mtime))/SEC2DAY; % delta t in seconds
%     dt_one = (adcp.mtime(2)-adcp.mtime(1))*24*3600;
%     [yr,mm,dd,hr,mins,sec] = datevec(adcp.mtime(1));
%     adcp_out.mtime = [datenum(yr,mm,dd,hr,0,0):t_ens/3600/24:adcp.mtime(end)]; %start on the hour -- leading values will be removed at the end
%     
%     if options.showoutput
%         nensapprox = (t_ens/dt);
%         disp(['   Ensemble interval: ',num2str(t_ens/60),' mins',...
%             ' (Approx ',num2str(round(nensapprox)),' points)'])
%     end
    
elseif strcmp(options.method,'Burst')
    if options.showoutput
        disp('   Method: Averaging Bursts')
    end
    
    dt = diff(adcp.mtime)*24*3600; % in seconds
    
    dt_one = mean(dt(find(dt<60)));
    
    
    ind = find(dt>dt_one*2); %Find indices where dt changes by a lot (ie. burst intervals)
    nensapprox = round(ind(end)-ind(end-1));
    
    if options.showoutput
        disp(['   Ensemble interval approximately: ',...
            num2str(nensapprox*dt_one/60),' mins',' (Approx ',num2str(round(nensapprox)),...
            ' points)'])
    end
    

    for ii = 1:length(ind)+1
        if ii == 1                   %first interval
            avgpts = 1:ind(1);
        elseif ii == length(ind)+1   %last interval
            avgpts = ind(ii-1)+1:length(adcp.mtime);
        else                         %mid intervals
            avgpts = ind(ii-1)+1:ind(ii);
        end
        adcp_out.mtime(ii) = mean(adcp.mtime(avgpts));
        
        t_ens_all(ii) = (adcp.mtime(avgpts(end)) - adcp.mtime(avgpts(1)))*24*3600;
        
    end
    
elseif strcmp(options.method,'NumPoints')
    error('Method not set up yet -- just copy frome ensemble_data.m?')
else
    error(['Invalid Method'])
end
adcp_out.mtime = adcp_out.mtime';
%% Initialize
ig  = 0;
adcp_in.mtime = adcp.mtime;
adcp = rmfield(adcp,'mtime');
fields = fieldnames(adcp);
%nbins = size(data.east_vel,2);
nbins = 1; %for scalars
for ii = 1:length(fields)
    fieldvar =  fields{ii};
    fieldstd = [fields{ii},'_std'];
    field = adcp.(fieldvar);
    c=class(field);
    nd = ndims(field);
    if strcmp(c,'double') || strcmp(c,'single')
        [~,nbins] = size(adcp.(fieldvar));
        if nd == 1 | nd == 2
            adcp_out.(fieldvar) = NaN*ones(length(adcp_out.mtime),nbins);
            adcp_out.(fieldstd) = NaN*ones(length(adcp_out.mtime),nbins);
        elseif nd == 3
            error('Need to modify code for 3D field')
%             adcp_out.(fieldvar) = NaN*ones(4,length(adcp_out.mtime));
%             adcp_out.(fieldstd) = NaN*ones(nbins,4,length(adcp_out.mtime));
        end
    else
        adcp_in.(fieldvar) = adcp.(fieldvar);
        adcp = rmfield(adcp,fieldvar);
        disp(['   Ignoring variable: ''',fieldvar,''''])
        ig = ig+1;
        fields_ignore{ig} = fieldvar;
        
    end
    
end
fields = fieldnames(adcp);

%% Look through time vector - Compute means and standard deviations
ind_disp = ceil(length(adcp_out.mtime)*[0.1:0.1:1]);

for ii = 1:length(adcp_out.mtime)
    if find(ind_disp==ii)
        if ii == ind_disp(1)
            fprintf(1,'%s','   Looping')
        end
        
        fprintf(1,'%s%3d%s','...',round(ii/length(adcp_out.mtime)*100),'%')
        
        if ii == ind_disp(end)
            fprintf(1,'\n')
        end
    end
    
    if strcmp(options.method,'TimeSearch')
        tstart = adcp_out.mtime(ii) - 1/2*(t_ens*SEC2DAY)
        tend   = adcp_out.mtime(ii) + 1/2*(t_ens*SEC2DAY)

        ind_t = find(adcp_in.mtime>=tstart & adcp_in.mtime<tend);
    elseif strcmp(options.method,'Burst')
        if ii == 1                   %first interval
            ind_t = 1:ind(1);
        elseif ii == length(ind)+1   %last interval
            ind_t = ind(ii-1)+1:length(adcp_in.mtime);
        else                         %mid intervals
            ind_t = ind(ii-1)+1:ind(ii);
        end
    end
    if length(ind_t)<nensapprox*(1-ens_tol) %If there aren't enough points
        adcp_out.mtime(ii) = NaN;
    else
        % loop through data fields
        for jj = 1:length(fields)
            fieldvar =  fields{jj};
            fieldstd = [fields{jj},'_std'];
            if strcmp(fieldvar,'mtime') == 0
                eval(['field = adcp.',fieldvar,';']);
                if sum(isnan(field(ind_t,:)))>0%0.25*length(field(ind_t,:))
                    disp('Dropping burst -- more than 0% of measurements are erroneous')
                    continue
                end
                
                nd = ndims(field);
                
                if nd == 1 || nd == 2
                
                    adcp_out.(fieldvar)(ii,:)    = nanmean(field(ind_t,:));%Compute mean
                    adcp_out.(fieldstd)(ii,:) =  nanstd(detrend(field(ind_t,:)));%Compute std dev
                else
                    error('Too many dimensions')
                end %if
                    
                % end
            end %if
        end %jj
    end %if
    
    clear ind_t
end
%% Display some info to the screen
ind_t2 = find(~isnan(adcp_out.mtime));

if options.showoutput
    if strcmp(options.method,'TimeSearch')
        

        t_drop1 = (adcp_out.mtime(ind_t2(1)) - adcp_in.mtime(1) - 1/2*(t_ens*SEC2DAY))*24*60;
        t_drop2 = (adcp_in.mtime(end) - adcp_out.mtime(ind_t2(end)) - 1/2*(t_ens*SEC2DAY))*24*60;
        
        disp(['   Dropped ',num2str(t_drop1),' mins of data at the start of file'])
        disp(['   Dropped ',num2str(t_drop2),' mins of data at the end of file'])
    else
         ind_drop = find(isnan(adcp_out.mtime))';
         for ii = ind_drop
             if ii == 1                   %first interval
                npts = length(1:ind(1));
            elseif ii == length(ind)+1   %last interval
                npts = length(ind(ii-1)+1:length(adcp_in.mtime));
            else                         %mid intervals
                npts = length(ind(ii-1)+1:ind(ii));
            end
             disp(['   Dropped burst number ',num2str(ii),' of ',...
                 num2str(length(ind)+1 ),' (',num2str(npts),' points)'])
         end
    end
    

end

%% Trim Nans from the front and end
adcp_out.mtime = adcp_out.mtime(ind_t2(1):ind_t2(end));

for ii = 1:length(fields)
    fieldvar =  fields{ii};
    fieldstd = [fields{ii},'_std'];
    nd = ndims(adcp_out.(fieldvar));
    if nd == 1 || nd == 2
        adcp_out.(fieldvar) = adcp_out.(fieldvar)(ind_t2(1):ind_t2(end),:);
        adcp_out.(fieldstd) = adcp_out.(fieldstd)(ind_t2(1):ind_t2(end),:);
    end
end

%% add fields that were ignored in averaging
% for ii = 1:length(fields_ignore)
%     adcp_out.(fields_ignore{ii}) = adcp_in.(fields_ignore{ii});
% end

disp_linebreak('_')
