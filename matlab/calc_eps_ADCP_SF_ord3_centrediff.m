function [eps,yint,R2,D]=calc_eps_ADCP_SF_ord3_centrediff(vp,z,options)
% calculate the dissipation rate from an ADCP using a THIRD OEDER
% structure function approach
%
% Justine McMillan
% Feb 8, 2016



if ~isfield(options,'figure'); options.figure = 1; end
if ~isfield(options,'theta');  options.theta = 20; end
if ~isfield(options,'C3');    options.C3 = -4/5;  end

dz = mean(diff(z));
dr = dz/cosd(20);

if ~isfield(options,'nrmax');
    options.nrmax = 7;
    disp(['Max r: ',num2str(2*options.nrmax*dr)])
end

r = 2*dr*[1:options.nrmax];
nr = length(r);

%% initialize
nz = length(z);
D = NaN*ones(nz,nr);
eps = NaN*ones(nz,1);
yint = NaN*ones(nz,1);
R2 = NaN*ones(nz,1);

%% r vector
% ind_r = [0:floor(options.nrmax/2)+1]; % relative to central point
% r = 2*dr*ind_r


%% for each height, compute structure functions, epsilon, noise
for zz = 5:nz-5 
    for rr = 1:nr
        binU = zz+rr;
        binL = zz-rr;
        vdiff = vp(:,binU)-vp(:,binL); % Centre difference
        D(zz,rr) = nanmean((vdiff.^3));
    end
      
     % least squares fit
    ind = ~isnan(D(zz,:));

    xi = r(ind);
    yi = D(zz,ind);
    
    switch options.fittype
        case 'polyfitZero'
            P = polyfitZero(xi,yi,1);
        case 'polyfit'
            P = polyfit(xi,yi,1);
    end
    
    % calculate eps
    eps(zz) = P(1)/options.C3;
    
    % calculate intercept
    yint(zz) = P(2);
    
    % R2
    fi = polyval(P,xi);
    R2(zz) = 1-sum((yi-fi).^2)/sum((yi-mean(yi)).^2);
    
   
    
    if options.figure && options.Uref>0.7
        if zz == 17
        figure(3),clf
        plot(r,D(zz,:),'o')
        hold all
        plot([0:1:10],P(1)*[0:1:10]+P(2),'k')
        title({['z = ',num2str(z(zz)),' m,   U = ',num2str(options.Uref),' m/s'],...
                    ['\epsilon = ', num2str(eps(zz)),' W/kg, R^2 = ',num2str(R2(zz))]})
%         pause
        end
    end
end

%%
if options.figure && options.Uref>0.7
    figure(2),clf
    subplot(121)
    semilogx(eps, z(1:nz))
    ylabel('z')
    xlabel('\epsilon')

    subplot(122)
    plot(yint,z(1:nz))
    xlabel('\sigma_N')
end