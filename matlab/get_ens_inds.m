function [ind_start,ind_end,npts]=get_ens_inds(tvec,tens,fs)
% gets the indices corresponding to the start and end of ensembles. Puts
% the central time on an even multiple of tvec
% JMM
% Feb 9, 2016

nens_min = tens*fs; % ensemble length in minutes

tS = round2(tvec(1),tens);
tE = round2(tvec(end),tens);

tvec_ens = [tS:tens:tE];
% datestr(tvec_ens/24/60)
nens = length(tvec_ens);

ind_start = NaN*ones(1,length(nens));
ind_end = NaN*ones(1,length(nens));
npts = NaN*ones(1,length(nens));

for ii = 1:nens
    ind = find(tvec>=tvec_ens(ii)-tens/2 & tvec<tvec_ens(ii)+tens/2);
    if isempty(ind)
        tvec_ens(ii) = NaN;
        continue
    end
    if mean(diff(ind))~=1
        error('Something wrong. Not continuous?')
    end
    ind_start(ii) = ind(1);
    ind_end(ii) = ind(end);
    npts(ii) = length(ind);
    if npts(ii)<0.95*nens_min
        disp(['ii = ',num2str(ii),' - Only ',num2str(npts(ii)),' points - dropping burst'])
        tvec_ens(ii) = NaN;
    end
    disp_percdone(ii,nens,10,'red')
end

% remove nans
ind = ~isnan(tvec_ens);
ind_start = ind_start(ind);
ind_end = ind_end(ind);
npts = npts(ind);
