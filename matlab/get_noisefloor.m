function [noisefloor specdata] = get_noisefloor(Sff,f,flim,U,Ulim,z, zlim,options)
% gets the noise level of the spectra
%
% JMM
% Date?
% Dec 8, 2015 -- Added remove outliers option
% Jan 20, 2016 - Changed plot
% Mar 21, 2017 -- Added optional output with spectra used

if ~isfield(options,'figure')
    options.figure = 1;
end
if ~isfield(options,'rmoutliers')
    options.rmoutliers = 0;
end
if ~isfield(options,'subtitle')
    options.subtitle = '';
end
if ~isfield(options,'manValue')
    options.manValue = NaN;
end

% get the limits
ind_f = find(f>=flim(1) & f <=flim(2));
ind_z = find(z>=zlim(1) & z <=zlim(2));
ind_U = find(U>=Ulim(1) & U <=Ulim(2));

% get the averaged spectra
Sff_avg_z = nanmean(Sff(:,:,ind_z),3);
if options.rmoutliers == 0
    Sff_avg = nanmean(Sff_avg_z(:,ind_U),2);
elseif options.rmoutliers == 1
    noise_all = nanmean(Sff_avg_z(ind_f,ind_U),1);
    [~,ind_min] = min(noise_all);
    [~,ind_max] = max(noise_all);
    ind_bad = ind_U([ind_min ind_max]);
    ind_keep = setdiff(ind_U,ind_bad);
    Sff_avg = nanmean(Sff_avg_z(:,ind_keep),2);
elseif options.rmoutliers == 2 %remove cases which are very different from manufacturer's value. 
    noise_all = nanmean(Sff_avg_z(ind_f,ind_U),1);
    ind_bad_tmp = find(noise_all>1.5*options.manValue);
    ind_bad = ind_U(ind_bad_tmp);
    ind_keep = setdiff(ind_U,ind_bad);
    Sff_avg = nanmean(Sff_avg_z(:,ind_keep),2);
end

noisefloor = nanmean(Sff_avg(ind_f));

% Spectra data to save (what is plotted below)
specdata.Sff_avg_z = Sff_avg_z(:,ind_U);
specdata.Sff_avg = Sff_avg;
specdata.f = f;
specdata.U = U;
specdata.ind_U = ind_U;
specdata.ind_keep = ind_keep;

%
if options.figure
   myfigure('noisefloor')
   
   % plot reference speed
   subplot(5,1,5)
   plot(U),hold all
   plot(ind_U,U(ind_U),'r.')
   
   % plot all spectra
   subplot(5,3,[1,2,4,5,7,8,10,11])
   loglog(f,Sff_avg_z(:,ind_U),'color',0.6*[1 1 1])
   hold all
   loglog(f,Sff_avg,'k','linewidth',3)
   fill_transparent([f(ind_f(1)) f(ind_f(end))],get(gca,'Ylim'),'g')
   loglog(get(gca,'Xlim'),noisefloor*[1 1],'k')
   loglog(get(gca,'Xlim'),options.manValue*[1 1],'r')
%    loglog(get(gca,'Xlim'),median(noise_all)*[1 1],'g')
   xlim([f(2), f(end)])
   title_str = ['Noisefloor at ',num2str(mean(zlim),'%3.1f'),' m  = ',...
       num2str(noisefloor,'%4.2e'),',  ',...
       num2str(length(ind_z)),'-bin avg in z,   ',...
       num2str(length(ind_U)),'-bin avg in speed'];
   if ~isempty(options.subtitle)
       title_str = {title_str;[options.subtitle]};
   end
   title(title_str)
   ylim([1e-3 1e-2])
   if options.rmoutliers >0 & ~isempty(ind_bad)
        loglog(f,Sff_avg_z(:,ind_bad),'b','linewidth',2)
    end
   
%    [~,ind] = max(Sff_avg_z(2,ind_U))
%    loglog(f,Sff_avg_z(:,ind_U(ind)),'b','linewidth',2)
%    [~,ind] = min(Sff_avg_z(2,ind_U))
%    loglog(f,Sff_avg_z(:,ind_U(ind)),'b','linewidth',2)

    % plot distribution of noise levels
     subplot(5,3,[3:3:12])
     bins_h = [min(noise_all):2e-4:max(noise_all)];
     N = hist(noise_all,bins_h);
     b=bar(bins_h,N,'facecolor',0.6*[1 1 1],'edgecolor',[1 1 1]);
     hold all
     p(1)=plot(noisefloor*[1 1],get(gca,'ylim'),'--k');
     xlim(4.5e-3+[-2e-3 2e-3])
     pd = fitdist(noise_all','Normal');
     p(2)=plot(pd.mu*[1 1],get(gca,'ylim'),'--c');
     p(3)=plot(options.manValue*[1 1],get(gca,'ylim'),'--r');
%      p(3)=plot(median(noise_all)*[1 1],get(gca,'ylim'),'--m');
%      y=pdf(pd,bins_h);
%      plot(bins_h,y,'k')
      legend(p,'Mean','mean - normal','manValue')
%       if abs((noisefloor-pd.mu)/noisefloor)>0.1
%           error('too many outliers?')
%       end
     
    

    
end
   
    