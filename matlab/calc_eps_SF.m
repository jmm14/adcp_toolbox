function [eps,sigmaN,Rinfo] = calc_eps_SF(r,D,options)
% Calculate the dissipation rate from the second order structure function
%
% Justine McMillan
% Feb 18, 2022
%
% 2022-04-09: Initialized variables and skip processing if there is no good data
%             Also, allow for inputs to have nans
% 2024-05-30: Added calculations of MSPE (mean squared percent error) and
%             MAD (mean absolute deviation)
%             Removed R2 as output and added to Rinfo matrix


%% Mandatory input
if ~isfield(options,'order');
    error('Need to specify the order of SF')
end

%% Set defaults
if ~isfield(options,'figure'); options.figure = 1; end
if ~isfield(options,'Uref'); options.Uref = NaN; end
if ~isfield(options,'Const')
    switch options.order
        case 2
            options.Const = 2.0;
        case 3
            options.Const = -4/5;
    end
end

%% Initialize
eps = NaN;
sigmaN = NaN;
Rinfo.slope = NaN;
Rinfo.yint = NaN;
Rinfo.CI_yint = NaN*ones(1,2);
Rinfo.CI_slope =  NaN*ones(1,2);
Rinfo.R2 = NaN;
Rinfo.d_eps = NaN;
Rinfo.MSPE = NaN;
Rinfo.MAD = NaN;
Rinfo.npts = NaN;

%% fit good data to theoretical curves
indGood = intersect(find(~isnan(D)),find(~isnan(r)));
N = length(indGood);

if N>2

    if options.order == 2
        xi = r(indGood).^(2/3);
        yi = D(indGood);
    elseif options.order == 3
        xi = r(indGood);
        yi = D(indGood);
    end

    % regression (with CIs)
    X = [ones(size(xi')) xi'];
    [b,bint] = regress(yi',X);
    slope = b(2);
    yint = b(1);
    d_slope = 0.5*diff(bint(2,:));
    d_yint = 0.5*diff(bint(1,:));

    %R2
    fi = slope*xi+yint;
    R2 = 1-sum((yi-fi).^2)/sum((yi-mean(yi)).^2);
    
    % Mean square percent error (MSPE as in Zeiden et al, 2024)
    MSPE = 1/N*sum(((yi-fi)./fi).^2);
    
    % Mean absolute deviation 
    MAD = 1/N*sum(abs(yi-fi)./fi);

    if options.order == 2
        % Calculate epsilon
        if slope > 0
            eps = real((slope/options.Const)^(3/2)); % Would be imaginary if slope is negative
            d_eps = 3/2*eps/slope*d_slope;
        else
            eps = NaN;
            d_eps = NaN;
        end

        % Calculate sigmaN
        if yint > 0
            sigmaN = sqrt(yint/2);
        else
            sigmaN = NaN;
        end
    elseif options.order == 3
        eps = slope/options.Const;
        d_eps = 5/4*d_slope;
        sigmaN = NaN; % could make this the yint
    end

    % if d_eps/eps<2
    %     options.figure=1
    % end

    if options.figure
        figure(4),clf
        labels = {};

        p(1) = plot(r.^(2/3),D,'.k','markersize',12,'linewidth',2); hold all
        labels{1} = ['Data'];



        % errorbars -- standard error
        % errorbar(xi,yi,D_std/length(vp),'ko')

        % determine confidence limits
        y_hat = slope*xi+yint;
        n = length(xi);
        s2 = (yi-y_hat)*(yi-y_hat)'/(n-2); %variance of the residuals (eqn given by Keith)
        xfit = 0:0.01:max(xi);
        X0 = [ones(size(xfit')) xfit'];
        Y0 = X0*b;
        sigma_Y0 = NaN*ones(size(Y0));
        for ii = 1:length(xfit)
            sigma_Y0(ii) = sqrt(X0(ii,:)*inv(X'*X)*X0(ii,:)'*s2);
        end
        % confidence intervals (95% - assuming normal dist)
        yfit = Y0;
        ylow = Y0 - 1.96*sigma_Y0; % lower confidence limit
        yupp = Y0 + 1.96*sigma_Y0; % upper confidence limit

        % plot regression & CIs
        p(2) = plot(xfit,yfit,'b','linewidth',2);
        labels{2} = 'Fit';
        plot(xfit,ylow,'r-.');
        p(3) = plot(xfit,yupp,'r-.');
        labels{3} = '95% CI'; 

        % plot expected intercept
        if options.order == 2
            p(4) = plot(0,2*options.sigmaN_v^2,'*m');
        elseif options.order == 3
            p(4) = plot(0,0,'*m');
        end
        labels{4} = '2\sigma_V^2';

        title({['U = ',num2str(options.Uref),', n = ',num2str(length(xi))],...
            ['\epsilon = ',num2str(eps),'  \Delta\epsilon/\epsilon = ',num2str(d_eps/eps)],...
            ['\sigma_N (norm.) = ',num2str(sigmaN/options.sigmaN_v)]})
        %  ylim([-0.005 0.025])
        %     %R2
        %     fi = polyval(P,xi);
        %     R2(zz) = 1-sum((yi-fi).^2)/sum((yi-mean(yi)).^2);
        legend(p,labels,'location','northwest')
        xlabel('r^{2/3}')
        ylabel('D')
        pause

    end

    %% regression info
    Rinfo.slope = slope;
    Rinfo.yint = yint;
    Rinfo.CI_yint = bint(1,:);
    Rinfo.CI_slope = bint(2,:);
    Rinfo.R2 = R2; 
    Rinfo.d_eps = d_eps;
    Rinfo.MSPE = MSPE;
    Rinfo.MAD = MAD;
    Rinfo.npts = sum(~isnan(D));

else
    Rinfo.npts = sum(~isnan(D));
end

