function [vel1, enu]=transformADV(vel,hdr,heading,pitch,roll,statusbit)
% [beam enu]=transformADV(vel,coord,heading,pitch,roll,T,statusbit)
% FUnction that rotates the vel measured (in either beam or xyz vel) ADV
% Nortek velocities intoother coordinate systems
% Required inputs:
%   vel: Preferably velocities recorded in the xyz frame of reference, which is the
%       frame of reference the corr applies to (always preferable to sample
%       in this coordinate system). 
%       The function will handle 'BEAM' vel but not ENU
%   hdr: structure of info taken from the header file
%       hdr.coord: string of coordinate system of ADV as obtained in hdr file using getADVhdr.m 'BEAM' or 'XYZ' 
%       hdr.T: Transformation matrix included in the header file (.hdr)
%       hdr.Fs: sampling frequency in Hz
%   heading, pitch and roll: as recorded in the .SEN file in deg
%   statusbit: logical 1/0 depending if the Nortek Vector ADV legs are pointing up/down. 
%           For other nortek instruments the convention is the opposite
% Outputs: if coord 'XYZ', vel1=beam velocitiues, and enu velocities in same units as xyz
%           if coord 'BEAM', vel1=xyz velocitues
%=================================================
T=hdr.T; % Too lazy to fix the script below
Fs=hdr.Fs;
nP=length(vel);
if all(all(T==round(T))) % Needs to be scaled by 4096 when interger numbers, 
    %as sometimes the T matrix (perhaps due to an hardware version change) is not in  the correct units
    T = T/4096;   % Scale the transformation matrix correctly to floating point numbers
end

T_org = T;
if statusbit == 1,
   T(2,:) = -T(2,:);   
   T(3,:) = -T(3,:);   
end

% heading, pitch and roll are the angles output in the data in degrees
hh = pi*(heading-90)/180; clear heading;
pp = pi*pitch/180; clear pitch;
rr = pi*roll/180; clear roll


% Initialize matrices
enu=vel; enu(:)=NaN;
vel1=enu;
%% Choose coordinate
switch hdr.coord
    case{'XYZ'}
        beam=enu;% initializes to NaN
        xyz=vel;
    case{'BEAM'}
        xyz=enu; % intializes to NaN
        beam=vel;
    otherwise
        error('Cant handle ENU coordinate system yet');
end

% Make heading matrix
for jj=1:length(hh)
    H = [cos(hh(jj)) sin(hh(jj)) 0; -sin(hh(jj)) cos(hh(jj)) 0; 0 0 1];
% Make tilt matrix
    P = [cos(pp(jj)) -sin(pp(jj))*sin(rr(jj)) -cos(rr(jj))*sin(pp(jj));...
      0             cos(rr(jj))          -sin(rr(jj));  ...
      sin(pp(jj)) sin(rr(jj))*cos(pp(jj))  cos(pp(jj))*cos(rr(jj))];
    R = H*P*T; % z, y and x
    
    % JMM edit since heading, pitch and roll are provided at 8Hz, instead
    % of 1Hz as code was initially written for
    switch hdr.coord 
        case{'XYZ'}
            beam(jj,:) = transpose(T_org\xyz(jj,:)');
        case{'BEAM'}
            xyz(jj,:) = transpose(T_org*beam(jj,:)');
    end
    enu(jj,:) = transpose(R*beam(jj,:)');
%     for kk=1+(jj-1)*Fs:(jj-1)*Fs+Fs % heading, pitch,roll are sampled at 1Hz, velocities at Fs
%        switch hdr.coord 
%            case{'XYZ'}
%                beam(kk,:) = transpose(T_org\xyz(kk,:)');      
%            case{'BEAM'}
%                xyz(kk,:) = transpose(T_org*beam(kk,:)');
%        end
%        enu(kk,:) = transpose(R*beam(kk,:)');
%     end

    clear H P R;
    
    disp_percdone(jj,length(hh),25);
end
clear hh pp rr;

switch hdr.coord
    case{'XYZ'}
        vel1=beam;
    case{'BEAM'}
        vel1=xyz;     
end
   
if nP<length(vel1)
    vel1=vel1(1:nP,:);
    enu=enu(1:nP,:);

end
%enu=enu'; % transforming back into the same format of 3 columns of velocity data
%beam=beam'; 