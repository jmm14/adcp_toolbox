function [field_out, bin_max] = nan_AboveSurf(field_in,z,depth,varargin)
% function [field_out, bin_max] = nan_AboveSurf(field_in,z,depth,varargin)
%
% Nans all the values of field_in that are above the threshold 
% (fraction of surface height) 
%
% Justine McMillan
% Dec 5th, 2013
% Nov 22, 2024 -- use matrices to improve speed

if nargin == 3
    threshold = 0.95;
elseif nargin == 4;
    threshold = varargin{1};
end

field_out = field_in;
[NZ,NT] = size(field_in);

% for ii = 1:length(depth)
%     disp_percdone(ii,length(depth),1)
%     zmax = threshold*depth(ii);
%     [~ ,bin_max(ii)] = min(abs(z-zmax));
%     if z(bin_max(ii))>zmax
%         bin_max(ii) = bin_max(ii) - 1;
%     end
%     field_out(ii,bin_max(ii)+1:length(z)) = NaN;
% end

zMat = z*ones(1,NT);
depthMat = ones(NZ,1)*depth;
indBad = find(zMat>threshold*depthMat);
field_out(indBad) = NaN;

if 0
    figure,clf
    set(gcf,'Name','NanAboveSurf')
    subplot(211)
    pcolor(1:NT,z,field_in); shading flat; colorbar
    title('field\_in')
    
    subplot(212)
    tP = ([1:NT]'*ones(1,NZ))';
    scatter(tP(:),zMat(:),10,field_out(:)); shading flat; colorbar
    hold all
    plot(1:NT,threshold*depth,'k')
    title('field\_out')
%     pause
end
