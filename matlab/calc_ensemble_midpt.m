function ens_field = calc_ensemble_midpt(ens_t,time,field)

%t_ens is the ensemble interval
%time is a vector of the measured timestamps
%field is the measured field values (assumes matrix is time x bins)
% This function can be much improved -- works in only very limited cases. 
% Assumes that the modpoint is one of the measured time values, so if the
% number of enseble points is even it actually adds one more to make it odd
% and to make the average centered. 


t_ens=ens_t(2) - ens_t(1);
dt = time(2)-time(1);
enspts = t_ens/dt;
if enspts - round(enspts)>0.0001
    error('Error: mismatch between measurement time step and specified ensemble')
end

if mod(enspts,2) == 0
    enspts = enspts+1; 
end

enspts = round(enspts);
halfpts = (enspts-1)/2;
disp(['The number of points used in ensemble is ',num2str(enspts)])
disp(['Actually averaging over ',num2str((time(enspts+1)-time(1))*24*60),' minutes'])
tind = find(ens_t(1) == time)
for ii = 1:length(ens_t)
    tind-halfpts:tind+halfpts;
    subfield = field(tind-halfpts:tind+halfpts,:);
    ens_field(ii,:) = nanmean(subfield);
    tind = tind+enspts;
end

