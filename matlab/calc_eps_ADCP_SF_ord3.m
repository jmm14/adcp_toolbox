function [eps,yint,R2,D]=calc_eps_ADCP_SF_ord3(vp,z,options)
% calculate the dissipation rate from an ADCP using a THIRD OEDER
% structure function approach
%
% Justine McMillan
% Feb 8, 2016
% Aoril 7, 2016 - Changed centre diff code

if ~isfield(options,'figure'); options.figure = 1; end
if ~isfield(options,'theta');  options.theta = 20; end
if ~isfield(options,'C3');    options.C3 = -4/5;  end

dz = mean(diff(z));
dr = dz/cosd(20);

if ~isfield(options,'nrmax');
    options.nrmax = 7;
    disp(['Max r: ',num2str(2*options.nrmax*dr)])
end

%% initialize
nz = length(z);
D = NaN*ones(nz,options.nrmax);
eps = NaN*ones(nz,1);
yint = NaN*ones(nz,1);
R2 = NaN*ones(nz,1);

%% r vector
ind_r = [floor(options.nrmax/2):-1:-floor(options.nrmax/2)]; % relative to central point
r = dr*ind_r;
nr = length(r);
%% for each height, compute structure functions, epsilon, noise
for zz = floor(options.nrmax/2)+1:nz-floor(options.nrmax/2)-1
    %  nr = min([zz-1,options.nrmax,nz-zz]);
    
    binO = zz;
    vO = vp(:,binO);
    
    for rr = 1:nr
        binR = zz+ind_r(rr);
        
        vR = vp(:,binR);
        
        vdiff = vO-vR;
        
        D(zz,rr) = nanmean((vdiff.^3));
    end
    
    % least squares fit
    ind = ~isnan(D(zz,:));
    xi = r(ind);
    yi = D(zz,ind);
    P = polyfit(xi,yi,1);
    
    % calculate eps
    eps(zz) = P(1)/options.C3;
    
    % calculate intercept
    yint(zz) = P(2);
    
    % R2
    fi = polyval(P,xi);
    R2(zz) = 1-sum((yi-fi).^2)/sum((yi-mean(yi)).^2);
    
    if options.Uref>0.7
        if options.figure
            if zz == 17
                figure(1),clf
                plot(r,D(zz,:),'o')
                hold all
                plot(r,P(1)*r+P(2),'r')
                plot(get(gca,'xlim'), [0 0],':k')
                ylim(max(abs(D(zz,:)))*[-1.1 1.1])
                title({['z = ',num2str(z(zz)),' m,   U = ',num2str(options.Uref),' m/s'],...
                    ['\epsilon = ', num2str(eps(zz)),' W/kg, R^2 = ',num2str(R2(zz))]})
                pause
            end
        end
    end
end

%%
options.Uref
if options.Uref>0.7
if options.figure
    figure(2),clf
    subplot(121)
    semilogx(eps, z(1:nz))
    ylabel('z')
    xlabel('\epsilon')
    
    subplot(122)
    plot(yint,z(1:nz))
    xlabel('\sigma_N')
end
end