function [ustar z0] = calc_lawofthewall(data,options)
%% Determines the u* and z0 values for an inputted profile
%
% Inputs: 
% data.spd = profile the flow speed (already averaged)
% data.z = z values
%
% options.zlim_fit = [zmin zmax]: vertical limits for fits
% options.dispfit = flg: display the fit
%
% Justine McMillan
% July 5th, 2013

kappa=0.4;

%Function
Func_Uz = @(Consts,z) Consts(1).*log(z)+Consts(2);
    % Func_Uz --> U(z) = ustar/kappa*ln(z)-ustar/kappa*ln(z0)
    % Const(1) = ustar/kappa, Const(2) = -ustar/kappa*ln(z0)
    % ustar = kappa*Const(1), z0 = exp(-Const(2)/Const(1));

% range of z over which to do fit
zmin = options.zlim_fit(1);
zmax = options.zlim_fit(2);
[~, izmax] = min(abs(data.z-zmax));
[~, izmin] = min(abs(data.z-zmin));

zN=1; %Don't normalize if zN = 1;
znorm = data.z(izmin:izmax)/zN; 
U = data.spd(izmin:izmax);

%initial guess 
initguess = [1,0.01]; 

%Nonlinear fit
[CFits]=nlinfit(znorm,U,Func_Uz,initguess);
        
% Determine u* and z0
ustar= kappa*CFits(1); 
z0 = zN*exp(-CFits(2)/CFits(1));

if options.dispfit
    figure
    set(gcf,'Name','Fit')
    semilogy(data.spd,data.z,'b.')
    hold on
    semilogy(ustar/0.4*log(data.z/z0),data.z,'r')
    semilogy(get(gca,'xlim'),zmin*[1 1],'--k')
    semilogy(get(gca,'xlim'),zmax*[1 1],'--k')
    xlabel('Speed')
    ylabel('z')
    legend('data','fit','Location','NorthWest')
    title(['u* = ',num2str(ustar),'  z0 = ',num2str(z0)])
    pause
end

%% TEST CODE        
%   
% clear
% 
% ustar = 0.05;
% z0 =0.001;
% 
% data.z = [0.25:0.5:6];
% data.spd = ustar/0.4*log(data.z/z0);
% 
% options.zlim_fit = [0.25,6];
% 
% [ustarfit, z0fit] = calc_lawofthewall(data,options)
% 
% myfigure('TestProfile')
% plot(data.spd,data.z,'b.')
% hold on
% plot(ustarfit/0.4*log(data.z/z0fit),data.z,'r')
% xlabel('Speed')
% ylabel('z')
% legend('data','fit','Location','NorthWest')
