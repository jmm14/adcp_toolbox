function [eps,sigmaN,R2,D]=calc_eps_ADCP_SF_constN(vp,z,sigmaN_in,options)
% calculate the dissipation rate from an ADCP using a structure function
% approach but with a constant noise level
% i.e. Fitting:
% D-2*sigmaN^2 = Cv*eps^(2/3)*r^2/3
%
% Justine McMillan
% Mar 29, 2016 - Modified figure

if ~isfield(options,'figure'); options.figure = 1; end
if ~isfield(options,'theta');  options.theta = 20; end
if ~isfield(options,'Cv2');    options.Cv2 = 2.0;  end

dz = mean(diff(z));
dr = dz/cosd(20);

if ~isfield(options,'nrmax');
    options.nrmax = 7;
    disp(['Max r: ',num2str(2*options.nrmax*dr)])
end

%% initialize
nz = length(z);
D = NaN*ones(nz,options.nrmax);
eps = NaN*ones(nz,1);
sigmaN = NaN*ones(nz,1);
R2 = NaN*ones(nz,1);
r = 2*dr*[1:1:options.nrmax];

%% for each height, compute structure functions, epsilon, noise
for zz = 4:nz-3 %Require there to be at least 3 points to fit
    
    nr = min([zz-1,options.nrmax,nz-zz]);
    
    for rr = 1:nr
        bins = [zz-rr,zz+rr];
        vdiff = vp(:,bins(1))-vp(:,bins(2)); % Centre difference
        D(zz,rr) = nanmean((vdiff.^2));
        DNN(zz,rr) = D(zz,rr) - 2*sigmaN_in(zz)^2;
    end
    
    % least squares fit
    ind = ~isnan(D(zz,:));
    xi = r(ind).^(2/3);
    yi = DNN(zz,ind);
    P = polyfitZero(xi,yi,1);
    
    %R2
    fi = polyval(P,xi);
    R2(zz) = 1-sum((yi-fi).^2)/sum((yi-mean(yi)).^2);
    
    % calculate eps
    eps(zz) = (P(1)/options.Cv2)^(3/2);
    
    % calculate noise
    sigmaN(zz) = sigmaN_in(zz);
    
%     if options.figure
%      %   if imag(sigmaN(zz))~=0
%             figure(1),clf
%             plot(r,D(zz,:),'o')
%             hold all
%             p = plot([0:1:10],P(1)*[0:1:10].^(2/3)+P(2),'k');
%             title(['\epsilon = ',num2str(eps(zz)),' W/kg  ,\sigma_N = ',num2str(sigmaN(zz))] )
%             legend(p,['R^2 = ',num2str(R2(zz))])
%             pause
%      %   end
%     end
end

if options.figure
    figure(3),clf
    count = 0;
    for zz = 4:7:nz-3
        count = count+1;
        offset = (count-1)*.01;

        p(count)=plot(r,offset+DNN(zz,:),'o');
        l{count} = ['z = ',num2str(z(zz)),' m, R^2 = ',num2str(R2(zz))];
        
        if count == 1;  hold all; end
        
        xline = [0:.1:10];
        yline = options.Cv2*eps(zz)^(2/3)*xline.^(2/3)+ offset;
        plot(xline,yline,'Color',get(p(count),'color'));
    
    end

legend(p,l,'location','eastoutside')
title(['U = ',num2str(options.Uref),' m/s'])
pause
end

%%
if options.figure
    figure(4),clf
    subplot(121)
    semilogx(eps, z(1:nz))
    ylabel('z')
    xlabel('\epsilon')

    subplot(122)
    plot(sigmaN,z(1:nz))
    xlabel('\sigma_N')
end