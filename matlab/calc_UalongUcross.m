function [Ualong, Ucross,signed_vel] = calc_UalongUcross(east_vel,north_vel,flooddir,varargin)
%% Calculate Ualong and Ucross for ADCP data that is in BP Format
% Computes the principal directions (for each bin) separately for ebb and
% flood (to handle asymmetric locations). Then uses this as the "channel
% angle" and gets the components of the velocity along and across the
% channel.
% 
% Note: Sometimes Ualong and Ucross are computed for each ensemble. Perhaps
% I should revisit this (but then getting the standard deviations would be
% tough) -- THINK ABOUT THIS AND COME UP WITH A CONSISTENT METHOD
%
% Justine McMillan
% July 10, 2014
%
% July 16, 2014 - Changed the inputs to be east velocity and north velocity 
%                   and a flood direction
if nargin == 3
    flg.dispoutput = 1;
else
    flg = varargin{1};
end

if flg.dispoutput == 1
    disp_linebreak('_')
    disp('[Calculating Ualong and Uacross...]')
end

minspeed = 0.5;

speed = sqrt(east_vel.^2+north_vel.^2);
dir = get_DirFromN(east_vel,north_vel);

%% signed speed
signed_vel = sign_speed(east_vel,east_vel,speed,dir, flooddir);

[PA_fld, PA_ebb, PA_all] = dir_PrincipalAxis(dir, signed_vel, minspeed, east_vel, north_vel);

%%
if flg.dispoutput == 1
disp(['   Using ',num2str(minspeed),' m/s as the minimum speed in the determination of principal axis'])
disp('    Principal directions (depth averaged):')
disp(['      All: ',num2str(mean(PA_all))])
disp(['      Fld: ',num2str(mean(PA_fld))])
disp(['      Ebb: ',num2str(mean(PA_ebb))])
end
%%
nbins = size(east_vel,2);
thetamat = NaN*ones(size(signed_vel));

for nn = 1:nbins
    for ii = 1:2
        if ii == 1 %fld
            ind_fld = find(signed_vel(:,nn)>0);
            thetamat(ind_fld,nn) = -PA_fld(nn);
        else %ebb
            ind_ebb = find(signed_vel(:,nn)<=0);
            thetamat(ind_ebb,nn) = 180-PA_ebb(nn);
        end
    end
end
[Ucross, Ualong] = rotate_to_channelcoords(east_vel,north_vel,thetamat);

if flg.dispoutput
    nb = ceil(nbins/2);
    
    myfigure('velocities')
    plot(Ualong(:,nb))
    hold all
    plot(Ucross(:,nb))
    plot(signed_vel(:,nb))
    legend('Ualong','Ucross','signed vel')
end

if flg.dispoutput == 1
    disp_linebreak('_')
end