function specs =  get_specs(all,ens_t,lon,lat,zlevel,zmin,zmax,variables)
%
% Make a column in a table of specifications
%
% Inputs: (there is likely a much better way to do this)
%   all = the struct names "all" (contains h)
%   ens_t = the time values
%   lon = longitude
%   lat = latitude
%   zlevel = level for which measurements are reported
%   variables = a cell of strings of which specs to get
%
% Justine McMillan
% June 5th, 2011

row = 0;

%
for ii=1:length(variables)
    row = row+1;
    var = char(variables(ii));
    
    if isequal(var,'h')
        value =  mean(getfield(all,var)); %% SOMETHING WRONG WITH h OUTPUT?
    elseif isequal(var,'ens_t')
        value = (ens_t(2)-ens_t(1))*24*60;
    elseif isequal(var,'dur')
        value = (ens_t(end)-ens_t(1)); %In days
    elseif isequal(var,'lon')
        value = lon;
    elseif isequal(var,'lat')
        value = lat;
    elseif isequal(var,'zlevel')
        if isempty(zlevel)
            value = NaN;
        else
            value = zlevel;
        end
    elseif isequal(var,'zmin')
        value = zmin;
    elseif isequal(var,'zmax')
        value = zmax;
    end    
        
    specs(row) = value;
end

