function [stress,time_out] = calc_ReynoldsStresses(time, data, stressoptions)
% Very much a work in progress
% Computes u'w'

%%
if ~isfield(stressoptions,'method'), stressoptions.method = 'TimeSearch'; end
if ~isfield(stressoptions,'showoutput'), stressoptions.showoutput = 1; end

%% Initialize
fields = stressoptions.fields;
nbins = min(size(data.(fields{1})));
phi = 20; %beam angle


if strcmp(stressoptions.method,'TimeSearch') == 1
%     if stressoptions.showoutput
%     disp('[COMPUTING Stresses] - Method: Time Search')
%     end
%     t_ens = stressoptions.ens_t*60;
%     ens_tol = 0.1;  % allow for 10% of the points to be missing, and still compute the ensemble
%     
%     if round(t_ens/60)>60
%         warning(['Check to make sure ensemble averaging is done correctly.',...
%             'Originally created for much shorter ensembles'])
%     end
%     
%     %% Set up time vector -- want timestamps to be on even minutes
%     SEC2DAY = 1/(24*3600);
%     dt = mean(diff(time.mtime))/SEC2DAY; % delta t in seconds
%     
%     [yr,mm,dd,hr,mins,sec] = datevec(time.mtime(1));
%     
%     time_out.mtime = [datenum(yr,mm,dd,hr,0,0):t_ens/3600/24:time.mtime(end)]; %start on the hour -- leading values will be removed at the end
%     nensapprox = (t_ens/dt);
%     if stressoptions.showoutput
%     disp(['[COMPUTING SPECTRA] - Segment Length is approximately: ',num2str(round(nensapprox)),' points'])
%     end
% 
%     %Initialize
%     nt = length(time_out.mtime);
%     
% 
%     
%     %% Compute spectra
%     for ii = 1:length(time_out.mtime)
%         
%         tstart = time_out.mtime(ii) - 1/2*(t_ens*SEC2DAY);
%         tend   = time_out.mtime(ii) + 1/2*(t_ens*SEC2DAY);
% 
%         ind_t = find(time.mtime>tstart & time.mtime<=tend);
%         dt = mean(diff(time.mtime(ind_t)))*24*3600;
%         fs = 1/dt;
%         
%         if length(ind_t)<nensapprox*(1-ens_tol)
%             time_out.mtime(ii) = NaN;
% 
%         end
%         % loop through data fields
%         for jj = 1:length(fields)
%             fieldvar =  fields{jj};
%             
%             if length(ind_t)<nensapprox*(1-ens_tol)
%                 Sxx.(fieldvar)(:,ii,:) = NaN;
%             else
%                 eval(['field = data.',fields{jj},'(:,ind_t);']);
%                 
%                 for kk = 1:nbins
%                     field_detrend = detrend(field(kk,:));
%                     %% Compute spectra
%                     [SXX, freq] = pwelch(field_detrend,...
%                             hanning(stressoptions.nfft),...
%                             stressoptions.overlap*stressoptions.nfft,...
%                             stressoptions.nfft,fs);
%                      Sxx.(fieldvar)(:,ii,kk) = SXX;
%                 end% kk
% 
%             end %if
%         end %jj
%         clear ind_t
%     end
%     
%     %% Display some info to the screen
%     ind_t2 = find(~isnan(time_out.mtime));
% 
%     if stressoptions.showoutput
%         t_drop1 = (time_out.mtime(ind_t2(1)) - time.mtime(1) - 1/2*(t_ens*SEC2DAY))*24*60;
%         t_drop2 = (time.mtime(end) - time_out.mtime(ind_t2(end)) - 1/2*(t_ens*SEC2DAY))*24*60;
%     
%         disp(['[COMPUTING SPECTRA] - Dropped ',num2str(t_drop1),' mins of data at the start of file'])
%         disp(['[COMPUTING SPECTRA] - Dropped (or carried over) ',num2str(t_drop2),' mins of data at the end of file'])
%     end
%     %% Trim Nans from the front and end
%     time_out.mtime = time_out.mtime(ind_t2(1):ind_t2(end));
%     for ii = 1:length(fields)
%         fieldvar =  fields{ii};
%         Sxx.(fieldvar) = Sxx.(fieldvar)(:,ind_t2(1):ind_t2(end),:);
%        
%     end
    
elseif strcmp(stressoptions.method,'NumPoints')
    npts = stressoptions.numpoints;
    if stressoptions.showoutput
        disp(['[COMPUTING STRESSES] - Using ',num2str(npts),' points'])
    end
    
    % Initialize
    nt = floor(length(time.mtime)/npts);
    stress.yz = NaN*ones(nt,nbins);
    stress.xz = NaN*ones(nt,nbins);

    
    for ii = 1:nt
        ind_t = 1+(ii-1)*npts:ii*npts;
        dt = (time.mtime(ind_t(end)) - time.mtime(ind_t(1)))*24*60; %minutes
        if dt>stressoptions.max_t || dt<stressoptions.min_t
            error('Something wrong with bursts')
        end
        
        time_out.mtime(ii) = mean(time.mtime(ind_t));

        % Get necessary data for burst
        V1 = detrend(data.v1(:,ind_t)'); 
        V1_std = nanstd(V1);
        V2 = detrend(data.v2(:,ind_t)'); 
        V2_std = nanstd(V2);
        V3 = detrend(data.v3(:,ind_t)'); 
        V3_std = nanstd(V3);
        V4 = detrend(data.v4(:,ind_t)'); 
        V4_std = nanstd(V4);
        
        % Compute stress in instrument coordinates
        stress.xz(ii,:) = -(V2_std.^2 - V1_std.^2)/(4*cosd(phi)*sind(phi));
        stress.yz(ii,:) = -(V3_std.^2 - V4_std.^2)/(4*cosd(phi)*sind(phi));

    end %ii
    if stressoptions.showoutput
        t_drop = length(time.mtime) - nt*npts;
       
    
        disp(['[COMPUTING SPECTRA] - Dropped ',num2str(t_drop),' points at the end of the file'])
    end
else
    error(['Invalid Method'])
end

