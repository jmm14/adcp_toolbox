function [Sxx, freq,time_out] = calc_turbulencespectra(time, data, spectraoptions)

% Description: calculates turbulence spectra of an entire matrix of data by
% separating the data into different segments using a "time search" or 
% "Number of points" method. (Definitely not very efficient code)
% The pwelch function is used to compute the spectra with the options
% defined in "spectraoptions".
%
%
%Inputs:
%   - time: raw time values in matlab format 
%   - data: raw data to be used to compute the spectra
%   - spectraoptions: structure with options including those needed for
%   pwelch function
%
% Outputs:
%    - Sxx: Structure of spectra amplitudes (3D matrix with freq x time x  bin)
%    - freq: Output frequency values correspondingt to spectra
%    - time_out: Central timestamps for each interval
%
% Justine McMillan
% Dec 12, 2013
%
% Spe 22, 2014 - Modified to get it to work for an instrument working in
% burst mode
%
% Mar 11, 2015 -- Modified -- THERE WAS AN ERROR IN MY fs
% June 5, 2015 -- Added variance calculation to the Burst section. The Sxx
%                 variable now includes "varTS" and "varSPEC" fields which
%                 correspond to the variance of the time series and the
%                 variance of the spectrum respectively
% July 6, 2015 -- Modified the code so it would work with AD2CP data
%                   (verified that it would still work with ADCP data)
% Jan 19, 2016 -- added a line to display progress
%              -- Fixed timesearch method
%%

if ~isfield(spectraoptions,'method'), spectraoptions.method = 'TimeSearch'; end
if ~isfield(spectraoptions,'showoutput'), spectraoptions.showoutput = 1; end
if ~isfield(spectraoptions,'fsmax'), spectraoptions.fsmax = 2; end
if ~isfield(spectraoptions,'fsmin'), spectraoptions.fsmin = 1; end


%% Initialize
fields = spectraoptions.fields;
nbins = min(size(data.(fields{1})));
nf = spectraoptions.nfft/2+1;

if strcmp(spectraoptions.method,'TimeSearch') == 1
 %   error('Need to add variance check to this part of the code')
    t_ens = spectraoptions.ens_t*60;
    ens_tol = 0.1;  % allow for 10% of the points to be missing, and still compute the ensemble
    
    if round(t_ens/60)>60
        warning(['Check to make sure ensemble averaging is done correctly.',...
            'Originally created for much shorter ensembles'])
    end
    
    %% Set up time vector -- want timestamps to be on even minutes
    SEC2DAY = 1/(24*3600);
    [yr,mm,dd,hr,mins,sec] = datevec(time.mtime(1));
    time_out.mtime = [datenum(yr,mm,dd,hr,0,0):t_ens/3600/24:time.mtime(end)]; %start on the hour -- leading values will be removed at the end

    % Get dt - assumes that the first two points are representative of
        % sampling frequency and not spread across the bursts
    dt_first = (time.mtime(2)-time.mtime(1))/SEC2DAY; % delta t in seconds
    nensapprox = (t_ens/dt_first);
    if spectraoptions.showoutput
        info = {['[COMPUTING SPECTRA] - Method: Time Search'];
                ['[COMPUTING SPECTRA] - Approx fs: ',num2str(1/dt_first),' Hz (based on first 2 points)'];
                ['[COMPUTING SPECTRA] - Averaging Segment Length: ',...
                        num2str(t_ens),' seconds'];
                ['[COMPUTING SPECTRA] - Averaging Segment Length: ',...
                        num2str(round(nensapprox)),' points'];
                ['[COMPUTING SPECTRA] - Min segment length: ',...
                        num2str((1-ens_tol)*round(nensapprox)*dt_first),' seconds']};
        disp_info(info)
        disp_linebreak('-')
    end

    %% Initialize
    nt = length(time_out.mtime);
    for ii = 1:length(fields);
        Sxx.(fields{ii}) = NaN*ones(nf,nt,nbins);
    end
    Sxx.freq = NaN*ones(nf,nt);
    
    %% Compute spectra
    for ii = 1:length(time_out.mtime)
        
        tstart = time_out.mtime(ii) - 1/2*(t_ens*SEC2DAY);
        tend   = time_out.mtime(ii) + 1/2*(t_ens*SEC2DAY);

        ind_t = find(time.mtime>tstart & time.mtime<=tend);
        dt = mean(diff(time.mtime(ind_t)))*24*3600;
        fs = 1/dt;
        if fs>spectraoptions.fsmax || fs <spectraoptions.fsmin
            fs
            error('fs seems wrong')
        end
         %frequency vector
        f_vec = [0:fs/spectraoptions.nfft:fs/2]';
        
        if length(ind_t)<nensapprox*(1-ens_tol) % not enough points
            time_out.mtime(ii) = NaN;
            %disp(['Only ',num2str(length(ind_t)),' points'])
            continue
        
        else
            % loop through data fields
            for jj = 1:length(fields)
                fieldvar =  fields{jj};
                eval(['field = data.',fields{jj},'(:,ind_t);']);
                
                for kk = 1:nbins
                    field_detrend = detrend(field(kk,:));
                    
                    % Compute spectra
                    [SXX, freq] = pwelch(field_detrend,...
                        hanning(spectraoptions.nfft),...
                        spectraoptions.overlap*spectraoptions.nfft,...
                        spectraoptions.nfft,fs);
                    Sxx.(fieldvar)(:,ii,kk) = SXX;
                    
                    % compute variances
                    varTS = std(field_detrend).^2;
                    varSPEC = trapz(freq,SXX);
                    
                    Sxx.([fieldvar,'_varTS'])(ii,kk) = varTS;
                    Sxx.([fieldvar,'_varSPEC'])(ii,kk) = varSPEC;
                    
                    % Check frequency vector
                    if sum(freq-f_vec)>1e-10
                        error('something wrong with frequency vector')
                    end
                end % kk (vertbins)
            end %jj (fields)
        end % if  
        Sxx.freq(:,ii) = f_vec;
        disp_percdone(ii,length(time_out.mtime),5)
        clear ind_t
    end % ii (time vector)
    
    % Error if variance diff is too large
    for ii = 1:length(fields)
        fieldvar = fields{ii};
        PD.(fieldvar) = abs(Sxx.([fieldvar,'_varTS'])-Sxx.([fieldvar,'_varSPEC']))./Sxx.([fieldvar,'_varTS'])*100;
        if mean(mean(PD.(fieldvar))) > 10
        error('Average percent diff between time series and spectra is greater than 10% -- perhaps something wrong with the code?')
        end
    end
    
    %% Display some info to the screen
    ind_t2 = find(~isnan(time_out.mtime));

    if spectraoptions.showoutput
        t_drop1 = (time_out.mtime(ind_t2(1)) - time.mtime(1) - 1/2*(t_ens*SEC2DAY))*24*60;
        t_drop2 = (time.mtime(end) - time_out.mtime(ind_t2(end)) - 1/2*(t_ens*SEC2DAY))*24*60;
    
        disp(['[COMPUTING SPECTRA] - Dropped ',num2str(t_drop1),' mins of data at the start of file'])
        disp(['[COMPUTING SPECTRA] - Dropped (or carried over) ',num2str(t_drop2),' mins of data at the end of file'])
    end
    %% Trim Nans from the front and end
    time_out.mtime = time_out.mtime(ind_t2(1):ind_t2(end));
    for ii = 1:length(fields)
        fieldvar =  fields{ii};
        Sxx.(fieldvar) = Sxx.(fieldvar)(:,ind_t2(1):ind_t2(end),:);
        Sxx.([fieldvar,'_varTS']) = Sxx.([fieldvar,'_varTS'])(ind_t2(1):ind_t2(end),:);
        Sxx.([fieldvar,'_varSPEC']) = Sxx.([fieldvar,'_varSPEC'])(ind_t2(1):ind_t2(end),:);
    end
    
elseif strcmp(spectraoptions.method,'NumPoints')
  
    dt = diff(time.mtime)*24*3600; % in seconds
    dt_one = mean(dt(find(dt<60)));
    ind = find(dt>dt_one*2); %Find indices where dt changes by a lot (ie. burst intervals)
    nensapprox = round(ind(end-1)-ind(end-2));
    
    if spectraoptions.showoutput
        disp(['[COMPUTING SPECTRA] - Using approximately ',num2str(nensapprox),' points'])
    end
    if nensapprox/spectraoptions.numpoints<0.98
        error('Computed npts for burst different than input')
    end
    

    
    % Initialize
    nt = length(ind)+1; % number of spectra
%   nt = floor(length(time.mtime)/nensapprox) % number of spectra

    for ii = 1:length(fields);
        Sxx.(fields{ii}) = NaN*ones(nf,nt,nbins);
        Sxx.([fields{ii},'_varTS']) = NaN*ones(nt,nbins);
        Sxx.([fields{ii},'_varSPEC']) = NaN*ones(nt,nbins);
    end
    Sxx.freq = NaN*ones(nf,nt);
    
    for ii = 1:nt
        if ii == 1                   %first interval
            ind_t = 1:ind(1);
        elseif ii == nt  %last interval
            ind_t = ind(ii-1)+1:length(time.mtime);
        else                         %mid intervals
            ind_t = ind(ii-1)+1:ind(ii);
        end
%         if length(ind_t)~=2394
%             disp(length(ind_t)),pause
%         end
        time_out.mtime(ii) = mean(time.mtime(ind_t));
        
     
%         t_ens_all(ii) = (adcp.mtime(avgpts(end)) - adcp.mtime(avgpts(1)))*24*3600;
        dt_burst = (time.mtime(ind_t(end)) - time.mtime(ind_t(1)))*24*60; %minutes
        if dt_burst>spectraoptions.max_t || dt_burst<spectraoptions.min_t
            warning(['Skipping burst number ',num2str(ii),' of ',num2str(nt),...
                ' -- only ',num2str(dt_burst),' minutes'])
            time_out.mtime(ii) = nan;
            continue
  
        end

        dt=mean(diff(time.mtime(ind_t)))*24*3600; %Seconds
        fs = 1/dt;
        if fs>spectraoptions.fsmax || fs <spectraoptions.fsmin
            fs
            error('fs seems wrong')
        end
        %frequency vector
        f_vec = [0:fs/spectraoptions.nfft:fs/2]';
        
        
        
        time_out.mtime(ii) = mean(time.mtime(ind_t));
        % loop through data fields
        for jj = 1:length(fields)
            fieldvar =  fields{jj};
            
            %if length(ind_t)<nensapprox*(1-ens_tol)
            %    Sxx.(fieldvar)(:,ii,:) = NaN;
            %else
                eval(['field = data.',fields{jj},'(:,ind_t);']);
                
                for kk = 1:nbins
                    % Skip data with errors
                    if sum(isnan(field(kk,:)))
                        break
                    end
                    field_detrend = detrend(field(kk,:));
                    %% Compute spectra
                    [SXX, freq] = pwelch(field_detrend,...
                            hanning(spectraoptions.nfft),...
                            spectraoptions.overlap*spectraoptions.nfft,...
                            spectraoptions.nfft,fs);
                     Sxx.(fieldvar)(:,ii,kk) = SXX;
                     
                    %% variance check
                    varTS = std(field_detrend).^2;
                    varSPEC = trapz(freq,SXX);

                    Sxx.([fieldvar,'_varTS'])(ii,kk) = varTS;
                    Sxx.([fieldvar,'_varSPEC'])(ii,kk) = varSPEC;
                      
                     if sum(freq-f_vec)>1e-10
                     	error('something wrong with frequency vector')
                     end
                end% kk

            %end %if
        end %jj
        
        Sxx.freq(:,ii) = f_vec;
        
    end %ii
    
    
    % Error if variance diff is too large
    for ii = 1:length(fields)
        fieldvar = fields{ii};
        PD.(fieldvar) = abs(Sxx.([fieldvar,'_varTS'])-Sxx.([fieldvar,'_varSPEC']))./Sxx.([fieldvar,'_varTS'])*100;
        if mean(mean(PD.(fieldvar))) > 10
        error('Average percent diff between time series and spectra is greater than 10% -- perhaps something wrong with the code?')
        end
    end
    
    % Remove nans from beginning and end (not middle)
    ind_t2 = find(~isnan(time_out.mtime));
    time_out.mtime = time_out.mtime(ind_t2(1):ind_t2(end));

    for ii = 1:length(fields)
        fieldvar =  fields{ii};
        Sxx.(fieldvar) = Sxx.(fieldvar)(:,ind_t2(1):ind_t2(end),:);
        Sxx.([fieldvar,'_varTS']) = Sxx.([fieldvar,'_varTS'])(ind_t2(1):ind_t2(end),:);
        Sxx.([fieldvar,'_varSPEC']) = Sxx.([fieldvar,'_varSPEC'])(ind_t2(1):ind_t2(end),:);
    end
    Sxx.freq = Sxx.freq(:,ind_t2(1):ind_t2(end));

    if spectraoptions.showoutput
        %Variance info
        for ii = 1:length(fields)
            fieldvar = fields{ii};
            disp(['[COMPUTING SPECTRA] - Variance Comparison (',fieldvar,'):'])   
            disp(['    Max Percent Diff = ',num2str(max(max(PD.(fieldvar))),'%4.1f'),'%'])
            disp(['   Mean Percent Diff = ',num2str(nanmean(nanmean(PD.(fieldvar))),'%4.1f'),' +/- ',...
               num2str(nanstd(nanstd(PD.(fieldvar))),'%4.1f') ,'%'])
        end
        
        % Dropped data
        tmin_in = time.mtime(1);
        tmax_in = time.mtime(end);
        
        if ind_t2(1)~= 1
            ind1 = ind(ind_t2(1)-1);
        else
            ind1 = 1;
        end
        if ind_t2(end)~= nt
            indN = ind(ind_t2(end))+1;
        else
            indN = length(time.mtime);
        end
       
        tmin_out = time.mtime(ind1);
        tmax_out = time.mtime(indN);

        
        disp(['[COMPUTING SPECTRA] - Dropped ',num2str(24*60*(tmax_in-tmax_out)),...
            ' mins of data at the start of file'])
         disp(['[COMPUTING SPECTRA] - Dropped ',num2str(24*60*(tmin_out-tmin_in)),...
            ' mins of data at the end of file'])

    end
    
else
    error(['Invalid Method'])
end

