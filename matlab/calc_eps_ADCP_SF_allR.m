function [eps,sigmaN,R2,r,D,Rinfo,D_N] = calc_eps_ADCP_SF_allR(vp,z,options)
% calculate the dissipation rate from an ADCP using a structure function
% where the number of combinations of separation points is maximized
%
% only works for one depth
%
% Justine McMillan
% June 6, 2016
%
% Aug 9, 2016 - modified to allow for other nrmax values
% Sep 22, 2016 - fixed confidence intervals in plot and now calculate error
% in eps for SF3 method
% Feb 9, 2022 - Added output for the number of observations (DLL_N) used to compute 
%               DLL (QC metric used for atomix)
%             - Rejected points where DLL_N is below a threshold
%             - Added the number of points in regression to Rinfo
%             - Take the real part of the slope and yintercept
%
% NOTE: THIS FUNCTION HAS BEEN SUBDIVIDED INTO calc_DLL_DLLL.m and
% calc_eps_SF.m. Change motivated by ATOMIX data processing

dz = mean(diff(z));
[nr,nc] = size(vp);
if nc~=length(z)
    disp('transposing vp - rows should be time, columns should be z')
    vp = vp';
end

%% Mandatory input
if ~isfield(options,'order');
    error('Need to specify the order of SF')
end

%% Set defaults
if ~isfield(options,'figure'); options.figure = 1; end
if ~isfield(options,'Uref'); options.Uref = NaN; end
if ~isfield(options,'theta');  options.theta = 20; end
if ~isfield(options,'DLL_N_threshold');  options.DLL_N_threshold = 0; end


dr = dz/cosd(options.theta);
if ~isfield(options,'nrmax');
    options.nrmax = length(z);
 %   disp(['Max r: ',num2str((options.nrmax-1)*dr)])
end

if ~isfield(options,'Const')
    switch options.order
        case 2
            options.Const = 2.0;
        case 3
            options.Const = -4/5;
    end
end

%% initialize
npts = nchoosek(options.nrmax-1,2);
% if options.nrmax == 9
%     npts = 28;
% 
% else
%     error('need to determine total number of points')
% end


r = NaN*ones(1,npts);
D = NaN*ones(1,npts);
D_N = NaN*ones(1,npts);



%% Compute velocity differences and dissipation rate
nz = length(z);
dd = 0; % R counter
for zz = 1:nz
    binL = zz;
    binU = binL+2;
    while binU<=nz
        dd = dd+1;
        r(dd) = (binU - binL)*dr;
        vdiff = vp(:,binL)-vp(:,binU);
        
        D(dd) = nanmean((vdiff.^options.order));
        D_std(dd) = nanstd((vdiff.^options.order));
        D_N(dd) = sum(~isnan(vdiff)); % number of observations used to compute DLL
        binU = binU+1;
      
%         % DEBUGGING FIGURE       
%         figure(2),clf
%         ax(1) = subplot(211);
%         plot(vp(:,binL),'o-')
%         hold all
%         plot(vp(:,binU-1),'o-')
%          nL = sum(isnan(vp(:,binL)));
%         nU = sum(isnan(vp(:,binU-1)));
%         legend(num2str(nL),num2str(nU))
%         
%         ax(2) = subplot(212);
%         plot(vdiff.^2);
%         hold all
%         plot(get(gca,'xlim'),D(dd)*[1 1],'k')
%         title(['binL = ',num2str(binL),',   binU = ',num2str(binU),...
%                ',   D = ',num2str(D(dd),'%3.1e'), ', N = ',num2str(D_N(dd))])
%          linkaxes(ax,'x')
%         pause
    end
end




% fit good data to theoretical curves
ind = find(D_N>options.DLL_N_threshold);
npts = length(ind);

if options.order == 2
    xi = r(ind).^(2/3);
    yi = D(ind);
elseif options.order == 3
    xi = r(ind);
    yi = D(ind);
end



% regression (with CIs)
X = [ones(size(xi')) xi'];
[b,bint] = regress(yi',X);
slope = b(2);
yint = b(1);
d_slope = 0.5*diff(bint(2,:));
d_yint = 0.5*diff(bint(1,:));

%R2
fi = slope*xi+yint;
R2 = 1-sum((yi-fi).^2)/sum((yi-mean(yi)).^2);

if options.order == 2
    % Calculate epsilon
    if slope > 0
        eps = real((slope/options.Const)^(3/2)); % Would be imaginary if slope is negative
        d_eps = 3/2*eps/slope*d_slope;
    else
        eps = NaN;
        d_eps = NaN;
    end
    
    % Calculate sigmaN
    if yint > 0
        sigmaN = sqrt(yint/2);
    else
        sigmaN = NaN;
    end
elseif options.order == 3
    eps = slope/options.Const;
    d_eps = 5/4*d_slope;
    sigmaN = NaN; % could make this the yint
end

% if d_eps/eps<2
%     options.figure=1
% end

if options.figure
    figure(3),clf
    
    pdata(1)=plot(r.^(2/3),D,'xk'); hold all
    labels{1} = ['All points'];
    length(xi)
    if length(xi)>0
        pdata(2)=plot(xi,yi,'o');
        labels{2} = ['points included (n=',num2str(npts),')'];
    end
    
    % Show which points are rejected
    disp(['D_N Threshold: ',num2str(options.DLL_N_threshold)])
    disp('      r        D       D_N    ')
    for rr = 1:length(r)
        disp([r(rr),D(rr),round(D_N(rr))])
    end
    
    
    % errorbars -- standard error
    % errorbar(xi,yi,D_std/length(vp),'ko')
    
    % determine confidence limits
    y_hat = slope*xi+yint;
    n = length(xi);
    s2 = (yi-y_hat)*(yi-y_hat)'/(n-2); %variance of the residuals (eqn given by Keith)
    xfit = 0:0.01:max(xi);
    X0 = [ones(size(xfit')) xfit'];
    Y0 = X0*b;
    sigma_Y0 = NaN*ones(size(Y0));
    for ii = 1:length(xfit)
        sigma_Y0(ii) = sqrt(X0(ii,:)*inv(X'*X)*X0(ii,:)'*s2);
    end
    % confidence intervals (95% - assuming normal dist)
    yfit = Y0;
    ylow = Y0 - 1.96*sigma_Y0; % lower confidence limit
    yupp = Y0 + 1.96*sigma_Y0; % upper confidence limit
    
    % plot regression & CIs
    plot(xfit,ylow,'r-.');
    plot(xfit,yupp,'r-.');
    plot(xfit,yfit,'b','linewidth',2);
    
    
    % plot expected intercept
    if options.order == 2
        plot(0,2*options.sigmaN_v^2,'*m')
    elseif options.order == 3
        plot(0,0,'*m')
    end
    
    title({['U = ',num2str(options.Uref)],...
        ['\epsilon = ',num2str(eps),'  \Delta\epsilon/\epsilon = ',num2str(d_eps/eps)],...
        ['\sigma_N (norm.) = ',num2str(sigmaN/options.sigmaN_v)]})
    %  ylim([-0.005 0.025])
    %     %R2
    %     fi = polyval(P,xi);
    %     R2(zz) = 1-sum((yi-fi).^2)/sum((yi-mean(yi)).^2);
    legend(pdata,labels,'location','northwest')
    pause

end

%% regression info
Rinfo.slope = slope;
Rinfo.yint = yint;
Rinfo.CI_yint = bint(1,:);
Rinfo.CI_slope = bint(2,:);
Rinfo.d_eps = d_eps;
Rinfo.npts = npts;