function  [ind_HW, ind_LW,times] = get_HW_LW(elev, time, options)
%% function [ind_HW,ind_LW] = get_HW_LW(elev, time, options)
% Identifies slack waters in time series by finding the extrema of the input time series. This function uses a different
% method than Brian Polagye
%
%
%%Inputs:
%   elev: surface elevation (m/s)
%   time: times (matlab serial format) associated with each speed
%   options: structure containing...
%       - T_thresh: the minimum time HW, LW (in hours)
%       - smoothpts: number of points to smooth over
%       - figure:    1 if you want to show a figure
%
%Outputs:
%   ind_HW: indices of high water
%   ind_LW: indices of low water
%   times: structure with times corresponding to HW/LW 
%   ind_gd: indices of the good data

% 
% Justine McMillan
% Nov 17th, 2013
%
% Jan 16, 2014 - added times as an output (note: not interpolated)
% Apr 15, 2014 - used a butterworth filter to get rid of high frequency noise
% July 17, 2014 - Kill last point if it is too close to the end of the time
% series (filter causes erroneous results)
% Apr 16, 2015 - Remove nans from the timeseries. Doesn't work with filter



%% Default options
if ~isfield(options,'T_thresh'),  options.T_thresh = 9;     end
if ~isfield(options,'smoothpts'), options.smoothpts = 30;   end
if ~isfield(options,'figure'),    options.figure = 1;      end
if ~isfield(options,'debug'),    options.debug = 0;      end

%%
dt = time(2)-time(1);

% Save input
elev_in = elev;
time_in = time; 

% Remove nans
ind = find(~isnan(elev));
elev = elev(ind);
time = time(ind);

% Smooth elevation
fN = 0.5*(1/mean(dt));
% fc = 1/(3/24);
sm_hr = options.smoothpts*24*dt;
fc = 1/(sm_hr/24);
[b,a] = butter(5,fc/fN);
elevsm = filtfilt(b,a,elev);
%elevsm = smooth(elev, options.smoothpts);
mpd = floor(options.T_thresh/24/dt);

[~,ind_HW]=findpeaks(elevsm,'minpeakdistance',mpd); %High water
[~,ind_LW]=findpeaks(-elevsm,'minpeakdistance',mpd); %High water
ind_HW = ind_HW';
ind_LW = ind_LW';

if options.debug ==1 
    myfigure('Debug_get_HW_LW')
    plot(get_yd(time),elev,'k','Linewidth',3,'Marker','x')
    hold all
    plot(get_yd(time),elevsm,'c','Linewidth',3)
  %  plot(get_yd(time(ind_HW)),(elev(ind_HW)),'ro')
  %  plot(get_yd(time(ind_LW)),(elev(ind_LW)),'bo')
  %  legend('meas','smooth','HW','LW')
end
%% Get times
times.HW = time(ind_HW);
times.LW = time(ind_LW);

% Kill last point if too close to the end 
if (time(end)-times.HW(end))*24*60<60
    times.HW = times.HW(1:end-1);
    ind_HW = ind_HW(1:end-1);
end

%% Display warning if length of HW and LW indices differ by more than 1
if abs(length(ind_HW)-length(ind_LW))>1
    warning('length of HW and LW indices differ by more than 1')
end

%% in case there are nans, need to get actual indices of input vector
ind_HW = ind(ind_HW);
ind_LW = ind(ind_LW);
%%
if options.figure
    myfigure('get_HW_LW.m')
    plot(get_yd(time_in),elev_in,'k','Linewidth',3,'Marker','x')
    hold all
    plot(get_yd(time),elevsm,'c','Linewidth',3)
    plot(get_yd(time_in(ind_HW)),(elev_in(ind_HW)),'ro')
    plot(get_yd(time_in(ind_LW)),(elev_in(ind_LW)),'bo')
    legend('meas','smooth','HW','LW')
end


