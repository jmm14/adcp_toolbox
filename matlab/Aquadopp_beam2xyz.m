function [vX,vY,vZ] = Aquadopp_beam2xyz(v1,v2,v3)
% BEAM2XYZ.M
% Converts the Aquadopp beam velocities to xyz velocities.  Based on
% transform.m provided by Nortek
% DJW 7/23/08
% JMM 2024/12/03 - Converted into function
%
% Notes: 
% - Not sure when this transformation matrix is relevant
% - Only works for upward looking Aquadopp


[NT,NZ] = size(v1);

% no_bursts = 1878; 
% no_pings = 507;
% no_cells = 28;
% filepath = 'C:\Aquadopp\AHL 6.08\';

% Transformation matrix for beam to xyz coordinates,
% the values can be found from the header file that is generated in the
% conversion of data to ASCII format

T =[6461 -3232 -3232;...
    0 -5596 5596;...
    1506 1506 1506];

% Scale the transformation matrix correctly to floating point numbers

T = T/4096;

T_org = T;

vX = NaN*ones(NT,NZ);
vY = NaN*ones(NT,NZ);
vZ = NaN*ones(NT,NZ);

% for i = 1:no_bursts
%     load([filepath 'V1\v1_' num2str(i)])
%     load([filepath 'V2\v2_' num2str(i)])
%     load([filepath 'V3\v3_' num2str(i)])
%     xyz1 = v1;
%     xyz2 = v2;
%     xyz3 = v3;
    for j = 1:NT
        disp_percdone(j,NT,10);
        for k = 1:NZ
            beam = [v1(j,k) v2(j,k) v3(j,k)]';
            xyz = T_org*beam;
            vX(j,k) = xyz(1);
            vY(j,k) = xyz(2);
            vZ(j,k) = xyz(3);
        end
    end
%     filename = [filepath 'XYZ\xyz_vel_' num2str(i)];
%     save(filename, 'xyz1', 'xyz2', 'xyz3')
% end