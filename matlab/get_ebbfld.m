function [tideflg, spdinterp] = get_ebbfld(time,mag_signed_vel,tinq)
%% function [tideflg, spdinterp] = get_ebbfld(time,mag_signed_vel,tinq)
% A function which outputs a 1 for times when the flow is flooding and 0
% for times when the flow is ebbing.
%
% Inputs:
%   - time: a vector of time values for the reference speed
%   - mag_signed_vel: The reference speed (typically depth averaged)
%   - tinq: Time values that are being inquired about (can be a vector)
%
% Outputs: 
%   - tideflg: A vector of 1s and 0s that is the same size as tinq
%   - spdinterp: The interpolated reference speed
%
% Justine McMillan
% Nov 14, 2013

disp('[INFO] - Getting Ebb/flood times')
tic
tideflg = zeros(size(tinq));

spdinterp = interp1(time,mag_signed_vel,tinq);
tideflg(spdinterp>0) = 1;
% for ii = 1:length(tinq)
%     [~,ind_t] = min(abs(time - tinq(ii)));
%     if mag_signed_vel(ind_t)>0
%         tideflg(ii) = 1;
%     end
% end
toc



