function [eps, k,Skk, diagnostics,klimits] = calc_eps_ADCP(Sff_dn, freq,spd, klimits, method, options)
%
% Compute the dissipation rate from the ADCP data using the specified
% method
%
% Input:
% - Sff_dn = denoised, averaged frequency spectra (structure with fields for each beam)
% - freq = frequency
% - spd = speed to use in Taylor hypothesis
% - klimits = limits of the wavenumber to use in inertial subrange fit (in rad/m) 
%      if klimits = [], "get_optimal_klim" will be called
% - method = string specifying method to use ('2beams','4beams','2beam_corr','vertbeam')
% - options = structure with options
%     .figure = flg to generate figure
%     .theta = beam angle (default is 20 degrees)
%     .klimits = options related to klimits determination
%  
% Output:
% - eps = dissipation rate, i.e. mean[(const*Skk.sum(ind_k).*k(ind_k).^(5/3)]
% - k = k values
% - Skk = structure of wavenumber spectra
%     .v# (along beam velocity spectra)
%     .sum (added spectra used in computatopm of epsilon)
% - diagnostics = structure with variables useful for determining quality
%                   of fit
%     .const = constant dependent on beam geometry and Kolmogorov constants
%     .ISRmean = mean of const*Skk.sum*k^(5/3) in inertial subrange
%     .ISRstd = std of const*Skk.sum*k^(5/3) in inertial subrange
%     .ratio = diagnostics.ISRstd/diagnostics.ISRmean
% - klimits = limits of the wavenumber that were used in the fit (in rad/m) 
%
% JMM
% Dec 2, 2015
% Dec 8, 2015 - Added calculation of std dev in ISR, changed outputs
% Dec 15, 2015 - Added diagnostic of deviation from -5/3 slope
% Dec 17, 2015 - Added tilt correction for four beam method
% Jan 29, 2016 - Added call to function to determine "optimal" k limits
% Feb 3, 2016 - Made output text optional

%% Default options
if ~isfield(options,'figure'),   options.figure = 1;         end
if ~isfield(options,'theta'),    options.theta = 20;         end % Angle of the beam wrt vertical
if ~isfield(options,'klimits'),  options.klimits = struct(); end 
if ~isfield(options,'dispoutput'),  options.dispoutput = 1;  end 

% Kolmogorov constants
Ku = 0.5;
Kw = 0.67;

% wavenumber (rad/m)
k = 2*pi*freq/spd; 

% wavespectra
if strfind(method,'2beams')
    fields = {'v3','v4'};
elseif strfind(method,'4beams')
    fields = {'v1','v2','v3','v4'};
elseif strfind(method,'vertbeam')
    fields = {'v5'};
end


for ii = 1:length(fields)
    Skk.(fields{ii}) = spd/(2*pi)*Sff_dn.(fields{ii}); %m^3/s^2
end

% initialize
eps = NaN;
diagnostics.const = NaN;
diagnostics.ISRmean = NaN;
diagnostics.ISRstd = NaN;
diagnostics.ratio = NaN;
diagnostics.slope = NaN;
diagnostics.gamma = NaN;
diagnostics.expdev = NaN;
diagnostics.ErrorMessage = '';


% compute epsilon
switch method
    case '2beams'
        if options.dispoutput
            disp('Using 2 beam method - only set up to use beams 3 and 4')
        end
        l = {'v3','v4'};
        const = (1/(2*sind(options.theta)^2*Ku+2*cosd(options.theta)^2*Kw));
%         const = 0.70^(2/3)
        Skk_mat = [Skk.v3 Skk.v4];
        Sff_mat = [Sff_dn.v3 Sff_dn.v4];
        Skk.sum = sum(Skk_mat,2);
    case '4beamsold'
        if options.dispoutput
            disp('Using 4 beam method')
        end
        l = {'v1','v2','v3','v4'};
        const = (1/(2*sind(options.theta)^2*Ku+4*cosd(options.theta)^2*Kw));
        Sff_mat = [Sff_dn.v1 Sff_dn.v2 Sff_dn.v3 Sff_dn.v4];
        Skk_mat = [Skk.v1 Skk.v2 Skk.v3 Skk.v4];
        Skk.sum = sum(Skk_mat,2);
    case '4beams'
        if options.dispoutput
            disp('Using 4 beam method (Rolfs suggestion)') %Change in the constant
        end
        l = {'v1','v2','v3','v4'};
        const = (1/(2*sind(options.theta)^2*Ku+2*sind(options.theta)^2*Kw+4*cosd(options.theta)^2*Kw));
        Sff_mat = [Sff_dn.v1 Sff_dn.v2 Sff_dn.v3 Sff_dn.v4];
        Skk_mat = [Skk.v1 Skk.v2 Skk.v3 Skk.v4];
        Skk.sum = sum(Skk_mat,2);
    case '2beams_corr'
        if options.dispoutput
            disp('Using 2 beam method with tilt correction')
        end
        warning('Need to check equation and beta value')
        beta = 0.1;
        l = {'v3','v4'};
        const = (1/(2*sind(options.theta)^2*Ku+2*cosd(options.theta)^2*Kw));
        gamma = 2*beta/tand(2*options.theta)
        Skk_mat = [(1+gamma)*Skk.v3 (1-gamma)*Skk.v4];
        Sff_mat = [Sff_dn.v3 Sff_dn.v4];
        Skk.sum = sum(Skk_mat,2);
    case 'vertbeam'
        if options.dispoutput
            disp('Using vertical beam method (for AD2CP)')
        end
        l = {'v5'};
        const = (1/Kw);
        Sff_mat = Sff_dn.v5;
        Skk_mat = Skk.v5;
        Skk.sum = Skk_mat;   
end

%% get klimits
if isempty(klimits) 
    klimits = get_optimal_klimits(k,Skk.sum,options.klimits);
end

ind_k = find(k>klimits(1) & k<klimits(2));

%% errors for not enough points in ISR
if length(ind_k)<=1
    diagnostics.ErrorMessage = 'Not enough points in ISR';
    if options.dispoutput; disp(diagnostics.ErrorMessage); end
    return
elseif length(find(~isnan(Skk.sum(ind_k))))<=1
    diagnostics.ErrorMessage = 'Spectral values in ISR are undefined (b/c lower than noise level)';
    if options.dispoutput; disp(diagnostics.ErrorMessage); end
    return
end
%%
ISRvals = const*Skk.sum(ind_k).*k(ind_k).^(5/3);
ISRmean = nanmean(ISRvals);
ISRstd = nanstd(ISRvals);
eps = (ISRmean).^1.5;

%%
diagnostics.const = const;
diagnostics.ISRmean = ISRmean;
diagnostics.ISRstd = ISRstd;
diagnostics.ratio = ISRstd/ISRmean;

%% determine slope in ISR
ind = find(~isnan(Skk.sum(ind_k)));
pf = polyfit(k(ind_k(ind)),ISRvals(ind),1);
diagnostics.slope = pf(1);

%% determine deviation from -5/3
y = log(Skk.sum(ind_k(ind)));
x = log(k(ind_k(ind)));
pf2 = polyfit(x,y,1);
diagnostics.gamma = -pf2(1);
diagnostics.expdev = (5/3-diagnostics.gamma)/(5/3); % deviation from 5/3


%%
if options.figure
    myfigure('eps_calc')
    subplot(131)
    p = loglog(freq,Sff_mat);
    legend(p,l)
    ylabel('Sff - N'),xlabel('f')
    subplot(132)
    p = loglog(k,Skk_mat);
    ylabel('Skk'),xlabel('k')
    legend(p,l)
    title(['U = ',num2str(spd)])
    subplot(133)
    p=plot(k,const*Skk.sum.*k.^(5/3),'.-');
    hold all
    plot(get(gca,'xlim'),[1 1]*ISRmean,'k')
    plot_fillstd(get(gca,'xlim'),[1 1]*ISRmean,[1 1]*ISRstd,0.6*[1 1 1]);
    pfit=plot(k(ind_k),polyval(pf,k(ind_k)),'y');
    fill_transparent(klimits,get(gca,'Ylim'),'c')
    xlim([k(2) k(end)])
    ylabel('C*Skk*k^{5/3} [m^{-4/3} s^{-2} rad^{5/3} ]'),xlabel('k [rad/m]')
    %title_str = ['ISRstd/ISRmean = ',num2str(ISRstd/ISRmean*100,'%3.1f'),'%'];
    %title(title_str)
    textstr = {['ISRstd/ISRmean = ',num2str(ISRstd/ISRmean*100,'%3.1f'),'%'],...
                     ['slope = ',num2str(pf(1),'%3.1e')],...
                     ['expdev = ',num2str(diagnostics.expdev,'%3.1f')]};

    annotation('textbox',[0.8 0.2 0.3 0.3],'String',textstr,...
        'FitBoxToText','on',...
        'EdgeColor',[0  0 0],...
        'LineWidth',2,...
        'BackgroundColor',[0.9  0.9 0.9],...
        'Color',[0.84 0.16 0]);

    % ...and position it on the current axis
   % set(ah,'parent',gca);
   % set(ah,'position',[31 .1 3 .2]);
  %  pause
end


    

