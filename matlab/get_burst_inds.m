function [ind_start,ind_end,npts] = get_burst_inds(tvec,min_dt)
% ind_start = start indices of bursts
% ind_end = end indices of bursts
% npts = number of points in the burst
% tvec = time vector
% min_dt = minimum time between the end of one burst and the start of
%       another
% NOTE: tvec and min_dt need to have the same units

dt = diff(tvec);
ind_burst = find(dt>min_dt);
nburst = length(ind_burst)+1;

for bb = 1:nburst
    if bb == 1
        ind_start(1) = 1;
        ind_end(1) = ind_burst(1);
    elseif bb == nburst
        ind_start(bb) = ind_burst(bb-1)+1;
        ind_end(bb) = length(tvec);
    else
        ind_start(bb) = ind_burst(bb-1)+1;
        ind_end(bb) = ind_burst(bb);
    end
    npts(bb) = ind_end(bb)-ind_start(bb)+1;
end
        
    