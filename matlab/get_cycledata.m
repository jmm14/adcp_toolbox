function cycle = get_cycledata(time,data,pres,ind_LW)
% Separate data into different tidal cycles where the start and end of a
% cycle is determined based on LW.
%
% Input:
%   - time: time.mtime structure
%   - data: structure with various data fields
%   - pres: pres.surf structure
%   - ind_LW: indices of LW times
%
% Output:
%   - cycle: Structure with all the output fields. Each index corresponds
%   to a different tide
%       alpha
%       data
%       
% 
% Justine McMillan
% Dec 5th, 2013

fields = fieldnames(data)

for ii =1:length(ind_LW)-1
    cycle(ii).time = time.mtime(ind_LW(ii):ind_LW(ii+1)-1);
    cycle(ii).pres  = pres.surf(ind_LW(ii):ind_LW(ii+1)-1);

    cycle(ii).alpha= (cycle(ii).time - cycle(ii).time(1))/(cycle(ii).time(end) - cycle(ii).time(1));
    
    cycle(ii).range = (max(cycle(ii).pres)-min(cycle(ii).pres));
    
    for jj = 1:length(fields)
        jj
        if length(data.(fields{jj})) == length(time.mtime) %strcmp(fields{jj},'bins') == 0
        cycle(ii).(fields{jj})  = data.(fields{jj})(ind_LW(ii):ind_LW(ii+1)-1,:);
    
        end
    end
end
