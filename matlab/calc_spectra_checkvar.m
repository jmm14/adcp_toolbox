function [Sxx,freq,varTS,varSPEC,varSPEC_sum] = calc_spectra_checkvar(field,fs,spectraoptions)
% Compute the spectra of a field and check the variance to ensure it is
% being conserved. The field is assumed to be a matrix in the form of
% (bins X time)
%
% Justine McMillan
% March 9, 2016
%
% July 27, 2016 -- Modified code to deal with missing values. 
% Aug 1, 2016 -- Moved nanterpolation part to a function, so input field is
% assumed to have no missing values


if ~isfield(spectraoptions,'figure');
    spectraoptions.figure = 0;
end

warningmsg = []; count = 0;
[nz,nt] = size(field);

% if nz>nt
%     error('More vertical bins than times? Transpose matrix?')
% end

t_approx = 1/fs*[0:1:nt-1];
freq_in = [0:fs/spectraoptions.nfft:fs/2]';
df = mean(diff(freq_in));
nf = length(freq_in);

% Initialize
Sxx = NaN*ones(nf,nz);
varTS = NaN*ones(1,nz);
varSPEC = NaN*ones(1,nz);
varSPEC_sum = NaN*ones(1,nz);

for zz = 1:nz
    x = field(zz,:);
    if ~isempty(find(isnan(x)))
        %disp('Nans - continuing')
        continue
    end

    field_detrend = detrend(x);
    
      
    % Compute spectra
    [SXX, freq] = pwelch(field_detrend,...
        hanning(spectraoptions.nfft),...
        spectraoptions.overlap*spectraoptions.nfft,...
        spectraoptions.nfft,fs);
    Sxx(:,zz) = SXX;
    
    % Check frequency vector
    if sum(abs(freq-freq_in))>1e-10
        error('something wrong with frequency vector')
    end
    
    % compute variances
    varTS(zz) = std(field_detrend).^2;
    varSPEC(zz) = trapz(freq,SXX);
    varSPEC_sum(zz) = df*sum(SXX);
    
    % Check variances
    PD = abs(varTS(zz)-varSPEC_sum(zz))/varTS(zz)*100;
    if PD > 15
        if 0
        figure(1),clf
        subplot(1,3,1:2)
        plot(field_detrend)
        
        hold all
        plot(get(gca,'xlim'),[1 1]*(mean(field_detrend)+sqrt(varTS(zz))),'--r')
        plot(get(gca,'xlim'),[1 1]*(mean(field_detrend)-sqrt(varTS(zz))),'--r')
        title(['var = ', num2str(varTS(zz)),'  PD = ',num2str(PD)])
        subplot(1,3,3)
        loglog(freq,Sxx(:,zz))
        title(num2str(varSPEC(zz)))
        pause
        end
        warningmsg=warning(['Average percent diff between time series and spectra (sum method) is ',...
                                num2str(PD),' -- perhaps something wrong with the code?']);
        if ~isempty(warningmsg)
            count = count +1;
            
        end
    end
    
     if spectraoptions.figure
        figure(1),clf
        subplot(1,3,1:2)
        plot(t_approx,field_detrend,'.-','markersize',15)
        legend(['zz = ',num2str(zz)])  
        xlabel('time (s)')
        subplot(1,3,3)
        loglog(freq,Sxx(:,zz))
        title(['varSpec = ', num2str(varSPEC(zz))])
        
        pause
    end
end % zz (vertbins)
if count>0
    disp([num2str(count) ' warnings of ' num2str(nz) ' spectra'])
    disp([ '  ' warningmsg])
end

freq = freq_in; % Since all freq vectors have been checked