function avgstruct = calc_depthavg_struct(Struct,z,varargin)
% Computes the depth_average of the struct output from Polagye tools
% Very simple function that averages over all the rows that are inputed
% Inputs:
% Struct = structure containing fields to average
% z = vector containing depths
% varargin = depth limits (use if the average is to be computed over a subset of
% the depth)
%   example varargin = [2.1 14] (2.1 = minimum depth, 14 = maximum depth)
%
% Changes
% July 4th -- allowed for optional input to averaged over a subset of the
% depth (i.e. only certain rows)
%
% Justine McMillan
% June 5th, 2012
%
% Changes:
%  Nov 24, 2013 -- Found an error in the calculation of H (was
%       underestimating H, and therefore overestimating the depth average)

if nargin == 3
    depthlims = varargin{1};
    zind = find(z>=depthlims(1) & z<=depthlims(2));
    z = z(zind);
elseif nargin>3
    error('Too many inputs')
else
    zind=1:length(z);
end
dz = z(2)-z(1);
%H = z(end)-z(1)
H = length(z)*dz; %Assumes that all bins are the same size

names = fieldnames(Struct);
avgstruct=struct();
for ii=1:length(names);
    names{ii};
    field = getfield(Struct,names{ii});
    if length(field>1) && ~isequal(names{ii},'h')
        avgfield = 1/H*nansum(field(zind))*dz;
    else
        avgfield = field;
    end
    avgstruct = setfield(avgstruct,names{ii},avgfield);

end


