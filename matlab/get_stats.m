function stats =  get_stats(Struct,bin,variables)
%
% Make a column in statistics table
%
% Inputs:
%   Struct = structure containing stats
%   bin = z-level to get (bin =[] will compute depth average)
%   variables = variables to get
%
% Justine McMillan
% June 5th, 2011

row = 0;


%statistics for all stages
for ii=1:length(variables)
    row = row+1;
    var = char(variables(ii));
    field =  getfield(Struct,var);
    if isempty(bin) %Depth Averaged
        stats(row) = field(1);
    else
        stats(row) = field(bin);
    end
end

