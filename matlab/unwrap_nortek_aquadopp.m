function [BEAM_VEL_UNWRAP,MASK_IND]=unwrap_nortek_aquadopp(BEAM_VEL,nFilt,options)
% function [BEAM_VEL_UNWRAP,MASK_IND]=unwrap_nortek_aquadopp(BEAM_VEL,thres,options)
% Unwrapping should only be done on beam velocities
% Inputs:
% - BEAM_VEL: Beam velocity [NTxNZx3]
% - nFilt: number of points to smooth.. Cynthia likes to use no more than 1-2 seconds... filter is 2*nFilt+1
% Outputs:
% - BEAM_VEL_UNWRAP: replaced (unwrapped) velocities
% -  MASK_IND: mx3 matric of 1/0 for whether the data was replaced or not.
%
%
% Updates
% - Jan 9, 2025:
%    Modified from Cynthia's ADV code. Biggest difference is
%    that it unwraps multiple bins and if there is a binRef specified, then it
%    will use the velocity at that bin do do a second unwrapping (necessary
%    when there are big jumps). Probably not the best method, but it works for
%    the Aquadopp data for the North Sea. 

if ~isfield(options,'vAmb')
    options.vAmb=(-min(BEAM_VEL(:))+max(BEAM_VEL(:)))/2; % estimate ambuiguity vel
    disp(options.vAmb)
end 
if length(options.vAmb)==1
    options.vAmb=options.vAmb*[1 1 1];
end

if ~isfield(options,'binRef')
    options.binRef = [];
end

%% Simple variables
vAmb = options.vAmb;
binRef = options.binRef;

%%

[NT,NZ,NB] = size(BEAM_VEL);
BEAM_VEL_UNWRAP = zeros(size(BEAM_VEL));
MASK_IND = zeros(size(BEAM_VEL));

%% Unwrap (Cynthia's method)
for zz = 1:NZ
    
    % Velocity for bin
    vOrig = squeeze(BEAM_VEL(:,zz,:));
    
    % Initialize
    vNew = vOrig;
    maskInd=zeros(size(vOrig));
    
    % Derivative (Not sure why this is needed)
    df = diff(vOrig);
    df = [df; 0 0 0];
    
    % Moving median
    y = movmedian(vOrig,[nFilt nFilt]);
    dy = y - vOrig;
    
    % Unwrap each beam    
    for jj=1:3
        indR=[];
        
        % Find velocities that are too low
        indN=find(vOrig(:,jj)<0); % Negative
        ind=find(dy(:,jj)>vAmb(jj));
        ind=intersect(indN,ind);
        vNew(ind,jj)=vOrig(ind,jj)+2*vAmb(jj);
        indR=union(indR, ind);
        
        % Find velocities that are too high
        indP=find(vOrig(:,jj)>0); % Positive
        ind=find(dy(:,jj)<-vAmb(jj));
        ind=intersect(indP,ind);
        vNew(ind,jj)=vOrig(ind,jj)-2*vAmb(jj);
        indR=union(indR, ind);
        
        % Set to one for any velocities that were unwrapped
        maskInd(indR,jj)=1;
        
        if 0
            figure(1),clf
            ax(1) = subplot(211);
            plot(vOrig(:,jj),'.-','displayname','orig'); hold on
            plot(y(:,jj),'displayname','filt')
            plot(vNew(:,jj),'.-','displayname','new')
            title(['beam = ' num2str(jj) ', zz = ' num2str(zz)])
            legend

            ax(2) = subplot(212);
            plot(dy(:,jj),'.')
            hold all
            plot(get(gca,'xlim'),[1 1]*vAmb(jj),'k')
            plot(get(gca,'xlim'),[1 1]*-vAmb(jj),'k')
            plot(indR,dy(indR,jj),'or')
            ylabel('filt - orig')

            linkaxes(ax,'x')
            pause
        end
    end
    
    BEAM_VEL_UNWRAP(:,zz,:) = vNew;
    
    if 0
        figure(2),clf
        t=1:1:length(BEAM_VEL);
        for jj=1:3
            ax(jj)=subplot(3,1,jj);
            plot(t,vOrig(:,jj),'color',.7*[1 1 1]); hold on

            plot(t,vNew(:,jj),'k-')
            plot(t,y(:,jj),'r-','linewidth',2)
        end
        title(ax(1),num2str(zz))
        pause
    end
end

%% Define a reference level and unwrap again
if ~isempty(binRef)
    vRef = movmedian(BEAM_VEL_UNWRAP(:,binRef,:),[nFilt nFilt]);

    for zz = 1:NZ
        vOrig = BEAM_VEL(:,zz,:);
        vTmp = BEAM_VEL_UNWRAP(:,zz,:);
        dy = vRef -vTmp;

        vNew = vTmp;
        for jj=1:3
            indR=[];

            % Find velocities that are too low
            ind=find(dy(:,jj)>vAmb(jj));
            vNew(ind,jj)=vTmp(ind,jj)+2*vAmb(jj);

            % Find velocities that are too high
            ind=find(dy(:,jj)<-vAmb(jj));
            vNew(ind,jj)=vTmp(ind,jj)-2*vAmb(jj);



            if 0
                figure(3),clf
                ax(1) = subplot(211);
    %             plot(vOrig(:,jj),'.-','displayname','orig'); 
                hold on
                plot(vTmp(:,jj),'.-','displayname','unwrap1')
                plot(vRef(:,jj),'displayname','reference')
                plot(vNew(:,jj),'-','displayname','new')
                title(['beam = ' num2str(jj) ', zz = ' num2str(zz)])
                legend

                ax(2) = subplot(212);
                plot(dy(:,jj),'.')
                hold all
                plot(get(gca,'xlim'),[1 1]*vAmb(jj),'k')
                plot(get(gca,'xlim'),[1 1]*-vAmb(jj),'k')
                plot(indR,dy(indR,jj),'or')
                ylabel('filt - orig')

                linkaxes(ax,'x')
                pause
            end
        end

        BEAM_VEL_UNWRAP(:,zz,:) = vNew;

    end
end

%% Define mask
vDiff = abs(BEAM_VEL_UNWRAP - BEAM_VEL);
ind = find(vDiff>1e-5);
MASK_IND(ind) = 1;

%% Plot pcolor of result
if 0
    clim = 0.1*[-1 1]; % Might need to change for different data sets
    figure(4),clf, clear ax
    for jj = 1:3
        subplot(3,3,3*jj-2);
        pcolor(BEAM_VEL(:,:,jj)'); shading flat; colorbar
        caxis(clim)
        title(['Original, Beam ',num2str(jj)])
        
        subplot(3,3,3*jj-1);
        pcolor(BEAM_VEL_UNWRAP(:,:,jj)'); shading flat; colorbar
        caxis(clim)
        title(['Unwrapped, Beam ',num2str(jj)])
        
        subplot(3,3,3*jj);
        pcolor(MASK_IND(:,:,jj)'); shading flat; colorbar
        caxis([0 1])
        title(['Mask, Beam ',num2str(jj)])

    end
    
end
