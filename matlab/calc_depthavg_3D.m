function avgfield = calc_depthavg_3D(field,z,varargin)
% Computes the depth_average of a 3D field
% Very simple function that averages over the third dimension of an
% inputted field (This should be made a lot more flexible where I specify
% which dimension to average over. 
% Inputs:
% Field = field to average (size: length(z)*length(time)
% z = vector containing depths
% varargin = depth limits (use if the average is to be computed over a subset of
% the depth)
%   example varargin = [2.1 14] (2.1 = minimum depth, 14 = maximum depth)
%
% Justine McMillan
% October 2nd, 2012

% Defaults
zind=1:length(z);
dim = 1;

if nargin >= 3
    depthlims = varargin{1};
    if ~isempty(depthlims)
        zind = find(z>=depthlims(1) & z<=depthlims(2));
        z = z(zind);
    end
end

if nargin == 4
    dim = varargin{2};
end

H = z(end)-z(1);
dz = z(2)-z(1);

if dim == 1
    field = field(zind,:,:);
elseif dim ==2
    field = field(:,zind,:);
elseif dim ==3
     field = field(:,:,zind);
end

avgfield = 1/H*nansum(field,dim)*dz;

end


