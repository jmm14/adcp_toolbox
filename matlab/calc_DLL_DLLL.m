function [r,D,D_N,binPairsOut] = calc_DLL_DLLL(vp,binPairs,dr,options)
% calculate the DLL or DLLL matrix by specifying based on the input of binPairs 
% to use in the differencing. Used to conform to ATOMIX netcdf format.
%
% Inputs:
% - vp: velocity fluctuation [matrix of size nTime x nBins]
% - binPairs: set of bin pairs to compute differences for. Note: If binPairs 
%             contains bins that are larger than nBins in vp function will
%             return Nan for output variables
% - dr: radial size of the range bins
% - options: structure with processing options including the following fields
%       * order : Order of the truct ure function (i.e. 2 for DLL, 3 for DLLL)
%       * figure : Flag to plot output
%       * methodMultDll : Method to handle multiple Dll for a single r
%                         value. Options:
%                           - None [default]: return all values
%                           - Average: return average of values for each r
%
% Outputs:
% - r: along beam separation distance (i.e. R_DEL in ATOMIX notation)
% - D: longitudinal structure function (i.e. DLL or DLLL depending on order)
% - D_N: number of points used to calculate D (often used for QAQC) 
% - binPairsOut: binPairs for returned values (Nan if points are averaged) 
% 
%
% Justine McMillan
% Feb 18, 2022
% 
% 2022-06-30: Added optional method to return average DLL values for each
%             unique r
% 2023-01-12: Added output of binPairs for returned DLL values (will be
%             nan if values are averaged together) 

%dz = mean(diff(z));
[nTime,nBins] = size(vp);
if nBins>nTime % Likely that nt will be larger than nb
    disp('transposing vp - rows should be time, columns should be bins')
    vp = vp';
end
[nTime,nBins] = size(vp);

%% Set defaults
if ~isfield(options,'figure'); options.figure = 0; end
if ~isfield(options,'order') % order of structure function 
    options.order = 2; 
    warning('Using a default order of 2')
end 
if ~isfield(options,'methodMultDll'); options.methodMultDll = 'None'; end

%% initialize

[nPts,~] = size(binPairs);

r = NaN*ones(1,nPts);
D = NaN*ones(1,nPts);
D_N = NaN*ones(1,nPts);
binPairsOut = NaN*ones(nPts,2);

%% Compute velocity differences and dissipation rate
for pp = 1:nPts
    binL = binPairs(pp,1);
    binU = binPairs(pp,2);
    
    if binU <= nBins
        r(pp) = (binU - binL)*dr;
        vdiff = vp(:,binL)-vp(:,binU);

        D(pp) = nanmean((vdiff.^options.order));
        D_N(pp) = sum(~isnan(vdiff)); % number of observations used to compute DLL

        % DEBUGGING FIGURE
        if options.figure == 1
            figure(2),clf
            ax(1) = subplot(211);
            plot(vp(:,binL),'o-')
            hold all
            plot(vp(:,binU-1),'o-')
            nL = sum(~isnan(vp(:,binL)));
            nU = sum(~isnan(vp(:,binU-1)));
            legend(['npts = ',num2str(nL)],['npts = ',num2str(nU)])
            ylabel('v')

            ax(2) = subplot(212);
            plot(vdiff.^2);
            hold all
            plot(get(gca,'xlim'),D(pp)*[1 1],'k')
            title(['binL = ',num2str(binL),',   binU = ',num2str(binU),...
                ',   D = ',num2str(D(pp),'%3.1e'), ', N = ',num2str(D_N(pp))])
            linkaxes(ax,'x')
            ylabel('(\Delta v)^2')
            xlabel('index')
            pause
        end
%     else
%         disp(['Unable to compute difference for bin pair ',num2str([binL,binU])])
    end
end

if options.figure
    figure(3),clf
    plot(r,D,'o')
    xlabel('r')
    ylabel('D')
end
    

switch options.methodMultDll
    case 'None'
        binPairsOut = binPairs;
        return
    case 'Average'
        ind = find(~isnan(r));
        [rUnique, iA, iC] = unique(r(ind));
        nPts = length(rUnique);
        if nPts > 0
            rFinal = NaN*ones(1,nPts);
            DFinal = NaN*ones(1,nPts);
            D_N_Final = NaN*ones(1,nPts);
            binPairsOutFinal = NaN*ones(nPts,2);

            for rr = 1:length(rUnique)
                indR = find(r == rUnique(rr));
                rFinal(rr) = rUnique(rr);
                DFinal(rr) = mean(D(indR));
                D_N_Final(rr) = min(D_N(indR)); % TODO: Would be better to apply threshold here and remove bad data points before averaging?
                if length(indR) == 1
                    binPairsOutFinal(rr,:) = binPairs(indR,:);
                end
            end
            
            if options.figure
                figure(3)
                hold all
                plot(rFinal,DFinal,'*')
                pause
            end

            r = rFinal;
            D = DFinal;
            D_N = D_N_Final;
            binPairsOut = binPairsOutFinal;
            
            
        else
            r = NaN;
            D = NaN;
            D_N = NaN;
            binPairsOut = [NaN NaN];
        end
        
end
