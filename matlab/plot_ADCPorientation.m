function [fig_h,ax_h,p,pname] = plot_ADCPorientation(angles,options)
%% function [fig_h,ax_h,p,pname] = plot_ADCPorientation(angles,options)
% Plots the orientation of the ADCP's coordinates in relation to True North
% and the flood and ebb directions of the flow.
%
% Inputs:
%   - angles: Structure with angles to plot (fields = fld, ebb, hdg)
%   - options:Structure containing options
%       1) holdon = 1: hold on the current figure
%       2) legendon = 1: include a legend
%       3) type = 'RDI' (or 'Nortek')
% Outputs:
%   - fig_h: figure handle
%   - ax_h:  axes handle
%
% Justine McMillan
% November 14, 2013
%
% Feb 23, 2014 - Added option to include second heading
% Mar 14, 2016 - Modified to enable multiple subplots. Also returns handles
%               for lines and legend labels.
% Jan 27, 2024 - Added option for Nortek instruments


%% Defaults
if ~isfield(options,'holdon')
    options.holdon = 0;
end
if ~isfield(options,'legendon')
    options.legendon = 1;
end
if ~isfield(options,'type')
    options.type = 'RDI';
end

if options.holdon == 1
    fig_h = gcf; hold on
    ax_h = gca;
    
else
    %     myfigure('ADCP_Orientation');
        fig_h=gcf;
    %     ax_h = subplot(1,1,1);
    ax_h = gca;
end

pnum = 0;


if isfield(angles,'hdg')
    pnum = pnum+1;
    switch options.type
        % Note: These angles are not relative to standard x,y axes because of view(90,-90) used below.
        %       Angles are in bearing coordinates with 0 being north, 90 being east, etc.
        %       In all cases, compass(cosd(angle), sind(angle)), where "angle" is CW between true north and the vector. 
        case 'RDI' 
            
            p(pnum) = compass(cosd(angles.hdg-90),sind(angles.hdg-90),'g'); % Angle between X axis and true north is heading - 90
            hold all
            pname{pnum} = '1->2, V_X (rdi def)';
            pnum = pnum+1;
            p(pnum) = compass(cosd(angles.hdg+90),sind(angles.hdg+90),'--g'); %Angle between Y axis and true North is heading
            pname{pnum} = '2->1, V_Y (right-handed def)'
            pnum = pnum+1;
            p(pnum) = compass(cosd(angles.hdg),sind(angles.hdg),'k'); %Angle between Y axis and true North is heading
            pname{pnum} = '4->3, V_Y (rdi def)';
        case 'Nortek' 
            p(pnum) = compass(cosd(angles.hdg),sind(angles.hdg),'g'); % Angle between X axis and true north is heading 
            hold all
            pname{pnum} = '3->1, V_X (nortek def)';
            pnum = pnum+1;
            p(pnum) = compass(cosd(angles.hdg-90),sind(angles.hdg-90),'k'); % Angle between Y axis and true north is heading - 90
            pname{pnum} = '2->4, V_Y (nortek def)';
    end
end

if isfield(angles,'hdg2')
    pnum = pnum+1;
    p(pnum) = compass(cosd(angles.hdg2-90),sind(angles.hdg2-90),'g--'); hold all
    pname{pnum} = '1-2, V_X (after shift)';
    pnum = pnum+1;
    p(pnum) = compass(cosd(angles.hdg2),sind(angles.hdg2),'k--');
    pname{pnum} = '4-3, V_Y (after shift)';
end

if isfield(angles,'fld')
    pnum = pnum+1;
    p(pnum)=compass(cosd(angles.fld),sind(angles.fld),'r'); hold all %Angle between flood axis and true north
    pname{pnum} = 'Flood';
end
if isfield(angles,'ebb')
    pnum = pnum+1;
    p(pnum) = compass(cosd(angles.ebb),sind(angles.ebb),'b'); hold all %Angle between ebb axis and true north
    pname{pnum} = 'Ebb';
end

if isfield(angles,'bathgrad')
    pnum = pnum+1;
    p(pnum) = compass(cosd(angles.bathgrad),sind(angles.bathgrad),'m'); hold all
    pname{pnum} = 'Bath Grad';
end
set(p,'linewidth',2)
view(90, -90)


if options.legendon == 1
    legend(p,pname)
end

