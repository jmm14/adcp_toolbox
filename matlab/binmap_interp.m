function [adcp,cfg]=binmap_interp(adcp,cfg,varargin)
% Function to interpolate beam velocities so that beams are "sampling" from
% the same heights.
% This function is run before the coordinate transformations from beam
% coordinates to ENU. 
%
%
% Justine McMillan
% Jan 30, 2014
%
% Updates:
%  - Feb 6/14: Added correlations and amplitude
%  - Feb 10/14: Made correlations and amplitude optional

if nargin <=2
    flg.ampcorr = 0;
end

%% Initialize
z1 = zeros(length(cfg.ranges),length(adcp.mtime));
z2 = zeros(length(cfg.ranges),length(adcp.mtime));
z3 = zeros(length(cfg.ranges),length(adcp.mtime));
z4 = zeros(length(cfg.ranges),length(adcp.mtime));

%% Compute z values at which velocities were measured
z1 = cfg.ranges*(cosd(adcp.pitch).*(cosd(adcp.roll)-tand(cfg.beam_angle).*sind(adcp.roll)));
z2 = cfg.ranges*(cosd(adcp.pitch).*(cosd(adcp.roll)+tand(cfg.beam_angle).*sind(adcp.roll)));
z3 = cfg.ranges*(cosd(adcp.roll).*(cosd(adcp.pitch)+tand(cfg.beam_angle).*sind(adcp.pitch)));
z4 = cfg.ranges*(cosd(adcp.roll).*(cosd(adcp.pitch)-tand(cfg.beam_angle).*sind(adcp.pitch)));

if size(z1,1) == 1
    z1 = z1';
    z2 = z2';
    z3 = z3';
    z4 = z4';
end
    

%% interpolate
zinterp = cfg.ranges;


for ii = 1:length(adcp.mtime)
    adcp.v1(:,ii) = interp1(z1(:,ii),adcp.v1(:,ii),zinterp);
    adcp.v2(:,ii) = interp1(z2(:,ii),adcp.v2(:,ii),zinterp);
    adcp.v3(:,ii) = interp1(z3(:,ii),adcp.v3(:,ii),zinterp);
    adcp.v4(:,ii) = interp1(z4(:,ii),adcp.v4(:,ii),zinterp);
    
    if flg.ampcorr
    adcp.intens(:,1,ii) = interp1(z1(:,ii),adcp.intens(:,1,ii),zinterp);
    adcp.intens(:,2,ii) = interp1(z2(:,ii),adcp.intens(:,2,ii),zinterp);
    adcp.intens(:,3,ii) = interp1(z3(:,ii),adcp.intens(:,3,ii),zinterp);
    adcp.intens(:,4,ii) = interp1(z4(:,ii),adcp.intens(:,4,ii),zinterp);
    
    adcp.corr(:,1,ii) = interp1(z1(:,ii),adcp.corr(:,1,ii),zinterp);
    adcp.corr(:,2,ii) = interp1(z2(:,ii),adcp.corr(:,2,ii),zinterp);
    adcp.corr(:,3,ii) = interp1(z3(:,ii),adcp.corr(:,3,ii),zinterp);
    adcp.corr(:,4,ii) = interp1(z4(:,ii),adcp.corr(:,4,ii),zinterp);
    end
    
end


%% Update configuration
cfg.binmap = mfilename;
