function [vE,vN,vU] = Aquadopp_xyz_2enu(vX,vY,vZ,heading,pitch,roll)
% XYZ2ENU.M
% Converts the coordinate xyz velocities from the Aquadopp to east, north, up (ENU) velocities.  Based on
% transform.m provided by Nortek
% MSK 3/22/10

% To use this program, open XYZ folder for dates of interest and open a .mat
% file.  The size of the .mat file tells you the no_pings (the first
% number) and the no_cells (the second number).  Also note the number of
% .mat files in the folder - this is no_bursts.  Remember to create an XYZ
% folder in the filepath.

% close all
% clear all
% 
% no_bursts = 3879; 
% no_samples = 507;
% no_cells = 28;
% filepath = '/Users/Danielle/Research/Intrusions/Data/Aquadopp/AHL 8.08/';

[NT,NZ] = size(vX);
vE = NaN*ones(NT,NZ);
vN = NaN*ones(NT,NZ);
vU = NaN*ones(NT,NZ);

% Transformation matrix for beam to xyz coordinates,
% the values can be found from the header file that is generated in the
% conversion of data to ASCII format

T =[6461 -3232 -3232;...
    0 -5596 5596;...
    1506 1506 1506];

% Scale the transformation matrix correctly to floating point numbers
T = T/4096;
T_org = T;

% for i = 1:no_bursts
%     load([filepath 'XYZ/xyz_vel_' num2str(i)])
%     load([filepath 'SEN/sen_' num2str(i)])
    for j = 1:NT
        disp_percdone(j,NT,10);
        for k = 1:NZ
            % heading, pitch and roll are the angles output in the data in degrees
            hh = pi*(heading(j)-90)/180;
            pp = pi*pitch(j)/180;
            rr = pi*roll(j)/180;
            % Make heading matrix
            H = [cos(hh) sin(hh) 0; -sin(hh) cos(hh) 0; 0 0 1];
            % Make tilt matrix
            P = [cos(pp) -sin(pp)*sin(rr) -cos(rr)*sin(pp);...
                0 cos(rr) -sin(rr);...  
                sin(pp) sin(rr)*cos(pp) cos(pp)*cos(rr)];
            % Make resulting transformation matrix
            R = H*P*T;
            xyz = [vX(j,k) vY(j,k) vZ(j,k)]';
            enu = R*inv(T_org)*xyz;
            vE(j,k) = enu(1);
            vN(j,k) = enu(2);
            vU(j,k) = enu(3);
        end
    end
%     filename = [filepath 'ENU/enu_' num2str(i)];
%     save(filename, 'east', 'north', 'up')
% end