function spd_signed = sign_speed_slacks(spd,time,fldflg)
%% function spd_signed = sign_speed_slacks(spd,time,fldflg)
% sign the speed based on slack water
% fldflg = 1 if first index is a flood tide
%
% Justine McMillan
% Nov 25, 2015

debug = 0;
spd_signed = spd;

dt = (time(2)-time(1))*24*3600;
mpd = floor(4*3600/dt);
[~,ind_slack] = findpeaks(-spd,'minpeakdistance',mpd);

% fix first point
if spd(ind_slack(1))>0.5
    ind_slack = ind_slack(2:end);
end

%Change ebb tides to be a negative speed
if fldflg == 1 
    ind_ii = 1:2:length(ind_slack);
else
    ind_ii = 2:2:length(ind_slack);
end

for ii = ind_ii
    if ii~=length(ind_slack)
        ind = ind_slack(ii):ind_slack(ii+1)-1;
    else
        ind = ind_slack(ii):length(time);
    end
    spd_signed(ind) = -spd(ind);
end

if debug 
    figure(100),clf
    plot(time,spd_signed)
    hold all
    plot(time(ind_slack),spd_signed(ind_slack),'o')
   % pause
end