function [D,D_N,binL,binU] = calc_DLL_DLLL_forward_diff(vp,options)
%% [D,D_N,binL,binU] = calc_DLL_DLLL_forward_diff(vp,options)
% calculate the DLL matrix for all bin pairs using 'forward'
% differencing methods. Used to conform to ATOMIX netcdf format.
%
% Inputs:
% - vp: velocity fluctuation [matrix of size nTime x nBins]
% - options: structure with processing options including the following fields
%       * order : Order of the structure function (i.e. 2 for DLL, 3 for DLLL)
%       * figure : Flag to plot output
%
% Outputs:
% - D: longitudinal structure function (i.e. DLL or DLLL depending on order)
% TODO: Explain format of D -- rows are z levels and columns are delta
% values
% - D_N: number of points used to calculate D (often used for QAQC)
% - binL: lower bin used to compute DLL
% - binU: upper bin used to compute DLL
%
%
% Justine McMillan
% Mar 29, 2023

[nTime,nBins] = size(vp);
if nBins>nTime % Likely that nt will be larger than nb
    disp('transposing vp - rows should be time, columns should be bins')
    vp = vp';
end
[nTime,nBins] = size(vp);

%% Set defaults
if ~isfield(options,'figure'); options.figure = 0; end
if ~isfield(options,'order') % order of structure function
    options.order = 2;
    warning('Using a default order of 2')
end

%% Initialize
D    = NaN*ones(nBins,nBins-1); % Structure function
D_N  = NaN*ones(nBins,nBins-1); % Number of points used in SF calculation
binL = NaN*ones(nBins,nBins-1); % Lower bin number used in differencing calculation
binU = NaN*ones(nBins,nBins-1); % Upper bin number used in differencing calculation

%% Compute velocity differences
for zz = 1:nBins
    vRef = vp(:,zz);
    
    % Differences with all bins
    vDiff = vp - vRef;
    
    % DLL values
    Dtmp = nanmean((vDiff.^options.order));
    D_Ntmp = sum(~isnan(vDiff)); % number of observations used to compute DLL
    
    % Assign to matrix (ATOMIX format, columns are increasing delta values)
    indGood = [zz+1:nBins];
    D(zz,1:length(indGood)) = Dtmp(indGood);
    D_N(zz,1:length(indGood)) = D_Ntmp(indGood);
    
    % Save bin pairs
    binL(zz,:) = zz;
    binU(zz,1:length(indGood)) = zz+1:nBins;
    
    % Debugging figure (plot only one velocity pair for simplicity)
    if options.figure == 1
        indRow = zz;
        indCol = 5; % Number of bin separations
        
        figBinL = binL(indRow,indCol);
        figBinU = binU(indRow,indCol);
        figD = D(indRow,indCol);
        figDN = D_N(indRow,indCol);
        
        if ~isnan(figBinL) && ~isnan(figBinU)
            % Check
            figvDiff = vp(:,figBinU) - vp(:,figBinL);
            Dcheck = nanmean(figvDiff.^options.order);
            
            disp(['Plotting bins ',num2str(figBinL),' and ',num2str(figBinU)])
            disp(['D_check = ',num2str(Dcheck)])
            disp(['D_out = ',num2str(figD)])
            if abs(Dcheck-figD)>1e-5
                warning('D values are inconsistent')
            end
            
            figure(1),clf
            ax(1) = subplot(211);
            plot(vp(:,figBinL),'o-')
            hold all
            plot(vp(:,figBinU),'o-')
            nL = sum(~isnan(vp(:,figBinL)));
            nU = sum(~isnan(vp(:,figBinU)));
            legend(['binL = ',num2str(figBinL),', npts = ',num2str(nL)],...
                ['binU = ',num2str(figBinU),', npts = ',num2str(nU)])
            ylabel('v')
            
            ax(2) = subplot(212);
            
            plot(figvDiff.^options.order);
            hold all
            plot(get(gca,'xlim'),figD*[1 1],'k')
            title(['binL = ',num2str(figBinL),',   binU = ',num2str(figBinU),...
                ',   D = ',num2str(figD,'%3.1e'), ', N = ',num2str(figDN)])
            linkaxes(ax,'x')
            ylabel('(\Delta v)^2')
            xlabel('index')
            pause
        end
    end
    
    
end
