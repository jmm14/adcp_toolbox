function [ebb fld] = calc_BottomDrag(data,options)

%% Determines the bottom friction coefficient
%
% Modification of code from swnsra2012 (doesn't do ensemble averaging)
% Uses structs to store velocities. 
%
% Inputs: 
% data.spd = a matrix of the flow speed (already averaged), neg values for
%               ebb tide
% data.z = z ranges
% data.pres = pressure
% 
% options.zpic = vector of z values to take horizontal slices
% options.zdet = z value to use to determine ebb vs fld
% options.Nmin = number of minutes in ensemble averages
% options.spdbins = bins to use in computation of average speeds
% options.FitFunc = string giving name of the function used to fit ('WallLaw')
% options.zlim_fit = [zmin zmax]: vertical limits for fits
% options.z_refspd = z value for determination of reference speed (depth average
% from the bottom to this height)
% options.phase = 0 (flood & ebb), 1 (flood), -1 (ebb)
%
%
% Justine McMillan
% July 4th, 2013
%
% Jan 10, 2014: Added phase option

disp(['# Function: ',mfilename('fullpath')])


MIN2SEC = 60;
HR2SEC = 3600;
DAY2SEC = 3600*24;
DAY2MIN = 24*60;
nz = length(data.z);

%% Default options
if ~isfield(options,'phase')
    options.phase = 0;
end

%% Figure properties
defaultfigsize = [900   590   969   350];
defaultaxespos = [0.05 0.15, 0.92 0.78];
set(0,'defaultAxesFontSize',14)

%% Choose z levels
npic = length(options.zpic);
for ii=1:npic
    [~, iz(ii)] = min(abs(data.z-options.zpic(ii)));
    zpic(ii) = data.z(iz(ii));
end


%% Plot V vs U at zpic
if options.show.scatter
    myfigure(['VvsU'])
    for ii = 1:npic
        subplot(2,2,ii)
        plot(data.u(:,iz(ii)),data.v(:,iz(ii)),'.')
        xlim(options.show.clim*[-1 1])
        ylim(options.show.clim*[-1 1])
        axis('equal')
        xlabel('u (m/s)')
        ylabel('v (m/s)')
        title(['z = ',num2str(zpic(ii)),' m'])
    end
end


%% depth average the speed data (to 95% of surface signal)

dmin = min(data.z);
for ii = 1:length(data.pres)
             
    ind = find(data.z<0.95*data.pres(ii));
    dmax = data.z(ind(end));
    
    davglims(ii,:) = [dmin dmax];
    
    davgspd(ii) = calc_depthavg(data.spd(ii,:),data.z,davglims(ii,:),2);
    
end


%% Separate flood and ebb
fld.ind = find(davgspd > 0);
ebb.ind = find(davgspd < 0);

%% Determine the speed to use to separate into bins
if ~isnan(options.zdet) %Speed at a reference height
    [~, izdet] = min(abs(data.z-options.zdet));
    zdet = z(izdet);
    disp(['Determining speed bins based on z = ',num2str(zdet),' m measurements'])
    Spd.ref = ens_spd(:,izdet);
else %depth avg speed
    disp(['Determining speed bins based on depth average speed'])
    Spd.ref = davgspd;
end

%% get Speed profiles separately for Ebb and flood, perform fits, estimate u* and z0

disp(['Using the ',options.FitFunc,' to fit'])

%set up bins
if isnan(options.zdet)
    SpdBins.info='Chosen bins to compute averages. Profiles are sorted based on depth-averaged speed';
else
    SpdBins.info='Chosen bins to compute averages. Profiles are sorted based on mid-depth speed';
end
SpdBins.Bins = options.spdbins; %in m/s
SpdBins.nBins = length(SpdBins.Bins);

for ii=1:2 %repeat for flood and ebb
    if ii == 1 && options.phase == -1;
        disp('Flood only')
        continue
    elseif ii == 2 && options.phase == 1;
        disp('ebb only')
        continue
    end
        
    if ii==1
        Tide='fld';
        tind = fld.ind;
        measavgSpd = data.spd(tind,:);
        measrefSpd = Spd.ref(tind); %Measured speed at depthavg or mid-depth during flood/ebb tide
        %depth over which to do fit
        zmin = options.zlim_fitfld(1);
        zmax = options.zlim_fitfld(2);
    else
        Tide='ebb';
        tind = ebb.ind;
        %Get absolute value
        measavgSpd = abs(data.spd(tind,:));
        measrefSpd = abs(Spd.ref(tind)); %Measured speed at mid-depth during flood/ebb tide
        zmin = options.zlim_fitebb(1);
        zmax = options.zlim_fitebb(2);
    end
    
    % get z values for fit
    [~, izmax] = min(abs(data.z-zmax));
    [~, izmin] = min(abs(data.z-zmin));
    zlimfit = [data.z(izmin) data.z(izmax)];
    
    %initialize
    barSpd  = zeros(1,SpdBins.nBins); %Mean reference speed within bin (either depth averaged or middepth)
    stdSpd  = zeros(1,SpdBins.nBins); %Std-dev of reference speed within bin (either depth averaged or middepth)
    barProfSpd = zeros(SpdBins.nBins,nz); %Mean profile within bin
    stdProfSpd = zeros(SpdBins.nBins,nz); %Std-dev profile within bin
    
    %Determining if the reference speed is within that bin
    for jj = 1:SpdBins.nBins
        SpdBins.Bins(jj)
        if jj < SpdBins.nBins
            ix = find(measrefSpd >= SpdBins.Bins(jj) & measrefSpd < SpdBins.Bins(jj+1));
        else
            ix = find(measrefSpd >= SpdBins.Bins(jj));
        end
        
       % if length(ix) == 0
      %      error(['Bin ',num2str(jj),' does not have any points in it. ADJUST SIZES AND RETRY'])
      %  end
        
        barProfSpd(jj,:) = nanmean(measavgSpd(ix,:));
        stdProfSpd(jj,:) = nanstd(measavgSpd(ix,:));
        barSpd(jj) = nanmean(measrefSpd(ix));
        stdSpd(jj) = nanstd(measrefSpd(ix));
        
    end
    
    
    %plot the binned speed profiles on regular and semilog axis
    myfigure(['BinnedSpeed',Tide])
    set(gcf,'Position',[281   590   900   700])
    
    subplot(211)
    plot(barProfSpd,data.z,'-b.')
    ylim([0 zpic(end)]),ylabel('height (m)')
    xlim([0 max(SpdBins.Bins)+0.3]),xlabel('Binned Speed (m/s)')
    
    subplot(212)
    semilogy(barProfSpd,data.z,'-b.')
    ylim([0 zpic(end)]),ylabel('height (m)')
    xlim([0 max(SpdBins.Bins)+0.3]),xlabel('Binned Speed (m/s)')
    
    %%% Law of the Wall Fits (should try defect law instead? could have just done a linear fit)
   
    kappa=0.4;
        
    ustar = NaN*zeros(1,SpdBins.nBins);
    z0 = NaN*zeros(1,SpdBins.nBins);
    nLinFitSpd=NaN*zeros(SpdBins.nBins,izmax-izmin+1);
    for jj = 1:SpdBins.nBins %drop lowest Bin
        datafit.z = data.z(izmin:izmax)';
        datafit.spd = barProfSpd(jj,izmin:izmax);
                
        if isequal(options.FitFunc,'WallLaw')
            optionsfit.zlim_fit = [0 max(datafit.z)];
            optionsfit.dispfit = 0;
            [ustar(jj), z0(jj)] = calc_lawofthewall(datafit,optionsfit); %Using law of the wall function
           % pause
            nLinFitSpd(jj,:) = ustar(jj)/kappa*log(data.z(izmin:izmax)./z0(jj));
            refSpd1m(jj) = ustar(jj)./kappa*log(1./z0(jj)); %Theoretical speed at 1m
            %figure(6)
            %plot(nLinFitSpd(jj,:),data.z(izmin:izmax),'g')
        else
            error('Not set up for other functions')
        end
    end
    
    myfigure(['BinnedSpeed',Tide])
    set(gcf,'Position',[281   590   900   700])
    subplot(211)
    semilogy(barProfSpd,data.z,'-b.')
    hold on
    semilogy(nLinFitSpd,data.z(izmin:izmax),'r')
    ylim([0 zpic(end)]),ylabel('height (m)')
    xlim([0 max(SpdBins.Bins)+0.3]),xlabel('Binned Speed (m/s)')
    title([Tide, 'Tide'])
        
    disp('Getting reference speed as mean of vertical averages within each bin');
    refSpddavg = barSpd;
    refSpddavgLim = ['Reference speed (for Cd calc) was mean speed of the vertically averaged profiles within the bin'];
    
    %Plot of ustar vs reference speed -- with fit, estimate Cd
    subplot(223)
    plot(refSpd1m,ustar,'.')
    hold on
    plot(refSpddavg,ustar,'r.')
    legend('refSpd @ 1m','refSpd - davg')
    xlimits = [0 2];
    
    ij=find(~isnan(refSpd1m)&~isnan(ustar));
    %[pp1m ss1m] = polyfit(refSpd1m(ij),ustar(ij),1);
    [pp1m] = polyfitZero(refSpd1m(ij),ustar(ij),1);
    plot([0 xlimits],([0 xlimits]*pp1m(1)+pp1m(2)),'b')
    ij=find(~isnan(refSpddavg)&~isnan(ustar));
    %[ppdavg ssdavg] = polyfit(refSpddavg(ij),ustar(ij),1);
    [ppdavg] = polyfitZero(refSpddavg(ij),ustar(ij),1);
    plot([0 xlimits],([0 xlimits]*ppdavg(1)+ppdavg(2)),'r')
    
    Cd1m = pp1m(1)^2;
    text(.5,0.2,['C_d = ',num2str(Cd1m,3)])
    Cddavg = ppdavg(1)^2;
    text(1,0.05,['C_d = ',num2str(Cddavg,3)])
    
    xlim(xlimits),xlabel('Current Speed (m/s)')
    ylim([-0.05 0.4]),ylabel('u_*(m/s)')
    
    plot(xlimits,[0 0],'--')
    
    
    %Plot z0 vs reference speed
    subplot(224)
    plot(refSpd1m,z0,'.')
    hold on
    plot(refSpddavg,z0,'r.')
    xlim(xlimits),xlabel('Current Speed (m/s)')
    ylabel('z_0 (m)')
    
    z0mean = nanmean(z0);
    plot(xlimits,[z0mean,z0mean],'--r')
    
    %Save the data in corresponding structure
    vars = {'barProfSpd','stdProfSpd','barSpd','stdSpd','nLinFitSpd',...
        'refSpd1m','refSpddavg','ustar','z0','Cd1m','Cddavg',...
        'zlimfit','refSpddavgLim','SpdBins'};
    for jj=1:length(vars)
        eval([Tide,'.',char(vars(jj)),'=',char(vars(jj)),';'])
        if strcmp(vars(jj),'SpdBins') == 0
            clear(char(vars(jj)))
        end
    end
    
end

%% TEST CODE (Copy to editor or new script to run)
% tvec = [0:0.5:50];
% data.z = [.1:0.25:10]';
% data.pres = 1*sin(tvec)+9;
% ustar = 0.2*sin(tvec);
% z0 = 0.001;
% 
% for ii = 1:length(tvec)
%     U(ii,:) = ustar(ii)/0.4*log(data.z/z0);
% end
% 
% data.spd = U;
% data.u = 0*data.spd;
% data.v = data.spd;
% 
% 
% 
% % figure(1),clf
% % imagesc(tvec,data.z,data.spd)
% % set(gca,'Ydir','norm')
% % hold on
% % plot(tvec,data.pres)
% 
% options.spdbins = [0:0.25:1.5];
% options.FitFunc = 'WallLaw';
% options.zpic = [5,10,15];
% options.show.scatter = 0;
% options.show.clim=1.5;
% options.zdet = NaN;
% options.zlim_fitfld = [0 5];
% options.zlim_fitebb = [0 5];
% 
% [ebb, fld] = calc_BottomDrag(data,options)

