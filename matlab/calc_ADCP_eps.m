function eps = calc_ADCP_eps(k,Skk1,Skk2,klim)
% Calculate the dissipation rate from ADCP data using along stream beam
% pairs
% Inputs: 
% - k: A vector with the wavenumbers
% - Skk1: A vector of the spectra from one beam (already averaged, denoised)
% - Skk2: A vector of the spectra from the other beam (already averaged, denoised)
% - klim: The wavenumber limits to use for the estimation of epsilon
%
% Output:
% - eps: The estimated dissipation rate

%
% Justine McMillan
% Aug 24, 2015

ind_k = find(k>klim(1) & k<klim(2));
S = Skk1+Skk2;


eps = 0.70*(nanmean(S(ind_k,:).*k(ind_k).^(5/3)))^1.5;

if 0 % debug figure
    myfigure('calc_eps')
    subplot(121)
    loglog(k,[Skk1 Skk2])
    hold all
    fill_transparent(klim,get(gca,'Ylim'),'g')
    
    subplot(122)
    plot(k,S.*k.^(5/3))
    ylim([0 4e-3])
    hold all
    fill_transparent(klim,get(gca,'Ylim'),'g')
    plot(get(gca,'Xlim'),nanmean(S(ind_k,:).*k(ind_k).^(5/3))*[1 1],'k','linewidth',3)
    
    pause
end