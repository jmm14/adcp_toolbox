% Edited by Emily to fix year day and include figure labels

function adcpwaves = RDI_adcpwaves_JH(datafile,plotFlag)
%ADCP waves data
% input:
%       datafile
%       plotFlag (default = 0)
%
% output: 
%       adcpwaves: structure containing wave data obtained from log8.txt file output 
%           from WavesMon
%
% LOG8 file info:
% col1: burst number
% col2: YY
% col3: MM
% col4: DD
% col5: HH
% col6: mm
% col7: ss
% col8: cc (2 fixed digits of 1/100th of a second)
% col9: Hs significant wave height (m)
% col10: Tp peak wave period (s)
% col11: Dp peak wave direction (deg)
% col12: depth (mm)
% col13: H1/10 10%highest waves
% col14: Tmean mean period (s)
% col15: Dmean mean peak wave direction (deg)
% col16: number of bins
% col17: depth level 1 magnitude (m/s)
% col18: depth level 1 direction (deg)
% colNMagnitude: depth level N magnitude (m/s)
% colNDirection: depth level N direction (deg)
%
% created by Jenna Hare on 13-Jan-2014

if nargin == 1
    plotFlag = 0;
end
    

data = load(datafile);

%time
YY = data(:,2)+2000;
MM = data(:,3);
DD = data(:,4);
HH = data(:,5);
mm = data(:,6);
ss = data(:,7);
cc = data(:,8);
tvec = datenum(YY,MM,DD,HH,mm,[ss+cc/100]);
yd = tvec - datenum(YY-1,12,31);

Hs = data(:,9); %significant wave height
Tp = data(:,10); %peak wave period
Dp = data(:,11); %peak wave direction
depth = data(:,12)/1000; %depth (m)

H10 = data(:,13); %10%higest waves
Tmean = data(:,14); %mean period
Dmean = data(:,15); %mean peak wave direction

bins = data(:,16); %number of bins

col17 = data(:,17); %depth level 1 magnitude (m/s)
% col18 = data(:,18); %depth level 1 direction (deg)
% col19 = data(:,19);
% col20 = data(:,20);

magnitude = data(:,17:2:end)'; %range x time
direction = data(:,18:2:end)';


%test figures
if plotFlag == 1
    figure(1),clf
    subplot(311)
    plot([yd(1),yd(end)],[0 0],'c')
    hold on
    plot(yd,Hs)
    xlabel('Day of year')
    ylabel('H_s (m)')
   
    subplot(312)
    plot(yd,Dp)
    xlabel('Day of year')
    ylabel('D_p (deg)')
    
    subplot(313)
    plot(yd,Tp)
    xlabel('Day of year')
    ylabel('T_p (s)')
    
    pause
    
    figure(2),clf
    plot(yd,col17,'.-')
    xlabel('Day of year')
    ylabel('depth level (m/s)')
    
    pause
    
    figure(3),clf
    ax(1) = subplot(211);
    imagesc(yd,1:42,magnitude),colorbar
    caxis([0 0.5])
    xlabel('time (yd)')
    ylabel('bins')
    title(file,'Interpreter','None')
    ax(2) = subplot(212);
    imagesc(yd,1:42,direction), colorbar
    caxis([0 360])    

    set(ax,'YDir','Normal')

    cbwidth
end

%output structure
adcpwaves.time = tvec';
adcpwaves.significantWaveHeight = Hs';
adcpwaves.peakWavePeriod = Tp';
adcpwaves.peakWaveDirection = Dp';
adcpwaves.depth = depth';
adcpwaves.H10 = H10';
adcpwaves.meanPeriod = Tmean';
adcpwaves.meanPeakWaveDirection = Dmean';
adcpwaves.bins = bins';
adcpwaves.magnitude = magnitude;
adcpwaves.direction = direction;

%comments

Comments = {['Created using function RDI_adcpwaves_JH on ',date];
    'Adcpwaves structure was created from output LOG8 file WavesMon program';
    'Most fields are self-explanatory';
    'time: yearday';
    'H10: 10%higest waves';
    'water depth: (m)'};

adcpwaves.comments = Comments;


