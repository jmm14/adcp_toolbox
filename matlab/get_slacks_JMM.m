function  [ind_slack,ind_EtoF,ind_FtoE,times] = get_slacks_JMM(signed_spd, time, options)
%% function [ind_slack,ind_EtoF,ind_FtoE] = get_slacks_JMM(signed_spd,time,options)
% Identifies slack waters in time series by finding the extrema of the input time series. This function uses a different
% method than Brian Polagye
%
%
%%Inputs:
%   signed_spd: signed speed series (m/s)
%   time: times (matlab serial format) associated with each speed
%   options: structure containing...
%       - T_thresh: the minimum time between slacks (in hours)
%       - smoothpts: number of points to smooth over
%       - slack_max: maximum speed to be considered slack
%       - slack_dur: slack duration over which to compute acceleration
%       - figure:    1 if you want to show a figure
%       - interp:    1 if you want to interpolate the slack water time
%
%Outputs:
%   ind_slack: indices of slack tide
%   ind_EtoF: indices when ebb changes to flood
%   ind_FtoE: indices when flood changes to ebb
%   times: structure with times corresponding to slacks (interpolated if that is the desired method) 
%
% Justine McMillan
% Nov 17th, 2013
%
% Jan 16, 2013 - added options.interp to interpolate the time of slack
%                water (particuarly useful when ensembles are longer)
% Apr 15, 2014 - used a butterworth filter to get rid of high frequency noise
% 
% NOTES: Can remove the use of the "smooth" function since the butterworth
% filter is now used


%% Default options
if ~isfield(options,'T_thresh'),  options.T_thresh = 4;     end
if ~isfield(options,'smoothpts'), options.smoothpts = 3;   end
if ~isfield(options,'slack_max'), options.slack_max = 0.2; end
if ~isfield(options,'slack_dur'), options.slack_dur = 1;   end
if ~isfield(options,'figure'),    options.figure = 1;      end
if ~isfield(options,'interp'),    options.interp = 0;      end

%%
dt = mean(diff(time));


% Smooth speed
fN = 0.5*(1/mean(dt));
fc = 1/(3/24); %3 hour cutoff period
[b,a] = butter(5,fc/fN);
spdsm = filtfilt(b,a,signed_spd);
%spdsm = smooth(signed_spd, options.smoothpts);
mpd = floor(options.T_thresh/24/dt);

[~,ind_slack]=findpeaks(-abs(spdsm),'minpeakdistance',mpd); %Slack water
slack_dur_pts = round2(options.slack_dur/24/dt,2);
ind_keep = find(ind_slack>slack_dur_pts & ind_slack< length(spdsm)-slack_dur_pts);
ind_slack = ind_slack(ind_keep);
%remove points that aren't actually slack
ind = find(abs(spdsm(ind_slack))<options.slack_max);
ind_slack = ind_slack(ind);

%% Get times
if options.interp == 0 % get time of data point nearest to slack water
    times.slack = time(ind_slack);
else % interpolate to time where speed would be zero
    for ii = 1:length(ind_slack)
        pts = [ind_slack(ii)-2 : ind_slack(ii) + 2]; % 3 points nearest slack
        times.slack(ii) = interp1(spdsm(pts),time(pts),0,'linear','extrap'); 
   end
end

%% Separate Ebb to Flood and Flood to Ebb
% EtoF = ebb to flood
% FtoE = flood to ebb

acc = smooth(diff(smooth(spdsm)));

cnt_FtoE = 0;
cnt_EtoF = 0;

for ii = 1:length(ind_slack)
    acc_inst = nanmean(acc(ind_slack(ii) -0.5*slack_dur_pts:ind_slack(ii) -0.5*slack_dur_pts));
    if acc_inst >0 %Ebb to Flood
        cnt_EtoF = cnt_EtoF+1;
        ind_EtoF(cnt_EtoF) = ind_slack(ii);
        times.EtoF(cnt_EtoF) = times.slack(ii);
    else
        cnt_FtoE = cnt_FtoE+1;
        ind_FtoE(cnt_FtoE) = ind_slack(ii); %Flood to ebb
        times.FtoE(cnt_FtoE) = times.slack(ii);
    end
end


%% Display warning if length of EtoF and FtoE indices differ by more than 1
if abs(length(ind_FtoE)-length(ind_EtoF))>1
    warning('length of EtoF and FtoE indices differ by more than 1')
end

%%
if options.figure
    myfigure('get_slacks_JMM.m')
    plot(get_yd(time),signed_spd,'k','Linewidth',3,'Marker','x')
    hold all
    plot(get_yd(time),spdsm,'c','Linewidth',3)
    plot(get_yd(times.EtoF),zeros(size(times.EtoF)),'bo')
    plot(get_yd(times.FtoE),zeros(size(times.FtoE)),'ro')
    legend('meas','smooth','E to F','F to E')
end


