function [eps,sigmaN,R2,D,r]=calc_eps_ADCP_SF(vp,z,options)
% calculate the dissipation rate from an ADCP using a structure function
% approach
%
% Justine McMillan
% Feb 7, 2016
% Mar 28, 2016 - Modified figure
% June 6th - simplified code -- now only works for one depth

if ~isfield(options,'figure'); options.figure = 1; end
if ~isfield(options,'theta');  options.theta = 20; end
if ~isfield(options,'Cv2');    options.Cv2 = 2.0;  end

dz = mean(diff(z));
dr = dz/cosd(20);


%% initialize
nz_R0 = floor(options.nrmax/2); % number of points about r0
% nz = length(z);
D = NaN*ones(1,nz_R0);
r = 2*dr*[1:1:nz_R0];
nr = length(r);

%% compute structure functions, epsilon, noise

ind_zc =get_nearestindex(z,options.Z);
for rr = 1:nr
    binL = ind_zc-rr;
    binU = ind_zc+rr;
    
    vdiff = vp(:,binL)-vp(:,binU); % Centre difference
    D(rr) = nanmean((vdiff.^2));
end

% least squares fit
ind = ~isnan(D);
xi = r(ind).^(2/3);
yi = D(ind);
P = polyfit(xi,yi,1);

%R2
fi = polyval(P,xi);
R2 = 1-sum((yi-fi).^2)/sum((yi-mean(yi)).^2);

% calculate eps
eps = (P(1)/options.Cv2)^(3/2);

% calculate noise
sigmaN = sqrt(P(2)/2);

%     if options.figure
%      %   if imag(sigmaN(zz))~=0
%             figure(1),clf
%             plot(r,D(zz,:),'o')
%             hold all
%             p = plot([0:1:10],P(1)*[0:1:10].^(2/3)+P(2),'k');
%             title(['\epsilon = ',num2str(eps(zz)),' W/kg  ,\sigma_N = ',num2str(sigmaN(zz))] )
%             legend(p,['R^2 = ',num2str(R2(zz))])
%             pause
%      %   end
%     end


if options.figure
    figure(1),clf
    plot(r,D,'o');
    hold all
        
    xline = [0:.1:10];
    yline = options.Cv2*eps^(2/3)*xline.^(2/3)+2*sigmaN.^2;
    plot(xline,yline,'Color','k');
        
   % end

    title(['U = ',num2str(options.Uref),' m/s'])
    
    
    pause
end