function [klimits klimopts] = get_optimal_klimits(k,Skk,options)
% klimits = get_optimal_klimits(k,Skk,options)
%
% Function to determine the limits of the inertial subrange based on the
% spectral shape. Based on the principal that it is expected that
% Y = S(k)*k^(5/3)
% will be flat in the inertial subrange
%
% Inputs
% - k: vector of k values
% - Skk: vector of Skk values
% - options: structure with options controlling the choice of the limits
%     .width = width in k units of the outputted ISR range
%     .method = method to use
%        'min_stdmean_ratio': determines the range where the ratio of the
%                             std to the mean is the lowest
%
% Outputs:
% - klimits: optimal k limits
% - klimopts: structure of options used in the fit
%
% Justine McMillan
% Jan 29, 2016


debug = 0;

% defaults
if ~isfield(options,'width');
    options.width = 0.8; %(rad/m)
end

if ~isfield(options,'method');
    options.method = 'min_stdmean_ratio';
end

if ~isfield(options,'figure');
    options.figure = 0;
end

% function that I expect to be flat in inertial subrange
Y = Skk.*k.^(5/3);

% determine k limits
switch options.method
    case 'min_stdmean_ratio';% determines the range where the ratio of the
        % std to the mean is the lowest
        
        dk = (k(end)-k(1))/length(k);
        nk = floor(options.width/dk); % number of points in each interval
        
        for kk = 1:length(k)-nk
            ind_k = kk:kk+nk;
            
            klim(kk,:) = [k(ind_k(1)), k(ind_k(end))];
            
            ind_nan = find(isnan(Y(ind_k)));
            if length(ind_nan)<length(ind_k)/2;
                Y_ratio(kk) = nanstd(Y(ind_k))/nanmean(Y(ind_k))*100;
            else % if there are too many nans, then skip
                Y_ratio(kk) = NaN;
            end
            
            
            if debug
                figure(1),clf
                plot(k,Y)
                hold all
                fill_transparent(klim(kk,:),get(gca,'Ylim'),'c')
                xlabel('k')
                ylabel('Skk')
                title(num2str(Y_ratio(kk)))
                pause
            end
        end
        
        % find minimum
        [~,ind_min] = nanmin(Y_ratio);
        klimits = klim(ind_min,:);
        
        
end
klimopts = options;

if options.figure
    figure,clf
    plot(k,Y)
    hold all
    fill_transparent(klimits,get(gca,'Ylim'),'c')
    xlabel('k')
    ylabel('Skk')
    
end
