%look at aquadopp data
pn = '/data/po/acoustic/kr-experiments/ecoII/june2013GP2/gp2/aquadopp/textFormat';
outpath = '/data/po/acoustic/kr-experiments/ecoII/june2013GP2/gp2/aquadopp/matlab';
inst = 'gpaqua02';
overwrite = 0;

nap.blankDist = [];
nap.avInt = [];
nap.ncells = [];
nap.coord = {};
nap.extRange = {};
nap.v3 = [];
nap.v2 = [];
nap.v1 = [];
nap.a3 = [];
nap.a2 = [];
nap.a1 = [];
nap.cA3 = [];
nap.cA2 = [];
nap.cA1 = [];
nap.c3 = [];
nap.c2 = [];
nap.c1 = [];
nap.pitch = [];
nap.roll = [];
nap.compassHeading = [];
nap.ensemble = [];
nap.burst = [];
nap.error = [];
nap.soundspeed = [];
nap.pressure = [];
nap.battery = [];
nap.status = [];
nap.temperature = [];
nap.NEPTUNEtime = [];
nap.comments = {['Processed using aquadopp meta data version ' version ' on ' datestr(datenum(now))]};
numStr =sprintf('s',datestr(now));
if ~exist([outpath '/' inst '.mat']) | overwrite
    hdrFID = fopen([pn '/' inst '.hdr'],'r');
    hdr = fread(hdrFID);
    fclose(hdrFID);
    hdr = char(hdr);
    t = find(hdr == 10);
    tn = findstr('Number of cells',hdr');
    tnn = find(abs(tn(1)-t) == min(abs(tn(1)-t)));
    nap.ncells = [nap.ncells str2num(hdr(t(tnn+1)+(-4:-1))')];
    tn = findstr('Average interval',hdr');
    tnn = find(abs(tn(1)-t) == min(abs(tn(1)-t)));
    nap.avInt = str2num(hdr(t(tnn+1)+(-7:-6))');
    tn = findstr('Blanking distance',hdr');
    tnn = find(abs(tn(1)-t) == min(abs(tn(1)-t)));
    nap.blankDist = str2num(hdr(t(tnn+1)+(-8:-3))');
    tn = findstr('Coordinate system',hdr');
    tnn = find(abs(tn(1)-t) == min(abs(tn(1)-t)));
    nap.coord{length(nap.coord)+1} = hdr(t(tnn+1)+(-5:-2))';
    tn = findstr('Extended velocity range',hdr');
    tnn = find(abs(tn(1)-t) == min(abs(tn(1)-t)));
    nap.extRange{length(nap.extRange)+1} = hdr(t(tnn+1)+(-4:-2))';
    
    switch nap.extRange{end}
        case ' ON' %regular run
            nap.cellSize = 0.015; %wrong in header file
        case 'OFF' %no ping run
            tn = findstr('Cell size',hdr');
            tnn = find(abs(tn(1)-t) == min(abs(tn(1)-t)));
            nap.cellSize = str2num(hdr(t(tnn+1)+(-6:-4))');
        otherwise
            nap.cellSize = 0.015;
    end
    tn = findstr('Extended velocity range',hdr');
    tnn = find(abs(tn(1)-t) == min(abs(tn(1)-t)));
    nap.extRange{length(nap.extRange)+1} = hdr(t(tnn+1)+(-4:-1))';
    
    nap.range_bins = (1:nap.ncells(1))*nap.cellSize+nap.blankDist(1);
    % Number of cells                       125
    % Average interval                      1 sec
    % Blanking distance                     0.10 m
    % Measurement load                      32 %
    % Coordinate system                     BEAM
    
    
    [month day year hour minute second burst ensemble error status battery soundspeed ...
        compassHeading pitch roll pressure temperature analog1 analog2] = textread([pn '/' inst '.sen'],...
        '%d%d%d%d%d%f%d%d%d%d%f%f%f%f%f%f%f%f%f');
    nap.v3 = load([pn '/' inst '.v3']);
    nap.v2 = load([pn '/' inst '.v2']);
    nap.v1 = load([pn '/' inst '.v1']);
    nap.a3 = load([pn '/' inst '.a3']);
    nap.a2 = load([pn '/' inst '.a2']);
    nap.a1 = load([pn '/' inst '.a1']);
    nap.c3 = load([pn '/' inst '.c3']);
    nap.c2 = load([pn '/' inst '.c2']);
    nap.c1 = load([pn '/' inst '.c1']);
    nap.pitch = [nap.pitch; pitch];
    nap.roll = [nap.roll; roll];
    nap.compassHeading = [nap.compassHeading; compassHeading];
    nap.burst = [nap.burst; burst];
    nap.ensemble = [nap.ensemble; ensemble];
    nap.battery = [nap.battery; battery];
    nap.status = [nap.status; status];
    nap.soundspeed = [nap.soundspeed; soundspeed];
    nap.error = [nap.error; error];
    nap.pressure = [nap.pressure; pressure];
    nap.temperature = [nap.temperature; temperature];
    nap.time = datenum(year,month,day)+(hour+(minute+second/60)/60)/24;
    nap.analog1 = analog1;
    nap.analog2 = analog2;
    
    
    
else
    
    disp(['Skipping ' pn '/' inst])
end %if exist

R = RSSI{aa};
nap.cA1 = range_and_attenuation(R(1)*nap.a1(:,3:nap.ncells+2),30,nap.range_bins,2*10^6,nanmean(nap.temperature),nap.ncells,95,20);
nap.cA2 = range_and_attenuation(R(2)*nap.a2(:,3:nap.ncells+2),30,nap.range_bins,2*10^6,nanmean(nap.temperature),nap.ncells,95,20);
nap.cA3 = range_and_attenuation(R(3)*nap.a3(:,3:nap.ncells+2),30,nap.range_bins,2*10^6,nanmean(nap.temperature),nap.ncells,95,20);

switch nap.coord{1}
    case ' ENU'
        disp(['No coordinate system change, collected in ENU.  Unrotate onboard compass, use fixed heading'])
        [nap.U nap.V] = fnc_rotate_RAC(nap.v1,nap.v2,nap.compassHeading);
        nap.W = nap.v3;
    case ' XYZ'
        disp(['collected in XYZ.'])
        disp('Going to ENU using fixed heading')
        enu = get_ENUfromXYZ(hCorrect,nap.pitch,nap.roll,nap.v1(:,3:end),nap.v2(:,3:end),nap.v3(:,3:end));
        nap.U = squeeze(enu(:,1,:));
        nap.V = squeeze(enu(:,2,:));
        nap.W = squeeze(enu(:,3,:));
    case 'BEAM'
        disp(['Collected in BEAM coordinate system.  Going to XYZ'])
        for rr = 1:length(nap.range_bins)
            A = [nap.v1(:,rr+2) nap.v2(:,rr+2) nap.v3(:,rr+2)];
            xyz = get_XYZfromNBeam(T{aa},A,3);
            nap.X(:,rr) = xyz(1,:);
            nap.Y(:,rr) = xyz(2,:);
            nap.Z(:,rr) = xyz(3,:);
            clear xyz
        end
        disp('Going to ENU')
        enu = get_ENUfromXYZ(hCorrect,nap.pitch,nap.roll,nap.X,nap.Y,nap.Z);
        nap.U = squeeze(enu(:,1,:));
        nap.V = squeeze(enu(:,2,:));
        nap.W = squeeze(enu(:,3,:));
    otherwise
        error('wrong value in coord')
end
%save([pn '/' dn{aa} '/' num2str(yy) '/' processedPath '/matlab/'  dn{aa} '_' numStr(1:end-7) '.mat'],'nap')

figure(1)
clf
a1 = subplot(311);
semilogy(nap.time,nap.analog1)
ylabel('ANALOG 1')
a1(2) = subplot(312); semilogy(nap.time,nap.analog2)
ylabel('ANALOG 2')
a1(3) = subplot(313);
plot(nap.time,nap.a1(:,20))
plot(nap.time,nap.a1(:,3))

figure(2)
clf
subplot(311)
imagesc(nap.a1(:,3:end)')
colorbar
subplot(312)
imagesc(nap.a2(:,3:end)')
colorbar
subplot(313)
imagesc(nap.a3(:,3:end)')
colorbar

figure(3)
clf
subplot(311)
imagesc(nap.c1(:,3:end)')
colorbar
subplot(312)
imagesc(nap.c2(:,3:end)')
colorbar
subplot(313)
imagesc(nap.c3(:,3:end)')
colorbar

figure(4)
clf
subplot(311)
imagesc(nap.v1(:,3:end)')
colorbar
subplot(312)
imagesc(nap.v2(:,3:end)')
colorbar
subplot(313)
imagesc(nap.v3(:,3:end)')
colorbar

plotAquadopp(nap,bsclims,header{aa})
