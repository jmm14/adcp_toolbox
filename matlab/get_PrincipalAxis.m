function [PA_all,PA_ebb,PA_fld] = get_PrincipalAxis(u, v, min_speed,varargin)
%% function PA = get_PrincipalAxis(u, v, min_speed)
%
% This function was modified from Brian Polagye's dir_PrincipalAxis.m code
% to only require u, v and min_speed as inputs. This is especically useful
% when the current direction is not known (ie. Station 2 in Grand Passage
% where the compass wasn't calibrated.)
%
% SHOULD ADD STD DEV calculations
%
%
% inputs
%  - u: x-component of horizontal velocity (t,z)
%  - v: y-component of horizontal velocity (t,z)
%  - min_speed: minimum speed to include for analysis
% Outputs
%  - PA: principal axes for composite tide (in compass coordinates)
%
% Justine McMillan
% Sept 20, 2012
%
% Changes:
% Sept 28, 2012 -- modified to give angle between 0 and 360 
% July 10. 2014 -- modified to output flood and ebb directions (requires
% the signed speed as an input. Different method for ebb/flood that Brian Polagye's)
% July 15, 2014 - MY METHOD DOESN'T SEEM TO BE WORKING

error('THIS CODE IS BELIEVED TO HAVE ERRORS (Ar least for flood/ebb PA). Use dir_PrincipalAxis.m instead, or fix this code')
numbins = size(u,2);
PA = zeros(numbins);

if nargin == 3
    warning('Can only compute PA_all. Specify signed speed as fourth input to separate fld and ebb')
    PA_ebb = NaN*PA;
    PA_fld = NaN*PA;
elseif nargin == 4;
    s = varargin{1}; %Signed speed is an input
end



for i = 1:numbins     %loop through all depth bins
    
    if ~exist('s','var')
        s = sqrt(u(:,i).^2+v(:,i).^2);
        jmax = 1;
    else
        jmax = 3;
    end
    d = get_DirFromN(u(:,i),v(:,i));
    
%    ind_real = find(abs(s(:,i))>=min_speed);  %work only with speeds greater than min
    
    for j = 1:jmax %Do separately for all, fld, ebb
        if j == 1 % all
            ind_real = find(abs(s(:,i))>=min_speed);  %work only with speeds greater than min
            PA_all(i) = calc_PA(u(ind_real,i),v(ind_real,i));
        elseif j == 2
            ind_real = find(s(:,i)>=min_speed);
            PA_fld(i) = calc_PA(u(ind_real,i),v(ind_real,i));
            if std(d(ind_real))>100 %Directions are likely wrapped. Hack to fix
                warning('Hack to fix the wrapping on the flood tide')
                ind_fix = find(d(ind_real)<50);
                d(ind_real(ind_fix)) = d(ind_real(ind_fix))+360;
            end
%             if abs(mean(d(ind_real))-PA_fld(i))>150
%                 disp('test')
%                 PA_fld(i) = PA_fld(i)-180;
%             end
        else
            ind_real = find(s(:,i)<=-min_speed);
            PA_ebb(i) = calc_PA(u(ind_real,i),v(ind_real,i));
%             if abs(mean(d(ind_real))-PA_ebb(i))>150
%                 disp('test')
%                 PA_ebb(i) = PA_ebb(i)-180;
%             end
        end
        myfigure('debug')
        scatter(u(ind_real,i),v(ind_real,i))
        hold all
        if j == 1
            plot([-3*sind(PA_all) 3*sind(PA_all)],[-3*cosd(PA_all) 3*cosd(PA_all)])
        elseif j == 2
            plot([0 3*sind(PA_fld)],[0 3*cosd(PA_fld)])
            title('flood')
        elseif j == 3
            plot([0 3*sind(PA_ebb)],[0 3*cosd(PA_ebb)])
            title('ebb')
        end
        pause
    end
            
end

end
%%
function PA = calc_PA(u,v)
    U = [u, v];     %create velocity matrix
    U = U(~isnan(U(:,1)),:);                %eliminate NaN values
    U = U - repmat(mean(U,1),length(U),1);  %convert matrix to deviate form
    R = U'*U/(length(U)-1);                 %compute covariance matrix (alternatively - cov(U))
    [V,lambda]=eig(R);                      %calculate eigenvalues and eigenvectors for covariance matrix

    %sort eignvalues in descending order so that major axis is given by first eigenvector
    [~, ilambda]=sort(diag(lambda),'descend');     %sort in descending order with indices
    V=V(:,ilambda);                                %reorder the eigenvectors

    ra = atan2(V(2,1),V(2,2));      %rotation angle of major axis in radians relative to cartesian coordiantes
    PA = -ra*180/pi+90      %express principal axis in compass coordinates

   if PA<0
       PA = PA+360;
   end
end