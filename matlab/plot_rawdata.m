function plot_rawdata(adcp,tlim)
% Creates some basic plots of the raw adcp structure
% Inputs:
%   adcp = structure containing raw data
%   tlim = time limits in year day units
%   flg = flag controlling some options
%       SAVE: save the figures

yd = get_yd(adcp.mtime);

if ~isempty(tlim)
    tind = find(yd>tlim(1) &yd<tlim(2));
else
    tind = 1:length(yd);
end
tplt = tind;
%tplt = 1:length(yd);
tplt_zoom = find(yd>yd(1) & yd<yd(1)+2); % First two days


%mean depth here is actually mean distance to the surface from the adcp

meandepth = mean(adcp.depth(tind));

[~,binsurf] = min(abs(adcp.config.ranges - meandepth));

dz = adcp.config.ranges(2)-adcp.config.ranges(1);

zplt = [1:binsurf+floor(5/dz)]; %Include bins 5 m above mean depth in plots
%zplt = [1:length(adcp.config.ranges)];

%% Ancillary Data
myfigure('AncillaryData')
set(gcf,'Position',[204   102   725   857])
a1 = subplot(511);
%plot_fillstd(yd(tplt),adcp.pressure(tplt),adcp.pressure_std(tplt),'b')
plot(yd(tplt),adcp.pressure(tplt))
ylabel({'pressure','(decibars)'})
a1(2) = subplot(512);
plot(yd(tplt),adcp.heading(tplt))
ylabel('heading')
a1(3) = subplot(513);
plot(yd(tplt),adcp.pitch(tplt))
ylabel('pitch')
a1(4) = subplot(514);
plot(yd(tplt),adcp.roll(tplt))
ylabel('roll')
a1(5) = subplot(515);
plot(yd(tplt),adcp.temperature(tplt))
ylabel('temperature')
xlabel('YD (UTC 1 Jan = YD 1)')
set(a1,'XLim',[yd(tplt(1)) yd(tplt(end))])

%% 
myfigure('Correlations')
set(gcf,'Position',[ 107 132  1145  766])
a2 = subplot(411);
imagesc(yd(tplt),adcp.config.ranges(zplt),adcp.c1(zplt,tplt)/128*100,[70 100]);
c=colorbar;
ylabel(c,'BEAM 1')
ylabel('range (m)')
a2(2) = subplot(412);
imagesc(yd(tplt),adcp.config.ranges(zplt),adcp.c2(zplt,tplt)/128*100,[70 100]);
ylabel('range (m)')
c=colorbar;
ylabel(c,'BEAM 2')
a2(3) = subplot(413);
imagesc(yd(tplt),adcp.config.ranges(zplt),adcp.c3(zplt,tplt)/128*100,[70 100]);
ylabel('range (m)')
c=colorbar;
ylabel(c,'BEAM 3')
a2(4) = subplot(414);
imagesc(yd(tplt),adcp.config.ranges(zplt),adcp.c4(zplt,tplt)/128*100,[70 100]);
ylabel('range (m)')
xlabel('YD (UTC 1 Jan = YD 1)')
c=colorbar;
ylabel(c,'BEAM 4')
set(a2,'YDir','Normal')

%%
myfigure('Backscatter')
set(gcf,'Position',[ 107 132  1145  766])
a3 = subplot(411);
imagesc(yd(tplt),adcp.config.ranges(zplt),adcp.A1(zplt,tplt));
ylabel('range (m)')
c=colorbar;
ylabel(c,'BEAM 1')
a3(2) = subplot(412);
imagesc(yd(tplt),adcp.config.ranges(zplt),adcp.A2(zplt,tplt));
ylabel('range (m)')
c=colorbar;
ylabel(c,'BEAM 2')
a3(3) = subplot(413);
imagesc(yd(tplt),adcp.config.ranges(zplt),adcp.A3(zplt,tplt));
ylabel('range (m)')
c=colorbar;
ylabel(c,'BEAM 3')
a3(4) = subplot(414);
imagesc(yd(tplt),adcp.config.ranges(zplt),adcp.A4(zplt,tplt));
c=colorbar;
ylabel(c,'BEAM 4')
ylabel('range (m)')
xlabel('YD (UTC 1 Jan = YD 1)')
set(a3,'YDir','Normal')

%% Flow measurements
midind = floor(binsurf/2);
ulim = 0.9*max(abs(adcp.east_vel(midind,tind)));
vlim = 0.95*max(abs(adcp.north_vel(midind,tind)));
wlim = 0.9*max(abs(adcp.vert_vel(midind,tind)));
elim = 0.9*max(abs(adcp.error_vel(midind,tind)));

%Select 2nd bin closest to ADCP and bin 8m from meandepth for timeseries
%Not necessarily 8m from meandepth due to dab of ADCP
bins = [2 binsurf-floor(8/dz)]; 

for i=4:5
    figure(i),clf
    if i==4
        set(gcf,'Name','FlowMeasurements')
    elseif i==5
        set(gcf,'Name','FlowMeasurementsZoom')
    end
    set(gcf,'Position',[ 107 132  1145  766])
    a4 = subplot(411);
    imagesc(yd(tplt),adcp.config.ranges(zplt),adcp.east_vel(zplt,tplt),[-1 1]*ulim);
    ylabel('range (m)')
    c= colorbar;
    ylabel(c,'U (m/s)')
    hold on
    plot([yd(tplt(1)) yd(tplt(end))],adcp.config.ranges(bins(1))*[1 1],'k')
    plot([yd(tplt(1)) yd(tplt(end))],adcp.config.ranges(bins(end))*[1 1],'k')

    a4(2) = subplot(412);
    imagesc(yd(tplt),adcp.config.ranges(zplt),adcp.north_vel(zplt,tplt),[-1 1]*vlim);
    ylabel('range (m)')
    c= colorbar;
    ylabel(c,'V (m/s)')
    hold on
    plot([yd(tplt(1)) yd(tplt(end))],adcp.config.ranges(bins(1))*[1 1],'k')
    plot([yd(tplt(1)) yd(tplt(end))],adcp.config.ranges(bins(end))*[1 1],'k')

    a4(3) = subplot(413);
    imagesc(yd(tplt),adcp.config.ranges(zplt),adcp.vert_vel(zplt,tplt),[-1 1]*wlim);
    ylabel('range (m)')
    c= colorbar;
    ylabel(c,'W (m/s)')
    hold on
    plot([yd(tplt(1)) yd(tplt(end))],adcp.config.ranges(bins(1))*[1 1],'k')
    plot([yd(tplt(1)) yd(tplt(end))],adcp.config.ranges(bins(end))*[1 1],'k')
    
    a4(4) = subplot(414);
    imagesc(yd(tplt),adcp.config.ranges(zplt),adcp.error_vel(zplt,tplt),[-1 1]*elim);
    ylabel('range (m)')
    xlabel('YD (UTC 1 Jan = YD 1)') 
    c= colorbar;
    ylabel(c,'Err (m/s)')
    hold on
    plot([yd(tplt(1)) yd(tplt(end))],adcp.config.ranges(bins(1))*[1 1],'k')
    plot([yd(tplt(1)) yd(tplt(end))],adcp.config.ranges(bins(end))*[1 1],'k')
    
    set(a4,'YDir','Normal')
    if i==5
        set(a4,'Xlim',[yd(tplt_zoom(1)) yd(tplt_zoom(end))])
    end
end


%% Speed Timeseries



for i=6:7
    if i == 6
        myfigure('SpeedTS')
    elseif i == 7
        myfigure('SpeedTS_Zoom')
    end
    set(gcf,'Position',[ 107 132  1145  766])
    a5(1) = subplot(411);
    plot(yd(tplt),sqrt(adcp.east_vel(bins,tplt).^2+adcp.north_vel(bins,tplt).^2))
    ylabel({'speed (m/s)'});
    il = legend(['@ ' num2str(adcp.config.ranges(bins(1)),'%3.2f'), 'm'],...
        ['@ ' num2str(adcp.config.ranges(bins(2)),'%3.2f') 'm']);
    a5(2) = subplot(412);
    plot(yd(tplt),adcp.vert_vel(bins,tplt)*100)
    ylabel({'W','(cm/s)'});
    a5(3) = subplot(413);
    plot(yd(tplt),adcp.error_vel(bins,tplt)*100)
    ylabel({'Err','(cm/s)'});
    a5(4) = subplot(414);
    plot(yd(tplt),adcp.A4(bins,tplt));
    ylabel({'SSL - beam 4', '(arb dB)'})
    xlabel('YD (UTC 1 Jan = YD 1)') 
    spdlim = sqrt(ulim.^2+vlim.^2);
    set(a5(1),'YLim',[0 spdlim])
    set(a5(2),'YLim',[-1 1]*wlim*100)
    set(a5(3),'YLim',[-1 1]*elim*100)
    set(a5(4),'YLim',[min(min(adcp.A4(bins,tplt))) max(max(adcp.A4(bins,tplt)))])
    if i == 6
        set(a5,'XLim',[yd(tplt(1)) yd(tplt(end))])
    elseif i == 7
        set(a5,'XLim',[yd(tplt_zoom(1)) yd(tplt_zoom(end))])
    end

end
    
