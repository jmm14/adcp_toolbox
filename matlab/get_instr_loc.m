function [lon,lat] = get_instr_loc(name)
%% function [lon,lat] = get_instr_loc(name,varargin)
%
% function to get the lat/lon of a deployment.
% Code modified from plot_instr_loc.m
% Inputs:
%   - name: name of a specific site, or a pattern within the site name
%   - varargin: Optional inputs
%       -> [1] = Set to 1 if on laptop
% Output: 
%   - p: plot handle
%
% Example:
% name = 'GP-1009';
% depfile = '/media/figgy-pc/Research/data/Deployments_summary.txt';
% varargin = {'Marker','o','Color','k','Markersize',10};
% 
% Justine McMillan
% March 15, 2015


depfiles = {'/home/justine/Research/data/Deployments_summary.txt','/media/justine/figgy-pc/Research/data/Deployments_summary.txt'};
if exist(depfiles{1})>0
    depfile = depfiles{1};
elseif exist(depfiles{2})>0
    depfile = depfiles{2};
else 
    error('Deployment file doesnt exist')
end
% load data
fid = fopen(depfile,'r');
data = textscan(fid,'%s%s%f%f%f%f%*[^\n]','CollectOutput',true,'HeaderLines', 2);
fclose(fid);

% get parameters of interest
all_names = data{1,1}(:,1);
all_lat   = data{1,2}(:,1)+data{1,2}(:,2)/60;
all_lon   =-data{1,2}(:,3)-data{1,2}(:,4)/60;

clear data

% search names for site of interest
ind_names = regexpcell(all_names,name);
for ii = 1:length(ind_names)
    disp(['Site: ',all_names{ind_names(ii)}])
    disp(['   Longitude: ',num2str( all_lon(ind_names(ii)))])
    disp(['   Latitude:  ',num2str( all_lat(ind_names(ii)))])
end


lon = all_lon(ind_names);
lat = all_lat(ind_names);

